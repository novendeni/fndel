<?php

class SiteController extends Controller
{
	//public $layout='//layouts/column2';
	
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout = '//layouts/main_as';
		$this->render('index');
	}
	
	/**
	* Display result base on search
	*/
	public function actionSearch($type='default',$page=0){
		if(!Yii::app()->user->isGuest)
			$this->layout='/layouts/column-user';
		else
			$this->layout='/layouts/column1';
		
		$pencarian = new Pencarian;
		$criteria = new CDbCriteria;
                $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
		$criteria->with = array('idTenant');
		
		if(isset($_GET['Pencarian'])){
                    if($type === 'default'){
			$pencarian->kategori = $_GET['Pencarian']['kategori'];
			$pencarian->nilai = $_GET['Pencarian']['nilai'];
			if($pencarian->nilai !== null){
				if($pencarian->kategori === 'warung'){
					$criteria->addSearchCondition('idTenant.nama_warung',$pencarian->nilai);
					$count = Menu::model()->count($criteria);
					$pages = new CPagination($count);
					// results per page
					$pages->pageSize=25;
					$pages->applyLimit($criteria);
					$models = Menu::model()->findAll($criteria);
				}
				if($pencarian->kategori === 'menu'){
					$criteria->addSearchCondition('nama_menu',$pencarian->nilai);
					$count = Menu::model()->count($criteria);
					$pages = new CPagination($count);
					// results per page
					$pages->pageSize=25;
					$pages->applyLimit($criteria);
					$models = Menu::model()->findAll($criteria);
				}
			}
                        $this->render('search',array('models'=>$models,'pages'=>$pages,'pencarian'=>$pencarian));
                    }
                    if ($type === 'json'){
                        $pencarian->nilai = $_GET['Pencarian']['nilai'];
			if($pencarian->nilai !== null){
                            $criteria->addSearchCondition('idTenant.nama_warung',$pencarian->nilai);
                            $criteria->addSearchCondition('nama_menu',$pencarian->nilai);
                            $criteria->limit = 10;
                            $criteria->offset = $page * 10;
                            
                            $models = Menu::model()->findAll($criteria);
			}
                        //echo CJSON::encode($models);
                        //$models = CJSON::encode($models);
                        $result = array();
                        foreach($models as $item){
                            $temp = $item->attributes;
                            if($item->available()){
                                $now = date('Y-m-d');
                                $kap = KapasitasHarian::model()->findByAttributes(array('id_menu'=>$item->id_menu, 'tanggal'=>$now));
                                if($kap !== null){
                                    $temp['flag'] = $kap->jml_stock - $kap->jml_terbeli;
                                }
                            }
                            else {
                                $temp['flag'] = -1;
                            }
                            $temp['nama_warung'] = $item->idTenant->nama_warung;
                            $result[] = $temp;
                        }
                        $result[] = $page;
                        echo CJSON::encode($result);
                    }
                    
		}
		
		//$this->render('search',array('models'=>$models,'pages'=>$pages,'pencarian'=>$pencarian));
	}
	
	/**
	* Display all voucher
	*/
	public function actionVoucher(){
		if(!Yii::app()->user->isGuest)
			$this->layout='/layouts/column-user';
		else
			$this->layout='/layouts/column1';
		
		$criteria = new CDbCriteria;
                $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
		$criteria->addSearchCondition('id_tenant','30');
		
		//pagination
		$count = Menu::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Menu::model()->findAll($criteria);
		
		$this->render('search',array('models'=>$models, 'pages'=>$pages));
	}
	
	/**
	* Display all menu
	*/
	public function actionMenu(){
		if(!Yii::app()->user->isGuest)
			$this->layout='/layouts/column-user';
		else
			$this->layout='/layouts/column1';
		
		$criteria = new CDbCriteria;
		//$criteria->order = 'star DESC';
		//$criteria->select = 't.*, kh.jml_stock stock';
                $criteria->select = 't.id_menu, t.nama_menu, t.id_tenant, t.hrg_jual, t.star, kh.jml_stock stock';
		$criteria->join = 'LEFT JOIN tbl_kapasitas_harian kh ON t.id_menu = kh.id_menu AND kh.tanggal = "'.date('Y-m-d').'"';
		$criteria->order = 'stock DESC';
		
		//pagination
		$count = Menu::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Menu::model()->findAll($criteria);
		
		if(Yii::app()->request->isAjaxRequest){
                    Yii::app()->clientScript->scriptMap=array("jquery.js"=>false, 'duplicatedScrip.js'=>false);
                    $this->renderPartial('_menu',array('models'=>$models, 'pages'=>$pages),FALSE,TRUE);
                } else
                    $this->render('search',array('models'=>$models, 'pages'=>$pages));
	}
	
	/**
	* display model base on popularity / rate
	*/
	public function actionRekomendasi()
	{
		if(!Yii::app()->user->isGuest)
			$this->layout='/layouts/column-user';
		else
			$this->layout='/layouts/column1';
		
		$criteria = new CDbCriteria;
                $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
		$criteria->order = 'star DESC';
		
		//pagination
		$count = Menu::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Menu::model()->findAll($criteria);
		
                //if(Yii::app()->request->isAjaxRequest){
                    Yii::app()->clientScript->scriptMap=array("jquery.js"=>false, 'duplicatedScrip.js'=>false);
                    $this->renderPartial('_menu',array('models'=>$models, 'pages'=>$pages),FALSE,TRUE);
                //}else
                   // $this->render('search',array('models'=>$models, 'pages'=>$pages));
	}
	
	/**
	* display models base on sum of quantity each item
	*/
	public function actionTerlaris($type='default',$page=0)
	{
            if(!Yii::app()->user->isGuest)
                $this->layout='/layouts/column-user';
            else
                $this->layout='/layouts/column1';
            /*
            $q = Yii::app()->db->createCommand()
                    ->select('t.id_menu,t.id_tenant,t.nama_menu,t.hrg_jual,sum(td.jumlah) as jml')
                    ->from('tbl_menu as t, tbl_transaksi_detail as td')
                    ->where('t.id_menu = td.id_menu')
                    ->group('td.id_menu')
                    ->order('jml DESC')
                    ->limit(10)
                    ->queryAll();
            print_r($q);
            */
            
            $criteria = new CDbCriteria;
            $criteria->select = 't.id_menu, t.nama_menu, t.id_tenant, t.hrg_jual, t.star, sum(td.jumlah) jml';
            $criteria->join = 'LEFT JOIN tbl_transaksi_detail td ON t.id_menu = td.id_menu';
            $criteria->group = 'td.id_menu';
            $criteria->order = 'jml DESC';
            
            if($type === 'json'){
                $criteria->limit = 10;
                $criteria->offset = $page * 10;
                
                $models = Menu::model()->findAll($criteria);
                
                $result = array();
                foreach($models as $item){
                    $temp = $item->attributes;
                    if($item->available()){
                        $now = date('Y-m-d');
                        $kap = KapasitasHarian::model()->findByAttributes(array('id_menu'=>$item->id_menu, 'tanggal'=>$now));
                        if($kap !== null){
                            $temp['flag'] = $kap->jml_stock - $kap->jml_terbeli;
                        }
                    }
                    else {
                        $temp['flag'] = -1;
                    }
                    $temp['nama_warung'] = $item->idTenant->nama_warung;
                    $result[] = $temp;
                }
                $result[] = $page;
                echo CJSON::encode($result);
            } else {
                //pagination
		$count = Menu::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Menu::model()->findAll($criteria);
                
                //if(Yii::app()->request->isAjaxRequest){
                    Yii::app()->clientScript->scriptMap=array("jquery.js"=>false, 'duplicatedScrip.js'=>false);
                    $this->renderPartial('_menu',array('models'=>$models, 'pages'=>$pages),FALSE,TRUE);
                //} else 
                    //$this->render('search',array('models'=>$models, 'pages'=>$pages));
                
            }
	}
	
        /*
         * Show newest menu
         */
        public function actionTerbaru($type='default',$page=0){
            if(!Yii::app()->user->isGuest)
                $this->layout='/layouts/column-user';
            else
                $this->layout='/layouts/column1';
		
            $criteria = new CDbCriteria;
            $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
            $criteria->order = 'id_menu DESC';
                
            if ($type === 'json'){
                $criteria->limit = 10;
                $criteria->offset = $page * 10;

                $models = Menu::model()->findAll($criteria);
                
                $result = array();
                foreach($models as $item){
                    $temp = $item->attributes;
                    if($item->available()){
                        $now = date('Y-m-d');
                        $kap = KapasitasHarian::model()->findByAttributes(array('id_menu'=>$item->id_menu, 'tanggal'=>$now));
                        if($kap !== null){
                            $temp['flag'] = $kap->jml_stock - $kap->jml_terbeli;
                        }
                    }
                    else {
                        $temp['flag'] = -1;
                    }
                    $temp['nama_warung'] = $item->idTenant->nama_warung;
                    $result[] = $temp;
                }
                $result[] = $page;
                echo CJSON::encode($result);
            }
            else {
                //pagination
		$count = Menu::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Menu::model()->findAll($criteria);
                
                //if(Yii::app()->request->isAjaxRequest){
                    Yii::app()->clientScript->scriptMap=array("jquery.js"=>false, 'duplicatedScrip.js'=>false);
                    $this->renderPartial('_menu',array('models'=>$models, 'pages'=>$pages));
                //}else 
                    //$this->render('search',array('models'=>$models, 'pages'=>$pages));
            }
        }
        
	/**
	*
	*/
	public function actionPromo($type='default',$page=0){
		$this->renderPartial('nodata');
	}
	
	/**
	*
	*/
	public function actionHowtoorder()
	{
		$this->render('how_to_order');
	}
	
	/**
	*
	*/
	public function actionAboutus()
	{
		$this->render('about_us');
	}
	
	/**
	*
	*/
	public function actionFaq()
	{
		$this->render('faq');
	}
	
	/**
	*
	*/
	public function actionReport(){
		if(isset($_POST['nama'])){
			$nama = $_POST['nama'];
			$email = $_POST['email'];
			$report = $_POST['report'];
			
			if($nama !== null && $email !== null && $report !== null){
				// send mail to tanyafndel@gmail.com
				$to = 'tanyafndel@gmail.com';
				$from = 'admin@fndel.com';
				$subject = 'Feedback user';
				$message = 'Dear Admin,
							<br/> berikut adalah masukan dari user : 
							<br/> Nama Pelapor : '.$nama.'
							<br/> Email : '.$email.'
							<br/> Masukan : <br/>'.$report.'
							<br/><br/>Harap segera ditindaklanjuti.
							<br/><br/>Terimakasih. :)';
				$this->mailsend($to, $from, $subject, $message);
				
				// send email to user
				$to = $email;
				$from = 'admin@fndel.com';
				$subject = 'Feedback user';
				$message = 'Dear '.$nama.',
							<br/>Terimakasi telah memberikan masukan kepada fndel.com, isi masukan/saran/kritik kakak adalah 
							<br/>'.$report.'
							<br/><br/>Kami akan segera memproses feedback kakak, Terimakasih. :)
							<br/><br>
							Salam Hangat,
							<br/><br/>CS fndel.com
							<br/>=============================================
							<br/>www.fndel.com
							<br/>Office : Gang Masjid Sukabirus(Samping Masjid Istiqomah)
							<br/>Phone : 0857-9413-6415
							<br/>Line : fndel
							<br/>Email : tanyafndel@gmail.com';
				$this->mailsend($to, $from, $subject, $message);
				
				Yii::app()->user->setFlash('success','Terimakasih atas saran/laporan anda kami akan segera menindak lanjuti laporan anda.');
			} else {
				Yii::app()->user->setFlash('error','Semua Field harus di isi!');
			}
		}
		
		$this->render('report');
	}
	
	/**
	*
	*/
	public function actionOrder()
	{
		if(!Yii::app()->user->isGuest)
			$this->layout='/layouts/column-user';
		else
			$this->layout='/layouts/column1';	 
		
		$model = new Menu;
		
		if (isset($_GET['id'])){
			$model = Menu::model()->findByPK($_GET['id']);
			$now = date('Y-m-d');
			$kap = KapasitasHarian::model()->findByAttributes(array('id_menu'=>$model->id_menu, 'tanggal'=>$now));
			if($kap !== null && (($kap->jml_stock - $kap->jml_terbeli) > 0))
				$available = true;
			else
				$available = false;
		}
		
		$this->render('order',array('model'=>$model, 'available'=>$available));
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page for user
	 */
	public function actionLogin()
	{
               if(Yii::app()->user->isGuest){
                    $model=new LoginForm;

                    // if it is ajax validation request
                    if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
                    {
                            echo CActiveForm::validate($model);
                            Yii::app()->end();
                    }

                    // collect user input data
                    if(isset($_POST['LoginForm']))
                    {
                            $model->attributes=$_POST['LoginForm'];
                            // validate user input and redirect to the previous page if valid
                            if($model->validate() && $model->login()){

                                    if(Yii::app()->user->roles === 'user'){
                                            $prevUrl = Yii::app()->request->urlReferrer;

                                            if(strpos($prevUrl,'login') !== FALSE){
                                                $this->redirect('index');
                                            } else {
                                                $this->redirect($prevUrl);
                                            }
                                    } else {
                                            echo 'Maaf anda tidak memiliki izin';
                                    }
                            }
                    }
                    // display the login form
		//$this->render('login',array('user'=>$user, 'model'=>$model));
                $this->render('login',array('model'=>$model));
                } else {
                    $this->render('_login');
                }
		
	}

	/**
	 * Displays the login page for karyawan
	 */
	public function actionKaryawan()
	{
		$model=new LoginKaryawan;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginKaryawan']))
		{
			$model->attributes=$_POST['LoginKaryawan'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				
				if(Yii::app()->user->roles === 'admin'){
					$this->redirect(array('/admin/menu'));
				} elseif (Yii::app()->user->roles === 'operator') {
					$this->redirect(array('/operator'));
				} elseif (Yii::app()->user->roles === 'kurir'){
					$this->redirect(array('/kurir'));
				} else {
					echo 'Maaf anda tidak memiliki izin';
				}
			}
			//$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('loginKaryawan',array('model'=>$model));
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		$role = Yii::app()->user->roles;
		Yii::app()->user->logout();

		if(isset(Yii::app()->request->cookies['order']))
			unset(Yii::app()->request->cookies['order']);
		
		if($role === 'user')
			$this->redirect(Yii::app()->homeUrl);
		else
			$this->redirect(array('/site/karyawan'));
	}
	
	/**
	*
	*
	*/
	public function actionForgetPass(){
		if($_POST['username'] && $_POST['email']){
			$user = Customer::model()->findByPK($_POST['username']);
			if($user->email === $_POST['email']){
				$pass = $this->generatePass();
				$user->password = CPasswordHelper::hashPassword($pass);
				$user->save(false);
				
				// send email to user
				$to = $user->email;
				$from = 'admin@fndel.com';
				$subject = 'Recover Password';
				$message = 'Password baru anda adalah <br/><br/>Password : '.$pass;
				$this->mailsend($to, $from, $subject, $message);
				
				Yii::app()->user->setFlash('success',"Password Baru Anda telah kami kirimkan, silahkan periksa email anda untuk melihatnya.");
			} else {
				Yii::app()->user->setFlash('danger',"Maaf e-mail yang anda inputkan tidak terdaftar.");
			}
		} 
		
		$this->render('recoverPass');
	}
        
        public function actionCoba(){
		$this->layout = '//layouts/column2';
		$data = 'from first page';
		$this->render('page1',array('data'=>$data));
	}
	
	public function actionAjax(){
		$data = 'call from ajax';
		
		$this->renderPartial('page2',array('data'=>$data));
	}
}