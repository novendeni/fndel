<?php

class MenuController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','loadImage'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
                        array('allow', // allow generate thumbnail actions
				'actions'=>array('GenerateThumbnail'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	 public function actionLoadImage($id){
		$model = $this->loadModel($id);
		$this->renderPartial('image',array('model'=>$model));
	 }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Menu;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Menu'])) {
			$model->attributes=$_POST['Menu'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id_menu));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Menu'])) {
			$model->attributes=$_POST['Menu'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id_menu));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Menu');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Menu('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Menu'])) {
			$model->attributes=$_GET['Menu'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Menu the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Menu::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Menu $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='menu-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        
        public function actionGenerateThumbnail() {           
            echo  $_SERVER['DOCUMENT_ROOT'];
            chmod( $_SERVER['DOCUMENT_ROOT'].'/images/product', 0777);
            
           
            foreach(Menu::model()->findAll() as $record) {
                
                // save as cache 
                $target = $_SERVER['DOCUMENT_ROOT'].'/images/product/'.$record->id_menu . '.jpg';
                
                if(strpos($record->fotoType,'image') === FALSE &&  strpos($record->fotoType,'png') === FALSE) continue;               
               // if(file_exists($target)) continue;
                
                echo $record->id_menu . "<br />";
                file_put_contents($target,$record->foto);
                
                //  create thumbnail
                $thumbProduct = $_SERVER['DOCUMENT_ROOT'].'/images/thumb/product_'.$record->id_menu . '_thumb.jpg';
                $mediumProduct = $_SERVER['DOCUMENT_ROOT'].'/images/medium/product_'.$record->id_menu . '_medium.jpg';
                $largeProduct = $_SERVER['DOCUMENT_ROOT'].'/images/large/product_'.$record->id_menu . '_large.jpg';
               
                
                list($width, $height) = getimagesize($target);
                // Load
                $thumb = imagecreatetruecolor(Yii::app()->params['productThumbSize'], Yii::app()->params['productThumbSize']);
                $medium = imagecreatetruecolor(Yii::app()->params['productMediumSize'], Yii::app()->params['productMediumSize']);
                $large = imagecreatetruecolor(Yii::app()->params['productLargeSize'], Yii::app()->params['productLargeSize']);
                
                
                switch ($record->fotoType) {
                    case 'jpeg':
                    case 'image/jp':
                         $source = imagecreatefromjpeg($target);
                        // Resize
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, Yii::app()->params['productThumbSize'], Yii::app()->params['productThumbSize'], $width, $height);
                        imagejpeg($thumb, $thumbProduct);    
                        
                        imagecopyresized($medium, $source, 0, 0, 0, 0, Yii::app()->params['productMediumSize'], Yii::app()->params['productMediumSize'], $width, $height);
                        imagejpeg($medium, $mediumProduct);  
                        
                        imagecopyresized($large, $source, 0, 0, 0, 0, Yii::app()->params['productLargeSize'], Yii::app()->params['productLargeSize'], $width, $height);
                        imagejpeg($large, $largeProduct);  
                        break;
                    case 'png':
                         $source = imagecreatefrompng($target);
                        // Resize
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, Yii::app()->params['productThumbSize'], Yii::app()->params['productThumbSize'], $width, $height);
                        imagepng($thumb, $thumbProduct);    
                        
                        imagecopyresized($medium, $source, 0, 0, 0, 0, Yii::app()->params['productMediumSize'], Yii::app()->params['productMediumSize'], $width, $height);
                        imagepng($medium, $mediumProduct);  
                        
                        imagecopyresized($large, $source, 0, 0, 0, 0, Yii::app()->params['productLargeSize'], Yii::app()->params['productLargeSize'], $width, $height);
                        imagepng($large, $largeProduct);    
                        break;
                    
                     case 'image/gi':
                         $source = imagecreatefromgif($target);
                        // Resize
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, Yii::app()->params['productThumbSize'], Yii::app()->params['productThumbSize'], $width, $height);
                        imagegif($thumb, $thumbProduct);    
                        
                        imagecopyresized($medium, $source, 0, 0, 0, 0, Yii::app()->params['productMediumSize'], Yii::app()->params['productMediumSize'], $width, $height);
                        imagegif($medium, $mediumProduct);  
                        
                        imagecopyresized($large, $source, 0, 0, 0, 0, Yii::app()->params['productLargeSize'], Yii::app()->params['productLargeSize'], $width, $height);
                        imagegif($large, $largeProduct);                                   
                        break;
                    default:
                        break;
                }
               

                imagedestroy($thumb);
            }
            
             chmod( $_SERVER['DOCUMENT_ROOT'].'/images/product', 0755);
        }
}