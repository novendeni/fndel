<?php

class KostController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','daftar'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Kost;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kost']))
		{
			$model->attributes=$_POST['Kost'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_kost));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kost']))
		{
			$model->attributes=$_POST['Kost'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_kost));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Kost');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Kost('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Kost']))
			$model->attributes=$_GET['Kost'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Kost the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Kost::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Kost $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kost-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionDaftar()
	{
		if(($_POST['nama'] !== '') && ($_POST['email'] !== '') && ($_POST['nama_kost'] !== '') && ($_POST['area'] !== '')){
			// send email to tanyafndel@gmail.com
			$to = 'tanyafndel@gmail.com';
			$from = 'admin@fndel.com';
			$subject = 'Pendaftaran Kost-an '.$_POST['nama_kost'];
			$message = 'Dear Admin,
						<br/>Telah terjadi pendaftaran kost-an dengan rincian sebagai berikut : 
						<br/>Nama Pendaftar : '.$_POST['nama'].'
						<br/>Email : '.$_POST['email'].'
						<br/>Nama Kost : '.$_POST['nama_kost'].'
						<br/>Alamat : '.$_POST['area'].'
						<br/>Keterangan : '.$_POST['ket'].'
						<br/><br/>Terimakasih. :)';
			$this->mailsend($to, $from, $subject, $message);
			
			// send email to user
			$to = $_POST['email'];
			$from = 'admin@fndel.com';
			$subject = 'Pendaftaran Kost-an '.$_POST['nama_kost'];
			$message = 'Dear '.$_POST['nama'].',
						<br/>Terimakasi telah mendaftarkan kost-an '.$_POST['nama_kost'].' ke FnDel.com
						<br/>Kami akan segera memproses pendaftaran kost-an '.$_POST['nama_kost'].'
						<br/><br/>Terimakasih.
						<br/><br>
						Salam Hangat,
						<br/><br/>CS fndel.com
						<br/>=============================================
						<br/>www.fndel.com
						<br/>Office : Gang Masjid Sukabirus(Samping Masjid Istiqomah)
						<br/>Phone : 0857-9413-6415
						<br/>Line : fndel
						<br/>Email : tanyafndel@gmail.com';
			$this->mailsend($to, $from, $subject, $message);
			
			Yii::app()->user->setFlash('success',"Kost-an anda telah diajukan pada admin untuk didaftarkan.");
			$this->redirect(array('/customer/register'));
		} else {
			Yii::app()->user->setFlash('error',"Kolom dengan tanda * tidak boleh kosong.");
			$this->redirect(array('/customer/register'));
		}
	}
}
