<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// Define a path alias for the Bootstrap extension as it's used internally.
// In this example we assume that you unzipped the extension under protected/extensions.
//	Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Food National Delivery',
	
	// set aliases for bootstrap
	'aliases'=>array(
		'bootstrap'=>realpath(__DIR__ . '/../extensions/bootstrap'),
	),
	
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		
		//import bootstrap components
		'bootstrap.helpers.TbHtml',
		'bootstrap.helpers.TbArray',
		'bootstrap.behaviors.TbWidget',
		'bootstrap.widgets.*'
	),

	'modules'=>array(
		'admin',
		'operator', 
		'users',
		'kurir',
                'API',
		
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'generatorPaths'=>array('bootstrap.gii'), // define bootstrap gii generator
			'class'=>'system.gii.GiiModule',
			'password'=>'pariaman',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
        ),
	),
	
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>'false',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		
		// uncomment the following to use a MySQL database
		'db'=>array( /*
			'connectionString' => 'mysql:host=103.28.12.190; dbname=edikiave_db_mynet',
			'emulatePrepare' => true,
			'username' => 'edikiave_mynet',
			'password' => 'mynet',
			'charset' => 'utf8',*/
			
			'connectionString' => 'mysql:host=127.0.0.1;dbname=edikiave_db_mynet',
			'emulatePrepare' => true,
			'username' => 'root', 
			'password' => '',
			'charset' => 'utf8', 
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		
		// bootstrap components
		'bootstrap'=>array(
			'class'=>'bootstrap.components.TbApi',
		),
		
		'Cookies'=>array(
			'class' => 'application.components.CookiesHelper',
		),
		
		// smtp mail component
		'Smtpmail'=>array(
			'class'=>'application.extensions.smtpmail.PHPMailer',
			'Host'=>"mail.fndel.com",   	//"mail.yourdomain.com",
            'Username'=>'admin@fndel.com',	//'test@yourdomain.com',
            'Password'=>'mynet1234',
            'Mailer'=>'smtp',
            'Port'=>25, 					//26,
            'SMTPAuth'=>true,
			'SMTPSecure'=>'tls', 			//add for gmail
		),
		
		//enable FileCache
		'cache'=>array(
			'class'=>'system.caching.CFileCache',
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
                'productThumbSize'=>'65',
                'productMediumSize'=>'150',
                'productLargeSize'=>'320'
	),
);