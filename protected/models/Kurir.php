<?php

/**
 * This is the model class for table "tbl_kurir".
 *
 * The followings are the available columns in table 'tbl_kurir':
 * @property integer $id_kurir
 * @property string $nama
 * @property string $password
 * @property string $tgl_lahir
 * @property string $no_hp
 * @property string $bank
 * @property string $no_rekening
 * @property string $roles
 *
 * The followings are the available model relations:
 * @property Transaksi[] $transaksis
 */
class Kurir extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_kurir';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, password, tgl_lahir, no_hp, bank, no_rekening', 'required'),
			array('nama, bank, no_rekening', 'length', 'max'=>40),
			array('password', 'length', 'max'=>128),
			array('no_hp, roles', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_kurir, nama, password, tgl_lahir, no_hp, bank, no_rekening, roles', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transaksis' => array(self::HAS_MANY, 'Transaksi', 'id_kurir'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_kurir' => 'Id Kurir',
			'nama' => 'Nama',
			'password' => 'Password',
			'tgl_lahir' => 'Tgl Lahir',
			'no_hp' => 'No Hp',
			'bank' => 'Bank',
			'no_rekening' => 'No Rekening',
			'roles' => 'Roles',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_kurir',$this->id_kurir);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('roles',$this->roles,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kurir the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function validatePassword($password){
		return CPasswordHelper::verifyPassword($password,$this->password);
	}
	
	public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }
}
