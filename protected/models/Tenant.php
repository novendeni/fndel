<?php

/**
 * This is the model class for table "tbl_tenant".
 *
 * The followings are the available columns in table 'tbl_tenant':
 * @property integer $id_tenant
 * @property string $nama_warung
 * @property string $nama_pemilik
 * @property string $no_hp
 * @property string $alamat
 * @property string $area
 *
 * The followings are the available model relations:
 * @property Menu[] $menus
 */
class Tenant extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_tenant';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_warung, nama_pemilik, no_hp, alamat, area', 'required'),
			array('nama_warung, nama_pemilik, alamat, area', 'length', 'max'=>40),
			array('no_hp', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tenant, nama_warung, nama_pemilik, no_hp, alamat, area', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menus' => array(self::HAS_MANY, 'Menu', 'id_tenant'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tenant' => 'Id Tenant',
			'nama_warung' => 'Nama Warung',
			'nama_pemilik' => 'Nama Pemilik',
			'no_hp' => 'No Hp',
			'alamat' => 'Alamat',
			'area' => 'Area',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tenant',$this->id_tenant);
		$criteria->compare('nama_warung',$this->nama_warung,true);
		$criteria->compare('nama_pemilik',$this->nama_pemilik,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('area',$this->area,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tenant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
