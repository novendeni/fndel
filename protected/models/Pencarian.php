<?php

/**
 * Pencarian class.
 * Pencarian is the data structure for keeping
 * user search form data. It is used by the 'search' action of 'SiteController'.
 */
class Pencarian extends CFormModel
{
	public $kategori;
	public $nilai;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(''),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'kategori'=>'Kategori',
		);
	}

}