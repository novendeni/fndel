<?php

/**
 * This is the model class for table "tbl_spesial_menu".
 *
 * The followings are the available columns in table 'tbl_spesial_menu':
 * @property integer $id_spesial_menu
 * @property integer $id_menu
 * @property string $tanggal_awal
 * @property string $update_by
 * @property string $occurence
 *
 * The followings are the available model relations:
 * @property Menu $idMenu
 */
class SpesialMenu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_spesial_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_menu, tanggal_awal, update_by, occurence', 'required'),
			array('id_menu', 'numerical', 'integerOnly'=>true),
			array('update_by, occurence', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_spesial_menu, id_menu, tanggal_awal, update_by, occurence', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMenu' => array(self::BELONGS_TO, 'Menu', 'id_menu'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_spesial_menu' => 'Id Spesial Menu',
			'id_menu' => 'Id Menu',
			'tanggal_awal' => 'Tanggal Awal',
			'update_by' => 'Update By',
			'occurence' => 'Occurence',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_spesial_menu',$this->id_spesial_menu);
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('tanggal_awal',$this->tanggal_awal,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('occurence',$this->occurence,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SpesialMenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
