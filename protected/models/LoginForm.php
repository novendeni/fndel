<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $nim;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// nim and password are required
			array('nim, password', 'required', 'message'=>'{attribute} tidak boleh kosong.'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
                        'nim' => 'NIM',
                    'password' => 'Password',
			'rememberMe'=>'Ingat Saya',
                                
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new CustomerIdentity($this->nim,$this->password);
			//var_dump($this->nim);
			if($this->_identity->authenticate() == 1 || $this->_identity->authenticate() == 2)
                            $this->addError('password','NIM / password salah');
                        else if($this->_identity->authenticate() == 3)
                            $this->addError('password','Akun belum aktif');
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new CustomerIdentity($this->nim,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			Yii::app()->session['username'] = Yii::app()->user;
			return true;
		}
		else
			return false;
	}
}