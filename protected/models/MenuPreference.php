<?php

/**
 * This is the model class for table "tbl_menu_preference".
 *
 * The followings are the available columns in table 'tbl_menu_preference':
 * @property string $nim
 * @property integer $id_menu
 * @property string $preferensi
 * @property string $last_update
 * @property string $update_by
 */
class MenuPreference extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_menu_preference';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nim, id_menu, preferensi, last_update, update_by', 'required'),
			array('id_menu', 'numerical', 'integerOnly'=>true),
			array('nim, preferensi, update_by', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nim, id_menu, preferensi, last_update, update_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nim' => 'Nim',
			'id_menu' => 'Id Menu',
			'preferensi' => 'Preferensi',
			'last_update' => 'Last Update',
			'update_by' => 'Update By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nim',$this->nim,true);
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('preferensi',$this->preferensi,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('update_by',$this->update_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MenuPreference the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
