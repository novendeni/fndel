<?php

/**
 * This is the model class for table "tbl_customer".
 *
 * The followings are the available columns in table 'tbl_customer':
 * @property string $nim
 * @property string $password
 * @property string $nama
 * @property string $nama_kos
 * @property integer $no_kamar
 * @property string $area
 * @property string $tempat_lhr
 * @property string $tgl_lhr
 * @property string $no_hp
 * @property string $email
 * @property integer $sisa_saldo
 * @property string $jns_kelamin
 *
 * The followings are the available model relations:
 * @property Menu[] $tblMenus
 * @property TopUp[] $topUps
 * @property Transaksi[] $transaksis
 */
class Customer extends CActiveRecord
{
	public $confirm_pass;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nim, nama, password, nama_kos, no_kamar, area, tempat_lhr, tgl_lhr, no_hp, email, sisa_saldo, jns_kelamin, confirm_pass', 'required', 'message'=>'{attribute} tidak boleh kosong.'),
			array('sisa_saldo', 'numerical', 'integerOnly'=>true, 'message'=>'{attribute} harus berupa angka.'),
			array('nim, nama, nama_kos, area, tempat_lhr, email', 'length', 'max'=>40),
			array('no_hp', 'length', 'max'=>20),
			array('jns_kelamin', 'length', 'max'=>2), 
			array('nim','unique','message'=>'NIM telah terdaftar'),
			array('password', 'compare', 'compareAttribute'=>'confirm_pass', 'message'=>'Password tidak sesuai.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nim, nama, nama_kos, no_kamar, area, tempat_lhr, tgl_lhr, no_hp, email, sisa_saldo, jns_kelamin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblMenus' => array(self::MANY_MANY, 'Menu', 'tbl_menu_preference(nim, id_menu)'),
			'topUps' => array(self::HAS_MANY, 'TopUp', 'nim'),
			'transaksis' => array(self::HAS_MANY, 'Transaksi', 'nim'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nim' => 'Nim',
			'nama' => 'Nama',
			'nama_kos' => 'Nama Kos',
			'no_kamar' => 'No Kamar',
			'area' => 'Area',
			'tempat_lhr' => 'Tempat Lahir',
			'tgl_lhr' => 'Tanggal Lahir',
			'no_hp' => 'No Hp',
			'email' => 'Email',
			'sisa_saldo' => 'Sisa Saldo',
			'jns_kelamin' => 'Jenis Kelamin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nim',$this->nim,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('nama_kos',$this->nama_kos,true);
		$criteria->compare('no_kamar',$this->no_kamar);
		$criteria->compare('area',$this->area,true);
		$criteria->compare('tempat_lhr',$this->tempat_lhr,true);
		$criteria->compare('tgl_lhr',$this->tgl_lhr,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('sisa_saldo',$this->sisa_saldo);
		$criteria->compare('jns_kelamin',$this->jns_kelamin,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Customer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function validatePassword($password){
		return CPasswordHelper::verifyPassword($password,$this->password);
	}
	
	public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }
}