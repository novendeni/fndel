<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class CustomerIdentity extends CUserIdentity
{
	private $_id;
        const ERROR_NOT_ACTIVE = 3;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
            $user = Customer::model()->findByPK($this->username);
            /*
            $criteria = new CDbCriteria;
            $criteria->select = 'nim,nama,roles,sisa_saldo,last_login,approve';
            $criteria->condition = 'nim=:nim';
            $criteria->params = array(':nim'=>$this->username);
            
            $user = Customer::model()->find($criteria);
            */
		if ($user === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if (!$user->validatePassword($this->password))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
                else if ($user->approve !== '1') //check if account active 
                        $this->errorCode = self::ERROR_NOT_ACTIVE;
		else {
			$this->_id=$user->nim;
			$this->username = $user->nim;
			$this->setState('nim', $user->nim);
			$this->setState('nama', $user->nama);
			$this->setState('roles', $user->roles);
			$this->setState('saldo', $user->sisa_saldo);
			$this->errorCode = self::ERROR_NONE;
			
			$user->last_login = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) + (7 * 60 * 60));
			$user->save(false);
		}
			return $this->errorCode;
	}
		
	public function getId(){
		return $this->_id;
	}
}