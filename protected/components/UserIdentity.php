<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		/*$admin = Admin::model()->findByAttributes(array('username'=>$this->username));
		if ($admin === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if (!$admin->validatePassword($this->password)){
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}
		else {
			$this->_id=$admin->id_admin;
			$this->username = $admin->username;
			$this->setState('roles', $admin->roles);
			$this->errorCode = self::ERROR_NONE;
		}
		
		$op = Operator::model()->findByAttributes(array('nama'=>$this->username));
		if ($op === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if (!$op->validatePassword($this->password))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			$this->_id = $op->id_operator;
			$this->username = $op->nama;
			$this->setState('roles', $op->roles);
			$this->errorCode = self::ERROR_NONE;
		}
		
		$kurir = Kurir::model()->findByAttributes(array('nama'=>$this->username));
		if ($kurir === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if (!$kurir->validatePassword($this->password))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			$this->_id=$kurir->id_kurir;
			$this->username = $kurir->nama;
			$this->setState('roles', $kurir->roles);
			$this->errorCode = self::ERROR_NONE;
		}
		*/
		
		$admin = Admin::model()->findByAttributes(array('username'=>$this->username));
		$op = Operator::model()->findByAttributes(array('nama'=>$this->username));
		$kurir = Kurir::model()->findByAttributes(array('nama'=>$this->username));
		
		if($admin !== NULL){
			if (!$admin->validatePassword($this->password)){
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			}
			else {
				$this->_id=$admin->id_admin;
				$this->username = $admin->username;
				$this->setState('nama', $admin->username);
				$this->setState('roles', $admin->roles);
				$this->errorCode = self::ERROR_NONE;
			}
		} elseif ($op !== NULL) {
			if (!$op->validatePassword($this->password))
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			else {
				$this->_id = $op->id_operator;
				$this->username = $op->nama;
				$this->setState('nama', $op->nama);
				$this->setState('roles', $op->roles);
				$this->errorCode = self::ERROR_NONE;
			}
		} elseif ($kurir !== NULL){
			if (!$kurir->validatePassword($this->password))
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			else {
				$this->_id=$kurir->id_kurir;
				$this->username = $kurir->nama;
				$this->setState('nama', $kurir->nama);
				$this->setState('roles', $kurir->roles);
				$this->errorCode = self::ERROR_NONE;
			}
		} else {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}
		return $this->errorCode == self::ERROR_NONE;
	}
		
	public function getId(){
		return $this->_id;
	}
}