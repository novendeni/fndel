<?php
/* @var $this TenantController */
/* @var $data Tenant */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tenant')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tenant),array('view','id'=>$data->id_tenant)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_warung')); ?>:</b>
	<?php echo CHtml::encode($data->nama_warung); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->nama_pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_hp')); ?>:</b>
	<?php echo CHtml::encode($data->no_hp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area')); ?>:</b>
	<?php echo CHtml::encode($data->area); ?>
	<br />


</div>