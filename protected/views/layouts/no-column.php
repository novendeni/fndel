<?php $this->beginContent('//layouts/main'); ?>

			<section id="search-box" class="collapse navbar-collapse">
				<div class="container">
					<form action="<?php echo Yii::app()->createUrl('site/search'); ?>" method="POST">
						<div class="row">
							<div class="col-md-9 col-sm-9">
								<div class="search-group">
									<div class="radio-select">
										<div class="options">
											<div><input type="radio" name="kategori" value="all" />All</div>
											<div><input type="radio" name="kategori" value="makanan" />Makanan</div>
											<div><input type="radio" name="kategori" value="minuman" />Minuman</div>
											<div><input type="radio" name="kategori" value="snack" />Snack</div>
										</div>	
									</div>
									<input type="text" name="nama_menu" placeholder="Search..."/>
								</div>
							</div>
							<div class="col-md-3 col-sm-3">
								<div class="button">
									<button type="submit" class="btn-4"><i class="fa fa-search"></i> Cari</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="close-collapse visible-xs"><i class="fa fa-angle-up"></i></div>
			</section>
			
			<section id="content">
				<div class="container">
				
					<?php if(isset($this->breadcrumbs)):
						if ( Yii::app()->controller->route !== 'site/index' )
						$this->breadcrumbs = array_merge(array (Yii::t('zii','Home')=>Yii::app()->homeUrl), $this->breadcrumbs);

						$this->widget('zii.widgets.CBreadcrumbs', array(
							'links'=>$this->breadcrumbs,
							'homeLink'=>false,
							'tagName'=>'ul',
							'separator'=>'',
							'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
							'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
							'htmlOptions'=>array ('class'=>'breadcrumb-2')
						)); ?><!-- breadcrumbs -->
					<?php endif; ?>
					
					<?php echo $content; ?>
					
					
				</div>
			</section>
           

<?php $this->endContent(); ?>