<?php $this->beginContent('//layouts/main'); ?>

			<section id="search-box" class="collapse navbar-collapse">
				<div class="container">
					<form action="<?php echo Yii::app()->createUrl('site/search'); ?>" method="POST">
						<div class="row">
							<div class="col-md-9 col-sm-9">
								<div class="search-group">
									<div class="radio-select">
										<div class="options">
											<div><input type="radio" name="kategori" value="all" />All</div>
											<div><input type="radio" name="kategori" value="makanan" />Makanan</div>
											<div><input type="radio" name="kategori" value="minuman" />Minuman</div>
											<div><input type="radio" name="kategori" value="snack" />Snack</div>
										</div>	
									</div>
									<input type="text" name="nama_menu" placeholder="Search..."/>
								</div>
							</div>
							<div class="col-md-3 col-sm-3">
								<div class="button">
									<button type="submit" class="btn-4"><i class="fa fa-search"></i> Cari</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="close-collapse visible-xs"><i class="fa fa-angle-up"></i></div>
			</section>
			
			<section id="content">
				<div class="container">
				
					<?php if(isset($this->breadcrumbs)):
						if ( Yii::app()->controller->route !== 'site/index' )
						$this->breadcrumbs = array_merge(array (Yii::t('zii','Home')=>Yii::app()->homeUrl), $this->breadcrumbs);

						$this->widget('zii.widgets.CBreadcrumbs', array(
							'links'=>$this->breadcrumbs,
							'homeLink'=>false,
							'tagName'=>'ul',
							'separator'=>'',
							'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
							'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
							'htmlOptions'=>array ('class'=>'breadcrumb-2')
						)); ?><!-- breadcrumbs -->
					<?php endif; ?>
					
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div id="side-menu" class="list-group">
								<div id="shopping-cart" class="panel-collapse collapse in">
									<?php echo CHtml::beginForm(Yii::app()->createUrl('/transaksi/simpan'),'POST'); ?>
									<table class="table" id="form-table">
										<tr>
											<th colSpan="4">Pesanan</th>
										</tr> 
										<?php 
											$total = 0;
											if(isset(Yii::app()->request->cookies['order'])){
												$cookies = Yii::app()->request->cookies['order']->value;
												$temp = CJSON::decode($cookies);
												$models = array();
												foreach($temp as $i=>$model){
													$obj = new TransaksiDetail;
													$obj->attributes = $temp[$i];
													$total = $total + $obj->subtotal;
													$models[] = $obj;
												} 
										?>
												<?php foreach($models as $i=>$detail): ?>
												<tr>
													<td><?php echo $detail->idMenu->nama_menu; ?></td>
													<td><?php echo $detail->jumlah; ?></td>
													<td><?php echo $detail->subtotal; ?></td>
													<td><a href="<?php echo Yii::app()->createUrl('/users/menu/cancel',array('id'=>$detail->id_menu));?>"><span title="Remove">&times;</span></a></td>
													<?php echo CHtml::activeHiddenField($detail,"[$i]id_menu",array('value'=>$detail->id_menu)); ?>
													<?php echo CHtml::activeHiddenField($detail,"[$i]jumlah",array('value'=>$detail->jumlah)); ?>
													<?php echo CHtml::activeHiddenField($detail,"[$i]subtotal",array('value'=>$detail->subtotal)); ?>
												</tr>
												<?php endforeach;?>
										
										<tr>
											<td colSpan="2">Total</td>
											<td colSpan="2">
												<?php echo $total;?>
												<?php echo CHtml::hiddenField('hrg-total',$total); ?>
											</td>
										</tr> 
										<?php } ?>
									</table>
													<?php echo CHtml::submitButton('Order',array('id'=>'btn-ok')); ?>
												<?php echo CHtml::endForm(); ?>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#menu-makanan">
									Menu
								</a>
								<div id="menu-makanan" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('site/terlaris');?>" class="list-group-item"><i class="fa fa-star"></i> Terlaris</a>
									<a href="<?php echo Yii::app()->createUrl('site/rekomendasi');?>" class="list-group-item"><i class="fa fa-thumbs-up"></i> Rekomendasi</a>
									<a href="#" class="list-group-item"><i class="fa fa-plus-circle"></i> Menu Baru</a>
									<a href="#" class="list-group-item"><i class="fa fa-bookmark"></i> Promo</a>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#kategori-makanan">
									Kategori
								</a>
								<div id="kategori-makanan" class="panel-collapse collapse in">
									<a href="#" class="list-group-item"><i class="fa fa-cutlery"></i> Makanan</a>
									<a href="#" class="list-group-item"><i class="fa fa-glass"></i> Minuman</a>
									<a href="#" class="list-group-item"><i class="fa fa-archive"></i> Camilan</a>
								</div>
							</div>
						</div>
						<div class="col-md-9 col-sm-9 left">
							<?php echo $content; ?>
						</div>
					</div>
				</div>
			</section>
           
<?php $this->endContent(); ?>