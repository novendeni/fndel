<?php $this->beginContent('//layouts/main'); ?>

			<section id="search-box" class="collapse navbar-collapse">
				<div class="container">
					<form action="<?php echo Yii::app()->createUrl('site/search'); ?>" method="GET">
						<div class="row">
							<div class="col-md-9 col-sm-9">
								<div class="search-group">
									<div class="radio-select">
										<div class="options">
											<div><input type="radio" name="Pencarian[kategori]" value="menu" />Makanan</div>
											<div><input type="radio" name="Pencarian[kategori]" value="warung" />Warung</div>
										</div>	
									</div>
									<input type="text" name="Pencarian[nilai]" placeholder="Search..."/>
								</div>
							</div>
							<div class="col-md-3 col-sm-3">
								<div class="button">
									<button type="submit" class="btn-4"><i class="fa fa-search"></i> Cari</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="close-collapse visible-xs"><i class="fa fa-angle-up"></i></div>
			</section>
			
			<section id="content">
				<div class="container">
				
					<?php if(isset($this->breadcrumbs)):
						if ( Yii::app()->controller->route !== 'site/index' )
						$this->breadcrumbs = array_merge(array (Yii::t('zii','Home')=>Yii::app()->homeUrl), $this->breadcrumbs);

						$this->widget('zii.widgets.CBreadcrumbs', array(
							'links'=>$this->breadcrumbs,
							'homeLink'=>false,
							'tagName'=>'ul',
							'separator'=>'',
							'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
							'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
							'htmlOptions'=>array ('class'=>'breadcrumb-2')
						)); ?><!-- breadcrumbs -->
					<?php endif; ?>
					
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div id="side-menu" class="list-group">
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#menu-makanan">
									Menu
								</a>
								<div id="menu-makanan" class="panel-collapse collapse in">
                                                                    <a href="<?php echo Yii::app()->createUrl('site/terlaris');?>" class="list-group-item"><i class="fa fa-star"></i> Terlaris</a>
                                                                    <a href="<?php echo Yii::app()->createUrl('site/rekomendasi');?>" class="list-group-item"><i class="fa fa-thumbs-up"></i> Rekomendasi</a>
									<!--<a href="#" class="list-group-item"><i class="fa fa-plus-circle"></i> Menu Baru</a>-->
                                                                    <a href="<?php echo Yii::app()->createUrl('site/promo');?>" class="list-group-item"><i class="fa fa-bookmark"></i> Promo</a>
                                                                </div> 
                                                                <!-- remove broken link
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#kategori-makanan">
									Kategori
								</a>
								<div id="kategori-makanan" class="panel-collapse collapse in">
									<a href="#" class="list-group-item"><i class="fa fa-cutlery"></i> Makanan</a>
									<a href="#" class="list-group-item"><i class="fa fa-glass"></i> Minuman</a>
									<a href="#" class="list-group-item"><i class="fa fa-archive"></i> Camilan</a>
								</div> -->
							</div>
						</div>
						<div class="col-md-9 col-sm-9 left">
							<?php echo $content; ?>
						</div>
					</div>
				</div>
			</section>
<?php $this->endContent();?>