<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
			<section id="search-box" class="collapse navbar-collapse">
				<div class="container">
					<form action="<?php echo Yii::app()->createUrl('site/search'); ?>" method="GET">
						<div class="row">
							<div class="col-md-9 col-sm-9">
								<div class="search-group">
									<div class="radio-select">
										<div class="options">
											<div><input type="radio" name="Pencarian[kategori]" value="menu" />Makanan</div>
											<div><input type="radio" name="Pencarian[kategori]" value="warung" />Warung</div>
										</div>	
									</div>
									<input type="text" name="Pencarian[nilai]" placeholder="Search..."/>
								</div>
							</div>
							<div class="col-md-3 col-sm-3">
								<div class="button">
									<button type="submit" class="btn-4"><i class="fa fa-search"></i> Cari</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="close-collapse visible-xs"><i class="fa fa-angle-up"></i></div>
			</section>
			
			<section id="content">
				<div class="container">
				
					<?php if(isset($this->breadcrumbs)):
						if ( Yii::app()->controller->route !== 'site/index' )
						$this->breadcrumbs = array_merge(array (Yii::t('zii','Home')=>Yii::app()->homeUrl), $this->breadcrumbs);

						$this->widget('zii.widgets.CBreadcrumbs', array(
							'links'=>$this->breadcrumbs,
							'homeLink'=>false,
							'tagName'=>'ul',
							'separator'=>'',
							'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
							'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
							'htmlOptions'=>array ('class'=>'breadcrumb-2')
						)); ?><!-- breadcrumbs -->
					<?php endif; ?>
					
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div id="side-menu" class="list-group">
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#shopping-cart">
									<i class="fa fa-shopping-cart"></i> &nbsp;Keranjang Belanja
								</a>
								<div id="shopping-cart" class="panel-collapse collapse in">
									<div class="list-group-item">Sisa Saldo <span class="saldo currency">
										<?php echo Yii::app()->user->saldo ?></span>
									</div>
									<?php echo CHtml::beginForm(Yii::app()->createUrl('users/transaksi/simpan'),'POST'); ?>
									<table class="table" id="form-table">
										<tr>
											<th colSpan="4">Pesanan</th>
										</tr> 
										<?php 
											$total = 0;
											if(isset(Yii::app()->request->cookies['order'])){
												$cookies = Yii::app()->request->cookies['order']->value;
												$temp = CJSON::decode($cookies);
												$models = array();
												foreach($temp as $i=>$model){
													$obj = new TransaksiDetail;
													$obj->attributes = $temp[$i];
													$total = $total + $obj->subtotal;
													$models[] = $obj;
												} 
										?>
											
												<?php foreach($models as $i=>$detail): ?>
												<tr>
													<td><?php echo $detail->idMenu->nama_menu; ?></td>
													<td><?php echo $detail->jumlah; ?></td>
													<td><?php echo $detail->subtotal; ?></td>
													<td><a href="<?php echo Yii::app()->createUrl('/users/menu/cancel',array('id'=>$detail->id_menu));?>"><span title="Remove">&times;</span></a></td>
													<?php echo CHtml::activeHiddenField($detail,"[$i]id_menu",array('value'=>$detail->id_menu)); ?>
													<?php echo CHtml::activeHiddenField($detail,"[$i]jumlah",array('value'=>$detail->jumlah)); ?>
													<?php echo CHtml::activeHiddenField($detail,"[$i]subtotal",array('value'=>$detail->subtotal)); ?>
												</tr>
												<?php endforeach;?>
										
										<tr>
											<td colSpan="2">Total</td>
											<td colSpan="2">
												<?php echo $total;?>
												<?php echo CHtml::hiddenField('hrg-total',$total); ?>
											</td>
										</tr> 
										<?php } ?>
									</table>
													<?php echo CHtml::submitButton('Order',array('id'=>'btn-ok')); ?>
												<?php echo CHtml::endForm(); ?>
								</div>
								<a class="list-group-item header lain" data-toggle="collapse" data-parent="#accordion" href="#menu-edit">
									<i class="fa fa-user"></i> &nbsp;Profil
								</a>
								<div id="menu-edit" class="panel-collapse collapse">
									<a href="<?php echo Yii::app()->createUrl('/users/customer/update'); ?>" class="list-group-item"><i class="fa fa-edit"></i> Edit Profil</a>
									<a href="<?php echo Yii::app()->createUrl('/users/transaksi/history'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> History Transaksi</a>
									<a href="<?php echo Yii::app()->createUrl('/users/topUp/history'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> History Top Up</a>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#menu-makanan">
									<i class="fa fa-bars"></i> &nbsp;Menu Makanan
								</a>
								<div id="menu-makanan" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('/users/menu/list'); ?>" class="list-group-item"><i class="fa fa-bars"></i> Daftar Menu</a>
									<a href="<?php echo Yii::app()->createUrl('/users/menuPreference/favorite'); ?>" class="list-group-item"><i class="fa fa-star"></i> Menu Favorit</a>
									<a href="<?php echo Yii::app()->createUrl('/users/menu/rekomendasi'); ?>" class="list-group-item"><i class="fa fa-thumbs-up"></i> Menu Rekomendasi</a>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#voucer">
									<i class="fa fa-bars"></i> &nbsp;Layanan lain
								</a>
								<div id="voucer" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('/users/menu/voucher'); ?>" class="list-group-item"><i class="fa fa-bars"></i> Voucher MyNet</a>
								</div>
							</div>
						</div>
						<div class="col-md-9 col-sm-9 left">
							<?php echo $content; ?>
						</div>
					</div>
				</div>
			</section>

<?php $this->endContent(); ?>