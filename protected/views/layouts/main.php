<?php /* @var $this Controller */ ?>
<?php //Yii::app()->bootstrap->register(); //load bootstrap?>
<?php //Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/fndl/css/fndl.css');?>
<?php 
	$activePage = $this->getAction()->controller->action->id;
?>
<?php if(!isset($model)){ $model=new LoginForm; }?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/images/icon.png" rel="shortcut icon" type="image/png" />
		<link href="<?php echo Yii::app()->request->baseUrl;?>/css/font-awesome.min.css" rel="stylesheet" >
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fndel.css" rel="stylesheet">

	</head>
	
	<body>
	
		<div id="wrapper">
			<header>
				<div id="line-header"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							
						</div>
					</div>	
					<nav class="navbar navbar-default" role="navigation"> 
						<div class="navbar-header">
							<button type="button" id="menu-toggle" class="navbar-toggle btn-3" data-toggle="collapse" data-target="#navbar-collapse-1">
								<i class="fa fa-bars"></i> Menu
							</button>
							<button type="button" id="user-toggle" class="navbar-toggle btn-3" data-toggle="collapse" data-target="#navbar-collapse-2">
								<i class="fa fa-user"></i>
							</button>
							<a href="<?php echo Yii::app()->request->baseUrl; ?>/" id="main-logo"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a> 
						</div>
						<div class="collapse navbar-collapse" id="navbar-collapse-1">
							<div id="phone-box" class="hidden-sm hidden-xs">
								<div id="phone-image"><i class="fa fa-phone"></i></div>
								<div>
									<div id="phone-text">Pesan Melalui Telpon</div>
									<div id="phone-number">0857 9413 6415</div>
								</div>
							</div>
							<ul id="main-nav" class="nav navbar-nav navbar-right">
								<li <?php if($activePage == "index"){echo 'class="active"';} ?>><a href="<?php echo Yii::app()->createUrl('site/index'); ?>">Home</a></li>
								<li <?php if($activePage == "aboutus"){echo 'class="active"';} ?>><a href="<?php echo Yii::app()->createUrl('site/aboutus'); ?>">About Us</a></li>
								<li <?php if($activePage == "howtoorder"){echo 'class="active"';} ?>><a href="<?php echo Yii::app()->createUrl('site/howtoorder'); ?>">How to Order</a></li>
								<li <?php if($activePage == "report"){echo 'class="active"';} ?>><a href="<?php echo Yii::app()->createUrl('site/report'); ?>">Feedback</a></li>
							</ul>
							<div class="close-collapse visible-xs"><i class="fa fa-angle-up"></i></div>
						</div>
						<div class="collapse navbar-collapse" id="navbar-collapse-2">
							<ul id="user-box" class="nav navbar-nav">
								<li>
									<div class="btn-group">
										<?php 
										if(!Yii::app()->user->isGuest)
										{
											echo '<a href="'.Yii::app()->createUrl('/site/logout').'"><i class="fa fa-sign-out"></i> Logout</a>';
										}
										else 
										{
										?>
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											<i class="fa fa-lock"></i> Login
										</a>
										<ul id="dropdown-login" class="dropdown-menu dropdown-menu-left" role="menu">
											<div class="form">
												<?php $form=$this->beginWidget('CActiveForm', array(
													'id'=>'login-form',
													'enableClientValidation'=>false,
													'clientOptions'=>array(
														'validateOnSubmit'=>true,
													),
													'action'=> Yii::app()->createUrl('/site/login'),
												)); ?>
												<li><?php echo $form->textField($model,'nim', array('class'=>'form-control','placeholder'=>'NIM')); ?></li>
												<li><?php echo $form->passwordField($model,'password', array('class'=>'form-control','placeholder'=>'Password')); ?></li>
												<li>
													<div class="checkbox">
														<label>
															<?php echo $form->checkBox($model,'rememberMe'); ?>
															<?php echo $form->label($model,'rememberMe'); ?>
														</label>
													</div>
												</li>
												<li><input type="submit" class="btn btn-2" name="" value="Login" /></li>
												<?php $this->endWidget(); ?>
											</div>
											<a href="<?php echo Yii::app()->createUrl('site/forgetPass');?>">Lupa Password?</a>
										</ul>
										<?php } ?>
						
									</div>
								</li>
								<li>
									<span class="separator hidden-xs">|</span>
									<?php 
									if(!Yii::app()->user->isGuest)
									{
										echo '<a class="hidden-xs" href="'.Yii::app()->createUrl('users/menu/list').'">'.ucfirst(Yii::app()->user->nama).'</a>';
									}
									else 
									{
										echo '<a href="'.Yii::app()->createUrl('/customer/register').'"><i class="fa fa-edit visible-xs"></i> Registrasi</a>';
									} 
									?>
								</li>
								<li <?php if(Yii::app()->user->isGuest){echo 'class="hidden-xs"';}?>>
									<div class="btn-group">
										<?php 
											if(!Yii::app()->user->isGuest)
											{
												echo	'<a href="#" class="dropdown-toggle" data-toggle="dropdown">
															<i class="fa fa-user"></i>
															<span class="visible-xs">'.ucfirst(Yii::app()->user->nama).'</span>
														</a>';
												echo  	'<ul class="dropdown-menu dropdown-menu-right" role="menu">
															<li><a href="'.Yii::app()->createUrl('users').'"><i class="fa fa-meh-o"></i> Profile</a></li>
															<li><a href="#"><i class="fa fa-cog"></i> Account</a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i> Order Status</a></li>
														</ul>';
											}
											else 
											{
												echo	'<a href="'.Yii::app()->createUrl("/site/login").'">
															<i class="fa fa-user hidden-xs"></i>
														</a>';
											}
										?>														
									</div>
								</li>
							</ul>
							<div class="close-collapse visible-xs"><i class="fa fa-angle-up"></i></div>
							<ul class="social-box hidden-xs">
								<li>
									<div class="social-box-base"><a href=""><i class="fa fa-envelope"></i></a></div>
									<a href="#"><i class="fa fa-envelope"></i></a>
								</li>
								<li>
									<div class="social-box-base"><a href=""><i class="fa fa-facebook"></i></a></div>
									<a href="https://www.facebook.com/fndelbandung" target="_blank" class="color-fb"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<div class="social-box-base"><a href=""><i class="fa fa-twitter"></i></a></div>
									<a href="https://twitter.com/fndel_bdg" target="_blank" class="color-twitter" ><i class="fa fa-twitter"></i></a>
								</li> <!--
								<li>
									<div class="social-box-base"><a href=""><i class="fa fa-google-plus"></i></a></div>
									<a href="#" class="color-gplus" ><i class="fa fa-google-plus"></i></a>
								</li> -->
							</ul>
						</div>
					</nav>
					<div class="modal fade" id="hero-search-box-alert" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									<h4 class="modal-title"><center><i class="fa fa-warning"></i> Field Pencarian belum di isi</center></h4>
								</div>
								<div class="modal-body">
									<center><div class="alert alert-danger" role="alert">Masukkan nama makanan ke kolom pencarian terlebih dahulu</div></center>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<div class="modal fade" id="user-login-alert" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									<h4 class="modal-title"><center><i class="fa fa-warning"></i> Data Login belum lengkap</center></h4>
								</div>
								<div class="modal-body">
									<center><div class="alert alert-danger" role="alert">Masukkan username dan password terlebih dahulu</div></center>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
	
			
			<?php echo $content; ?>
			<!-- Content -->
			
			<footer>
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4">
							<div id="footer-menu">
								<a href="<?php echo Yii::app()->request->baseUrl; ?>">Home</a>&nbsp;|&nbsp;
								<a href="<?php echo Yii::app()->createUrl('site/aboutus'); ?>">About Us</a>&nbsp;|&nbsp;
								<a href="<?php echo Yii::app()->createUrl('site/howtoorder'); ?>">How to Order</a>&nbsp;|&nbsp;
								<a href="<?php echo Yii::app()->createUrl('site/faq'); ?>">FAQ</a>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 no-padding">
							<center>&copy; Copyright 2014 - 2020 FnDel. All Rights Reserved</center>
						</div>
						<div class="col-md-4 col-sm-4">
							<div id="footer-social">
								<a href="https://www.facebook.com/fndelbandung" target="_blank"><span class="social-box-o"><i class="fa fa-facebook"></i></span> facebook</a>
								<a href="https://twitter.com/fndel_bdg" target="_blank"><span class="social-box-o"><i class="fa fa-twitter"></i></span> twitter</a>
								<a href="#"><span class="social-box-o"><i class="fa fa-google-plus"></i></span> google+</a>

							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		
		<div class="device-xs visible-xs"></div>
		<div class="device-sm visible-sm"></div>
		<div class="device-md visible-md"></div>
		
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.2.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fndel.js"></script>
		<script>
  			(function(i,s,o,g,r,a,m){
  				i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  			ga('create', 'UA-56397079-1', 'auto');
  			ga('send', 'pageview');

		</script>
	</body>
</html>