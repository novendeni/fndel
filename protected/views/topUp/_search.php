<?php
/* @var $this TopUpController */
/* @var $model TopUp */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_top_up',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'nim',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'jumlah',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'tanggal',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'update_by',array('span'=>5,'maxlength'=>40)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->