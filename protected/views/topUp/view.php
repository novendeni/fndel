<?php
/* @var $this TopUpController */
/* @var $model TopUp */
?>

<?php
$this->breadcrumbs=array(
	'Top Ups'=>array('index'),
	$model->id_top_up,
);

$this->menu=array(
	array('label'=>'List TopUp', 'url'=>array('index')),
	array('label'=>'Create TopUp', 'url'=>array('create')),
	array('label'=>'Update TopUp', 'url'=>array('update', 'id'=>$model->id_top_up)),
	array('label'=>'Delete TopUp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_top_up),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TopUp', 'url'=>array('admin')),
);
?>

<h1>View TopUp #<?php echo $model->id_top_up; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_top_up',
		'nim',
		'jumlah',
		'tanggal',
		'update_by',
	),
)); ?>