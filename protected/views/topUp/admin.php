<?php
/* @var $this TopUpController */
/* @var $model TopUp */


$this->breadcrumbs=array(
	'Top Ups'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TopUp', 'url'=>array('index')),
	array('label'=>'Create TopUp', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#top-up-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Top Ups</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'top-up-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_top_up',
		'nim',
		'jumlah',
		'tanggal',
		'update_by',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>