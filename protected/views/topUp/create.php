<?php
/* @var $this TopUpController */
/* @var $model TopUp */
?>

<?php
$this->breadcrumbs=array(
	'Top Ups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TopUp', 'url'=>array('index')),
	array('label'=>'Manage TopUp', 'url'=>array('admin')),
);
?>

<h1>Create TopUp</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>