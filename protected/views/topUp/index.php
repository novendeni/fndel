<?php
/* @var $this TopUpController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Top Ups',
);

$this->menu=array(
	array('label'=>'Create TopUp','url'=>array('create')),
	array('label'=>'Manage TopUp','url'=>array('admin')),
);
?>

<h1>Top Ups</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>