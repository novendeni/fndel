<?php
/* @var $this MenuController */
/* @var $data Menu */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_menu')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_menu),array('view','id'=>$data->id_menu)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tenant')); ?>:</b>
	<?php echo CHtml::encode($data->id_tenant); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_menu')); ?>:</b>
	<?php echo CHtml::encode($data->nama_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bahan_dasar_utama')); ?>:</b>
	<?php echo CHtml::encode($data->bahan_dasar_utama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hrg_jual')); ?>:</b>
	<?php echo CHtml::encode($data->hrg_jual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hrg_partner')); ?>:</b>
	<?php echo CHtml::encode($data->hrg_partner); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bumbu')); ?>:</b>
	<?php echo CHtml::encode($data->bumbu); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('foto')); ?>:</b>
	<?php echo CHtml::encode($data->foto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('star')); ?>:</b>
	<?php echo CHtml::encode($data->star); ?>
	<br />

	*/ ?>

</div>