<?php
/* @var $this OperatorController */
/* @var $data Operator */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_operator')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_operator),array('view','id'=>$data->id_operator)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_hp')); ?>:</b>
	<?php echo CHtml::encode($data->no_hp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roles')); ?>:</b>
	<?php echo CHtml::encode($data->roles); ?>
	<br />


</div>