<?php
/* @var $this OperatorController */
/* @var $model Operator */
?>

<?php
$this->breadcrumbs=array(
	'Operators'=>array('index'),
	$model->id_operator,
);

$this->menu=array(
	array('label'=>'List Operator', 'url'=>array('index')),
	array('label'=>'Create Operator', 'url'=>array('create')),
	array('label'=>'Update Operator', 'url'=>array('update', 'id'=>$model->id_operator)),
	array('label'=>'Delete Operator', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_operator),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Operator', 'url'=>array('admin')),
);
?>

<h1>View Operator #<?php echo $model->id_operator; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_operator',
		'nama',
		'password',
		'no_hp',
		'roles',
	),
)); ?>