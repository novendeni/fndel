<?php $this->layout='/layouts/no-column'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - Registrasi'; ?>

<?php 
	$this->breadcrumbs=array(
		'Registrasi',
	);
?>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>

	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-edit"></i> &nbsp;Registrasi</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-7 col-sm-7">
					<div class="form">
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'customer-form',
							'action'=> Yii::app()->createUrl('/customer/register'),
						)); ?>

							<div class="form-group">							
								<?php echo $form->labelEx($model,'nim', array('label'=>'NIM')); ?>
								<?php echo $form->textField($model,'nim', array('class'=>'form-control','placeholder'=>'NIM', 'id'=>'nim')); ?>
								<?php echo $form->error($model,'nim', array('class'=>'alert-text')); ?>
							</div>
							<div class="form-group">							
								<?php echo $form->labelEx($model,'nama', array('label'=>'Nama')); ?>
								<?php echo $form->textField($model,'nama', array('class'=>'form-control','placeholder'=>'Nama')); ?>
								<?php echo $form->error($model,'nama', array('class'=>'alert-text')); ?>
							</div>
							
							<div class="form-group">							
								<?php echo $form->labelEx($model,'email', array('label'=>'Email')); ?>
								<?php echo $form->emailField($model,'email', array('class'=>'form-control','placeholder'=>'your@mail.com')); ?>
								<?php echo $form->error($model,'email', array('class'=>'alert-text')); ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model,'password', array('label'=>'Password')); ?>
								<?php echo $form->passwordField($model,'password', array('class'=>'form-control','placeholder'=>'Password')); ?>
								<?php //echo $form->error($model,'password', array('class'=>'alert-text')); ?>
							</div>
							<div class="form-group">
								<?php //echo $form->labelEx($model,'confirm_pass', array('label'=>'Ulangi Password')); ?>
								<?php echo $form->passwordField($model, 'confirm_pass', array('class'=>'form-control','placeholder'=>'Ulangi Password')); ?>
								<?php echo $form->error($model,'password', array('class'=>'alert-text')); ?>
							</div>
							<div class="form-group form-inline" >
								<?php echo $form->labelEx($model,'nama_kos', array('label'=>'Kost / Tempat Tinggal')); ?>
								<br/>	
								<?php $areas = array_map('ucwords',CHtml::listData(Kost::model()->findAll(array('order'=>'area', 'group'=>'area')),'area','area'));?>
								<?php echo $form->dropDownList($model,'area',$areas, array('prompt'=>'-- Pilih Area --', 'class'=>'form-control', 'style'=>'width: 200px;', 'ajax'=>array('type'=>'POST','url'=>Yii::app()->createUrl('/customer/loadKost'),'update'=>'#Customer_nama_kos','data'=>array('area'=>'js:this.value')))); ?>
								<?php echo $form->dropDownList($model,'nama_kos',array(),array('prompt'=>'-- Pilih Kost --', 'class'=>'form-control', 'style'=>'width: 200px;')); ?>
								<?php echo $form->textField($model,'no_kamar', array('class'=>'form-control','placeholder'=>"Nomor Kamar", 'style'=>'width: 100px;')); ?>
								<?php echo $form->error($model,'no_kamar', array('class'=>'alert-text')); ?>
								<span class="help-block visible-xs-block visible-sm-block"></span>
								<a href="#modal-daftar" data-toggle="modal">Daftarkan Kost-an</a>
							</div>
							<div class="form-group form-inline" >
								<?php echo $form->labelEx($model,'tgl_lhr', array('label'=>'Tempat / Tanggal lahir')); ?>
								<br/>
								<?php echo $form->textField($model,'tempat_lhr', array('class'=>'form-control','placeholder'=>'Tempat Lahir')); ?>
								<?php echo $form->error($model,'tempat_lhr', array('class'=>'alert-text')); ?>
								<span class="help-block visible-xs-block"></span>
								<div class="input-append date date-picker inline">
									<?php echo $form->textField($model,'tgl_lhr', array('class'=>'form-control','placeholder'=>'dd-mm-yyyy', 'data-date'=>'01-01-1995', 'data-date-format'=>'dd-mm-yyyy', 'data-date-viewmode'=>'years')); ?>
									<?php echo $form->error($model,'tgl_lhr', array('class'=>'alert-text')); ?>
								</div>
								
							</div>
							<div class="form-group">							
								<?php echo $form->labelEx($model,'jns_kelamin', array('label'=>'Jenis Kelamin')); ?>
								<?php echo $form->dropDownList($model, 'jns_kelamin', array(
									'L'=>'Laki-laki', 
									'P'=>'Perempuan',
								), array('empty'=>'--Pilih Jenis Kelamin--', 'class'=>'form-control')); ?>
								
							</div>
							<div class="form-group">							
								<?php echo $form->labelEx($model,'no_hp', array('label'=>'Nomor HP')); ?>
								<?php echo $form->textField($model,'no_hp', array('class'=>'form-control','placeholder'=>'Nomor HP')); ?>
								<?php echo $form->error($model,'no_hp', array('class'=>'alert-text')); ?>
							</div>
							<button type="submit" class="btn btn-primary">Daftar</button>
							
							<?php $this->endWidget(); ?>
							<br/><br/>
							Sudah Punya Akun? <a href="<?php echo Yii::app()->createUrl('site/login'); ?>">Login</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<div id="modal-daftar" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4>Pendaftaran Kost-an</h4>
			</div>
			<div class="modal-body">
				<form id="form-daftar" name="form-daftar"  action="<?php echo Yii::app()->createUrl('/kost/daftar');?>" method="POST">
					<table>
						<tr>
							<td><?php echo CHtml::label('Nama',''); ?><span class="required">*</span></td>
							<td><?php echo CHtml::textField('nama','',array('width'=>200));?></td>
						</tr>
						<tr>
							<td><?php echo CHtml::label('Email',''); ?><span class="required">*</span></td>
							<td><?php echo CHtml::textField('email','',array('width'=>200));?></td>
						</tr>
						<tr>
							<td style="width:150px"><?php echo CHtml::label('Nama Kost',''); ?><span class="required">*</span></td>
							<td><?php echo CHtml::textField('nama_kost','',array('width'=>200));?></td>
						</tr>
						<tr>
							<td><?php echo CHtml::label('Area',''); ?><span class="required">*</span></td>
							<td><?php echo CHtml::textField('area','',array('width'=>200));?></td>
						</tr>
						<tr>
							<td><?php echo CHtml::label('Keterangan',''); ?></td>
							<td><?php echo CHtml::textField('ket','',array('cols'=>30));?></td>
						</tr>
					</table>
				</form>
			</div>
			<div class="modal-footer">
				<input type="button" class="btn btn-primary modal-order" name="btn_daftar" id="btn_daftar" value="Daftar" data-dismiss="modal"/>
				<!--<input type="submit" value="pesan" data-dismiss="modal">-->
			</div>
		</div>
	</div>
</div>