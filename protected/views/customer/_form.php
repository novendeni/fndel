<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'customer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'nim',array('span'=>5,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'nama',array('span'=>5,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'nama_kos',array('span'=>5,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'no_kamar',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'area',array('span'=>5,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'tempat_lhr',array('span'=>5,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'tgl_lhr',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'no_hp',array('span'=>5,'maxlength'=>20)); ?>

            <?php echo $form->textFieldControlGroup($model,'email',array('span'=>5,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'sisa_saldo',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'jns_kelamin',array('span'=>5,'maxlength'=>2)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->