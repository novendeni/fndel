<?php
/* @var $this KostController */
/* @var $model Kost */

$this->breadcrumbs=array(
	'Kosts'=>array('index'),
	$model->id_kost,
);

$this->menu=array(
	array('label'=>'List Kost', 'url'=>array('index')),
	array('label'=>'Create Kost', 'url'=>array('create')),
	array('label'=>'Update Kost', 'url'=>array('update', 'id'=>$model->id_kost)),
	array('label'=>'Delete Kost', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_kost),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Kost', 'url'=>array('admin')),
);
?>

<h1>View Kost #<?php echo $model->id_kost; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_kost',
		'nama_kost',
		'area',
	),
)); ?>
