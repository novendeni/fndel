<?php
/* @var $this KostController */
/* @var $model Kost */

$this->breadcrumbs=array(
	'Kosts'=>array('index'),
	$model->id_kost=>array('view','id'=>$model->id_kost),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kost', 'url'=>array('index')),
	array('label'=>'Create Kost', 'url'=>array('create')),
	array('label'=>'View Kost', 'url'=>array('view', 'id'=>$model->id_kost)),
	array('label'=>'Manage Kost', 'url'=>array('admin')),
);
?>

<h1>Update Kost <?php echo $model->id_kost; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>