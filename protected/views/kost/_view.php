<?php
/* @var $this KostController */
/* @var $data Kost */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kost')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_kost), array('view', 'id'=>$data->id_kost)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kost')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area')); ?>:</b>
	<?php echo CHtml::encode($data->area); ?>
	<br />


</div>