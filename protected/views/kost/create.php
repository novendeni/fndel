<?php
/* @var $this KostController */
/* @var $model Kost */

$this->breadcrumbs=array(
	'Kosts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Kost', 'url'=>array('index')),
	array('label'=>'Manage Kost', 'url'=>array('admin')),
);
?>

<h1>Create Kost</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>