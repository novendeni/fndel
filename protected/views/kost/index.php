<?php
/* @var $this KostController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kosts',
);

$this->menu=array(
	array('label'=>'Create Kost', 'url'=>array('create')),
	array('label'=>'Manage Kost', 'url'=>array('admin')),
);
?>

<h1>Kosts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
