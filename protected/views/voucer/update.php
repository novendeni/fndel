<?php
/* @var $this VoucerController */
/* @var $model Voucer */

$this->breadcrumbs=array(
	'Voucers'=>array('index'),
	$model->id_voucer=>array('view','id'=>$model->id_voucer),
	'Update',
);

$this->menu=array(
	array('label'=>'List Voucer', 'url'=>array('index')),
	array('label'=>'Create Voucer', 'url'=>array('create')),
	array('label'=>'View Voucer', 'url'=>array('view', 'id'=>$model->id_voucer)),
	array('label'=>'Manage Voucer', 'url'=>array('admin')),
);
?>

<h1>Update Voucer <?php echo $model->id_voucer; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>