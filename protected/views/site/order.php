<?php $this->pageTitle=Yii::app()->name . ' - Order'; ?>

<?php 
	$this->breadcrumbs=array(
		Pencarian => Yii::app()->createUrl('site/search'),
		'Order',
	);
?>

<div class="page-title">
	<div class="title">Detail Makanan</div>
</div>
<div id="menu-details">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-1">
			<div class="title title-1"><?php echo $model->nama_menu; ?></div>
			<div class="status">
				<span class="likes">
					<i class="fa fa-thumbs-up"></i> 300 Likes
				</span>
				<span class="separator">|</span>
				<span class="rating">
					<span class="rating-avg" value="<?php echo $model->star;?>"></span>
				</span>
			</div>
			<div class="description">
				<div class="title">Deskripsi</div>
				<p>
					Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius
				</p>
			</div>
			<div class="price">
				<span class="text">Harga : </span>
				<span class="number">Rp. <?php echo $model->hrg_jual; ?></span>
			</div>
			<?php 
				if(!Yii::app()->user->isGuest){
					if($available)
						echo '<a href="#form-modal" class="btn btn-4 btn-lg pesan" id="btn-order" data-toggle="modal" data-id-menu='.$model->id_menu.'data-nama-menu='.$model->nama_menu.'data-hrg-menu='.$model->hrg_jual.'>Pesan</a>';
					else
						echo '<a href="#" class="btn btn-4 btn-lg pesan" id="btn-order">Out Of Stock</a>';
				} else 
					echo '<a href="'.Yii::app()->createUrl('/site/login').'" class="btn btn-4 btn-lg pesan" id="btn-order">Pesan</a>';
			?>
			<!--<a href="#form-modal" class="btn btn-4 btn-lg pesan" id="btn-order" data-toggle="modal" data-id-menu="<?php echo $model->id_menu;?>" data-nama-menu="<?php echo $model->nama_menu;?>" data-hrg-menu="<?php echo $model->hrg_jual;?>">Pesan</a><br/>-->
			<br />
			<a href="<?php echo Yii::app()->createUrl("users/menuPreference/tambah/id/".$model->id_menu);?>" class="btn btn-2 btn-lg" id="btn-add">Tambahkan ke Favorite</a>
		</div>
		<div class="col-md-6 col-sm-6 col-2">
			<div class="image">
				<?php echo CHtml::image(Yii::app()->controller->createUrl('menu/loadImage', array('id'=>$model->id_menu))); ?>
			</div>
			<div class="social-group">
				<button class="btn btn-primary">Like</button>
				<span class="float-right">
					<button class="btn btn-success"><i class="fa fa-share-alt"></i></button>
					<button class="btn btn-info"><i class="fa fa-twitter"></i></button>
					<button class="btn btn-primary"><i class="fa fa-facebook"></i></button>
					<button class="btn btn-danger"><i class="fa fa-google-plus"></i></button>
				</span>
					
			</div>
			<div class="review-group">
				<div id="input-review" class="panel-collapse collapse">
					<h4><b>Tulis Review</b></h4>
					<form action="" method="">
						<input type="text" class="form-control" placeholder="Nama">
						<input type="url" class="form-control" placeholder="URL (optional)">
						<input type="email" class="form-control" placeholder="Email (optional)">
						<h4>Rate :</h4>
						<div class="rating">
							<span><input type="radio" name="rating" value="1" /><label class="rating-label"><i class="fa fa-star"></i></label></span>
							<span><input type="radio" name="rating" value="2" /><label class="rating-label"><i class="fa fa-star"></i></label></span>
							<span><input type="radio" name="rating" value="3" /><label class="rating-label"><i class="fa fa-star"></i></label></span>
							<span><input type="radio" name="rating" value="4" /><label class="rating-label"><i class="fa fa-star"></i></label></span>
							<span><input type="radio" name="rating" value="5" /><label class="rating-label"><i class="fa fa-star"></i></label></span>

						</div>
						<div class="clear"></div>
						<textarea class="form-control" rows="3" placeholder="Review..."></textarea>
						<button type="submit" class="btn btn-1">Submit</button>
					</form><br/>
				</div>
				<div class="title">
					Review
					<button class="btn btn-2" data-toggle="collapse" data-parent="#accordion" href="#input-review">Tulis review / Rate</button>
				</div>
				<div class="review-box">
					<div class="name"><a href="#">Someone#1</a></div>
					<div class="rating-date">
						<span class="rating-avg" value="3.5"></span>
						<span class="date">17/4/2014</span>
					</div>
					<div class="review-text">
						<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas</p>
					</div>
				</div>
				<div class="review-box">
					<div class="name">Someone#1</div>
					<div class="rating-date">
						<span class="rating-avg" value="4.5"></span>
						<span class="date">17/4/2014</span>
					</div>
					<div class="review-text">
						<p>Sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas</p>
					</div>
				</div>
				<div class="review-box">
					<div class="name">Someone#1</div>
					<div class="rating-date">
						<span class="rating-avg" value="2"></span>
						<span class="date">17/4/2014</span>
					</div>
					<div class="review-text">
						<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas</p>
					</div>
				</div>
			</div>
			<ul class="pagination">
				<li><a href="#">&laquo;</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">&raquo;</a></li>
			</ul>
		</div>
		
	</div>
</div>

<div id="form-modal" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4>Pilih Jumlah Pesanan</h4>
			</div>
			<div class="modal-body">
				<form id="form-pesan" action="<?php echo Yii::app()->createUrl('users/menu/cart');?>" method="POST" name="form-pesan" >
					<fieldset>
						<input type="hidden" name="id-menu" id="id-menu" value=""/>
						<input type="hidden" name="hrg-menu" id="hrg-menu" value=""/>
						<input type="text" name="nama-menu" id="nama-menu" disabled="true" value=""/>
						Jumlah 
						<select id="jml-menu">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
				<input type="button" class="btn btn-primary modal-order" name="submit_id" id="btn_id" value="Pesan" data-dismiss="modal"/>
				<!--<input type="submit" value="pesan" data-dismiss="modal">-->
			</div>
		</div>
	</div>
</div>