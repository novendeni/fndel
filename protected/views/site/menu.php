<?php 
	$this->breadcrumbs=array(
		'Pencarian',
	);
?>
		
<?php
	if(!isset($user)){
		$user = new LoginForm;
	}
?>

<?php $this->beginContent('//layouts/main_as'); ?>

	<section id="search-box" class="collapse navbar-collapse">
		<div class="container">
			<form action="<?php echo Yii::app()->createUrl('site/search'); ?>" method="GET">
				<div class="row">
					<div class="col-md-9 col-sm-9">
						<div class="search-group">
							<div class="radio-select">
								<div class="options">
									<div><input type="radio" name="Pencarian[kategori]" value="menu" />Makanan</div>
									<div><input type="radio" name="Pencarian[kategori]" value="warung" />Warung</div>
								</div>	
							</div>
							<input type="text" name="Pencarian[nilai]" placeholder="Search..."/>
						</div>
					</div>
					<div class="col-md-3 col-sm-3">
						<div class="button">
							<button type="submit" class="btn-4"><i class="fa fa-search"></i> Cari</button>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="close-collapse visible-xs"><i class="fa fa-angle-up"></i></div>
	</section>
			
	<section id="content">
		<div class="container">
				
			<?php if(isset($this->breadcrumbs)):
				if ( Yii::app()->controller->route !== 'site/index' )
				$this->breadcrumbs = array_merge(array (Yii::t('zii','Home')=>Yii::app()->homeUrl), $this->breadcrumbs);

				$this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
					'homeLink'=>false,
					'tagName'=>'ul',
					'separator'=>'',
					'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
					'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
					'htmlOptions'=>array ('class'=>'breadcrumb-2')
				)); ?><!-- breadcrumbs -->
			<?php endif; ?>
					
			<div class="row">
				<div class="col-md-3 col-sm-3">
					<div id="side-menu" class="list-group">
						<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#menu-makanan">
							Menu
						</a>
						<div id="menu-makanan" class="panel-collapse collapse in">
							<!--<a href="<?php //echo Yii::app()->createUrl('site/terlaris');?>" class="list-group-item"><i class="fa fa-star"></i> Terlaris</a>-->
							<?php echo CHtml::ajaxLink(
									'<i class="fa fa-star"></i>Terlaris',
									 Yii::app()->createUrl('site/terlaris'),
									array(
										'update'=>'#result'
									),
									array(
										'class'=>'list-group-item',
										'href'=>Yii::app()->createUrl('site/terlaris'),
										'id'=>'send-link-'.uniqid(),
									)
							 );?>
							<!--<a href="<?php //echo Yii::app()->createUrl('site/rekomendasi');?>" class="list-group-item"><i class="fa fa-thumbs-up"></i> Rekomendasi</a>-->
							<?php echo CHtml::ajaxLink(
									'<i class="fa fa-thumbs-up"></i>Rekomendasi',
									 Yii::app()->createUrl('site/rekomendasi'),
									array(
										'update'=>'#result'
									),
									array(
										'class'=>'list-group-item',
										'href'=>Yii::app()->createUrl('site/rekomendasi'),
										'id'=>'send-link-'.uniqid(),
									)
							 );?>
							<!--<a href="#" class="list-group-item"><i class="fa fa-plus-circle"></i> Menu Baru</a>-->
							<?php echo CHtml::ajaxLink(
									'<i class="fa fa-plus-circle"></i>Menu Baru',
									 Yii::app()->createUrl('site/terbaru'),
									array(
										'update'=>'#result'
									),
									array(
										'class'=>'list-group-item',
										'href'=>Yii::app()->createUrl('site/terbaru'),
										'id'=>'send-link-'.uniqid(),
									)
							 );?>
							<!--<a href="<?php //echo Yii::app()->createUrl('site/promo');?>" class="list-group-item"><i class="fa fa-bookmark"></i> Promo</a>-->
							<?php /*echo CHtml::ajaxLink(
									'<i class="fa fa-bookmark"></i>Promo',
									 Yii::app()->createUrl('site/promo'),
									array(
										'update'=>'#result'
									),
									array(
										'class'=>'list-group-item',
										'href'=>Yii::app()->createUrl('site/promo'),
										'id'=>'send-link-'.uniqid(),
									)
							 );*/?>
						</div> 
                        <!-- remove broken link
						<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#kategori-makanan">
							Kategori
						</a>
						<div id="kategori-makanan" class="panel-collapse collapse in">
							<a href="#" class="list-group-item"><i class="fa fa-cutlery"></i> Makanan</a>
							<a href="#" class="list-group-item"><i class="fa fa-glass"></i> Minuman</a>
							<a href="#" class="list-group-item"><i class="fa fa-archive"></i> Camilan</a>
						</div> -->
					</div>
						</div>
						<div class="col-md-9 col-sm-9 left">
							<div id="result">
								<?php $this->renderPartial('_menu',array('models'=>$models,'pages'=>$pages));?>
							</div>
						</div>
					</div>
				</div>
			</section>
<?php $this->endContent(); ?>

<?php
								$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
									'id'=>'order_msg',
									'options'=>array(
										'autoOpen'=>false,
										'width'=>500,
										'height'=>150
									)
								));
									echo 'Pesanan anda telah masuk keranjang belanja. Silahkan klik <b>order di sebelah kiri</b> untuk memproses transaksi';
								$this->endWidget('zii.widgets.jui.CJuiDialog');
							?>
							
							<div id="login-modal" class="modal fade in">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4>Login</h4>
										</div>
										<div class="modal-body">
											<p class="text-warning">Untuk melakukan pemesanan anda harus login terlebih dahulu.</p>
											<div class="form">
													<?php $form=$this->beginWidget('CActiveForm', array(
														'id'=>'login-widget',
														'enableClientValidation'=>true,
														'clientOptions'=>array(
															'validateOnSubmit'=>true,
														),
														'action'=> Yii::app()->createUrl('/site/login'),
													)); ?>
													
														<div class="form-group">							
															<?php echo $form->labelEx($user,'nim', array('label'=>'Username / NIM')); ?>
															<?php echo $form->textField($user,'nim', array('class'=>'form-control','placeholder'=>'Username / NIM')); ?>
															<?php echo $form->error($user,'nim', array('class'=>'alert-text')); ?>
														</div>
														<div class="form-group">
															<?php echo $form->labelEx($user,'password', array('label'=>'Password')); ?>
															<?php echo $form->passwordField($user,'password', array('class'=>'form-control','placeholder'=>'Password')); ?>
															<?php echo $form->error($user,'password', array('class'=>'alert-text')); ?>
														</div>
														<div class="checkbox">
															<label>
																<?php echo $form->checkBox($user,'rememberMe'); ?>
																<?php echo $form->label($user,'rememberMe'); ?>
																<?php echo $form->error($user,'rememberMe', array('class'=>'alert-text')); ?>
															</label>
														</div>
														<!--<button type="submit" class="btn btn-primary" data-dismiss="modal">Submit</button>-->
														
														<?php $this->endWidget(); ?>
														<br/><br/> 
														<a href="<?php echo Yii::app()->createUrl('site/forgetPass');?>">Lupa Password?</a> | Belum Punya Akun? <a href="<?php echo Yii::app()->createUrl('/customer/register'); ?>">Register</a>
												</div>
										</div>
										<div class="modal-footer">
											<input type="button" class="btn btn-primary" name="submit_id" id="submit_id" value="Login" data-dismiss="modal"/>
											<!--<input type="submit" value="pesan" data-dismiss="modal">-->
										</div>
									</div>
								</div>
							</div>

							<div id="form-modal" class="modal fade in">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4>Pilih Jumlah Pesanan</h4>
										</div>
										<div class="modal-body">
											<form id="form-pesan" name="form-pesan"  action="<?php echo Yii::app()->createUrl('users/menu/cart');?>" method="POST">
												<fieldset>
													<input type="hidden" name="id-menu" id="id-menu" value=""/>
													<input type="hidden" name="hrg-menu" id="hrg-menu" value=""/>
													<input type="text" name="nama-menu" id="nama-menu" readOnly="true" value=""/>
													&nbsp;&nbsp;Jumlah 
													<select id="jml-menu" name="jml-menu">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</fieldset>
											</form>
										</div>
										<div class="modal-footer">
											<input type="button" class="btn btn-primary modal-order" name="submit_id" id="btn_id" value="Pesan" data-dismiss="modal"/>
											<!--<input type="submit" value="pesan" data-dismiss="modal">-->
										</div>
									</div>
								</div>
							</div>