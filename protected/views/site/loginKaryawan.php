<?php $this->layout='no-column'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - Login'; ?>

<?php 
	$this->breadcrumbs=array(
		'Login',
	);
?>

	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-lock"></i> &nbsp;Login</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="form">
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'login-form',
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
							),
							'action'=> Yii::app()->createUrl('/site/karyawan'),
						)); ?>
						
							<div class="form-group">							
								<?php echo $form->labelEx($model,'username', array('label'=>'Username')); ?>
								<?php echo $form->textField($model,'username', array('class'=>'form-control','placeholder'=>'Username')); ?>
								<?php echo $form->error($model,'username', array('class'=>'alert-text')); ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($model,'password', array('label'=>'Password')); ?>
								<?php echo $form->passwordField($model,'password', array('class'=>'form-control','placeholder'=>'Password')); ?>
								<?php echo $form->error($model,'password', array('class'=>'alert-text')); ?>
							</div>
							<div class="checkbox">
								<label>
									<?php echo $form->checkBox($model,'rememberMe'); ?>
									<?php echo $form->label($model,'rememberMe'); ?>
									<?php echo $form->error($model,'rememberMe', array('class'=>'alert-text')); ?>
								</label>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
							
							<?php $this->endWidget(); ?>
					</div>
				</div>
			</div>

		</div>
	</div>