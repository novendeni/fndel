<?php $this->layout='no-column'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - Lupa Password'; ?>

<?php 
	$this->breadcrumbs=array(
		'Lupa Password',
	);
?>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('danger')):?>
    <div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('danger'); ?>
    </div>
<?php endif; ?>

<div class="col-md-6 col-sm-6">
	<div class="form">
		<?php echo CHtml::beginForm(Yii::app()->createUrl('/site/forgetPass'),'POST'); ?>
			<?php echo CHtml::label('Username',''); ?>
			<?php echo CHtml::textField('username','', array('class'=>'form-control','placeholder'=>'Username / NIM')); ?>
			<?php echo CHtml::label('email',''); ?>
			<?php echo CHtml::emailField('email','', array('class'=>'form-control','placeholder'=>'email')); ?>
			<br/>
			<?php echo CHtml::submitButton('Reset Password',array('id'=>'btn-ok','class'=>'btn btn-primary')); ?>
		<?php echo CHtml::endForm(); ?>
	</div>
</div>