<?php $this->layout='column1'; ?>
<?php 
	$this->breadcrumbs=array(
		'report',
	);
?>
			
			
<div class="page-title">
	<div class="title">Report Problem</div>
</div>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>

<div class="row">
	<div class="form" role="form">
		<?php echo CHtml::beginForm(Yii::app()->createUrl('/site/report'),'POST'); ?>
			<?php echo CHtml::label('Nama',''); ?>
			<?php echo CHtml::textField('nama','', array('class'=>'form-control','placeholder'=>'Nama')); ?>
			<?php echo CHtml::label('email',''); ?>
			<?php echo CHtml::emailField('email','', array('class'=>'form-control','placeholder'=>'email')); ?>
			<?php echo CHtml::label('report',''); ?>
			<?php echo CHtml::textArea('report','', array('class'=>'form-control','placeholder'=>'email','maxlength'=>500, 'rows'=>7, 'cols'=>80)); ?>
			<br/>
			<?php echo CHtml::submitButton('Send Report',array('id'=>'btn-ok','class'=>'btn btn-primary')); ?>
		<?php echo CHtml::endForm(); ?>
	</div>
</div>