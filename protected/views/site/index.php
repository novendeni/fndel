<?php 
	if(isset(Yii::app()->user->roles)){
		if(Yii::app()->user->roles === 'operator')
			$this->redirect(array('/operator'));
	}
?>
<?php $this->layout = "//layouts/main";?>		
			<section id="hero">
				<div class="container">
					<div id="hero-phone-text" class="hidden-md hidden-lg"><i class="fa fa-phone"></i> 0857 9413 6415</div>
					<div id="hero-text">
						<marquee>Jam Operasional : Senin - Jum'at Pukul 10.00 - 21.00 WIB(Last Order Pukul 20.00 WIB).</marquee>
						Lapar? Delivery Order aja <i class="fa fa-smile-o"></i> </br>
						<span class="sub-hero-text">
							...
							<span style="color:#FFFF00; font-weight: bold;"><?php echo Menu::model()->count();?> Menu</span>
							 siap diantar ke kamar anda...
						</span>
					</div>
					<div id="hero-search-box">
						<form action="<?php echo Yii::app()->createUrl('site/search'); ?>" method="GET">
							<div class="row">
								<div class="col-md-9 col-sm-9 no-padding">
									<div class="search-group">
										<div class="radio-select">
											<div class="options">
												<div><input type="radio" name="Pencarian[kategori]" value="menu" />Makanan</div>
												<div><input type="radio" name="Pencarian[kategori]" value="warung" />Warung</div>
											</div>	
										</div>
										<input type="text" name="Pencarian[nilai]" placeholder="Search..."/>
									</div>
								</div>
								<div class="col-md-3 col-sm-3 no-padding">
									<div class="button">
										<button type="submit"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>	
				</div>
			</section>
			<section id="content-home">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4">
							<div class="featured-menu">
								<div class="thumbnail">
									<a href="<?php echo Yii::app()->createUrl('site/menu'); ?>">
										<div class="image">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/menu2.jpg"></img>
										</div>
										<div class="icon">
											<i class="ci stars"></i>
										</div>
										<div class="title">
											Menu Makanan
										</div>
									</a>
								</div>
								<div class="text">
									<p>Menu - menu makanan yang tersedia.</p>
								</div>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-4">
							<div class="featured-menu">
								<div class="thumbnail">
									<a href="<?php echo Yii::app()->createUrl('site/rekomendasi'); ?>">
										<div class="image">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/menu1.jpg"></img>
										</div>
										<div class="icon">
											<i class="ci thumbs-up"></i>
										</div>
										<div class="title">
											Menu Rekomendasi
										</div>
									</a>
								</div>	
								<div class="text">
									<p>Menu-menu pilihan yang kami rekomendasikan karena rasa nya yang yummy.</p>
								</div>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-4">
							<div class="featured-menu">
								<div class="thumbnail">
									<a href="<?php echo Yii::app()->createUrl('site/voucher'); ?>">
										<div class="image">
											<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/Voucher.png"></img>
										</div>
										<div class="icon">
											<i class="ci promo"></i>
										</div>
										<div class="title">
											Voucher MyNet
										</div>
									</a>
								</div>
								<div class="text">
									<p>Voucher Kuota dan Unlimited MyNet.<i class="fa fa-smile-o"></i></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>