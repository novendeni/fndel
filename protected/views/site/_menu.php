<div class="page-title">
	<div class="title">Hasil Pencarian : <?php echo $pages->itemCount;?> menu ditemukan</div> <!--
	<div class="option">
		<span class="hidden-xs">Urutkan Berdasarkan</span>
		<select name="sorting" onchange="">
			<option value="Harga">Harga</option>
			<option value="Popularitas">Popularitas</option>
			<option value="Jumlah Pesanan">Jumlah Pesanan</option>
			<option value="Terbaru">Terbaru</option>
		</select>
	</div> -->
</div>
<div class="menu-grid list">
	<div class="view-option">
		<button id="btn-list" class="btn btn-default active"><i class="fa fa-list-ul"></i></button>
		<button id="btn-grid" class="btn btn-default"><i class="fa fa-th"></i></button>
	</div>
	
	<?php if($models != null): ?>
		<?php 
              
                foreach($models as $model): ?>
			<?php $orderPage = Yii::app()->createUrl("site/order/id/".$model->id_menu); ?>
			<div class="box-menu">
				<div class="image">
					<a href="#<?php //echo $orderPage ?>">
						<!--<img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/menu2.jpg"/>-->
						<?php //echo CHtml::image(Yii::app()->controller->createUrl('menu/loadImage', array('id'=>$model->id_menu))); ?>
						<?php 
							                                                        
                                                        $thumbFile = $_SERVER['DOCUMENT_ROOT'].'/_FnDel/images/thumb/product_'.$model->id_menu . '_thumb.jpg';
                                                         $thumbURL =  Yii::app()->getBaseUrl(true).'/images/thumb/product_'.$model->id_menu . '_thumb.jpg';
                                                       
                                                        if(!file_exists($thumbFile)){
                                                              // link to default image 
                                                               $thumbURL =    Yii::app()->getBaseUrl(true).'/images/menu_default.jpg';
							}
                                                        
                                                        
						?>
						
                                            <img src="<?php echo $thumbURL; ?>" width="<?php echo Yii::app()->params['productThumbSize'];?>px" height="<?php echo Yii::app()->params['productThumbSize'];?>px" alt=" "/>
					</a>
				</div>
				<div class="title">
					<!--<a href="<?php //echo $orderPage ?>"><?php //echo $model->nama_menu; ?></a>-->
					<?php echo $model->nama_menu;?>
					<a href="<?php echo Yii::app()->createUrl('/tenant/menu',array('id'=>$model->id_tenant)); ?>" class="tenant"><i class="fa fa-home"></i> <?php echo $model->idTenant->nama_warung; ?></a>
				</div>
				<div class="price currency"><?php echo $model->hrg_jual; ?></div>
				<div class="status">
					<span class="likes">
						<a href="#"><i class="fa fa-thumbs-up"></i> 300 Likes</a>
					</span>
					<span class="separator">|</span>
					<span class="rating">
						<span class="rating-avg" value="<?php echo $model->star;?>"></span>
					</span>
				</div>
				<div class="button">
					<?php 
						if(Yii::app()->user->isGuest){
							if($model->available())
								echo '<a href="#login-modal" class="btn-1 pesan" id="btn-login" data-toggle="modal">Pesan</a>';
							else
								echo '<p class="btn btn-1 pesan" style="font-size:12px">Out Of Stock</p>';
						} else {
							if($model->available()): ?>
								<a href="#form-modal" class="btn-1 pesan" id="btn-order" data-toggle="modal" data-id-menu="<?php echo $model->id_menu;?>" data-hrg-menu="<?php echo $model->hrg_jual;?>" data-nama-menu="<?php echo $model->nama_menu;?>">Pesan</a>
							<?php else : ?>
								<p class="btn btn-1 pesan" style="font-size:12px">Out Of Stock</p>
							<?php endif;
						}
					?>
				</div>
			</div>
		<?php endforeach; ?>
	<?php else :?>
		Maaf data Tidak ditemukan
	<?php endif; ?>
</div>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'maxButtonCount'=>5,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>
