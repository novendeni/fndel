<?php
/* @var $this SpesialMenuController */
/* @var $model SpesialMenu */
?>

<?php
$this->breadcrumbs=array(
	'Spesial Menus'=>array('index'),
	$model->id_spesial_menu,
);

$this->menu=array(
	array('label'=>'List SpesialMenu', 'url'=>array('index')),
	array('label'=>'Create SpesialMenu', 'url'=>array('create')),
	array('label'=>'Update SpesialMenu', 'url'=>array('update', 'id'=>$model->id_spesial_menu)),
	array('label'=>'Delete SpesialMenu', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_spesial_menu),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SpesialMenu', 'url'=>array('admin')),
);
?>

<h1>View SpesialMenu #<?php echo $model->id_spesial_menu; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_spesial_menu',
		'id_menu',
		'tanggal_awal',
		'update_by',
		'occurence',
	),
)); ?>