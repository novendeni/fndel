<?php
/* @var $this SpesialMenuController */
/* @var $model SpesialMenu */
?>

<?php
$this->breadcrumbs=array(
	'Spesial Menus'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SpesialMenu', 'url'=>array('index')),
	array('label'=>'Manage SpesialMenu', 'url'=>array('admin')),
);
?>

<h1>Create SpesialMenu</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>