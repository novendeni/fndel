<?php
/* @var $this SpesialMenuController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Spesial Menus',
);

$this->menu=array(
	array('label'=>'Create SpesialMenu','url'=>array('create')),
	array('label'=>'Manage SpesialMenu','url'=>array('admin')),
);
?>

<h1>Spesial Menus</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>