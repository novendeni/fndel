<?php

/**
 * This is the model class for table "tbl_transaksi".
 *
 * The followings are the available columns in table 'tbl_transaksi':
 * @property integer $id_transaksi
 * @property integer $id_kurir
 * @property string $nim
 * @property string $tanggal
 * @property integer $total
 * @property integer $nominal_pembayaran
 * @property string $jam_pesan
 * @property string $jam_terkirim
 * @property integer $status
 * @property integer $rating
 *
 * The followings are the available model relations:
 * @property Kurir $idKurir
 * @property Customer $nim0
 * @property TransaksiDetail[] $transaksiDetails
 */
class Transaksi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_transaksi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nim, tanggal, status, rating', 'required'),
			array('id_kurir, total, nominal_pembayaran, status, rating', 'numerical', 'integerOnly'=>true),
			array('nim', 'length', 'max'=>40),
			array('jam_pesan, jam_terkirim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_transaksi, id_kurir, nim, tanggal, total, nominal_pembayaran, jam_pesan, jam_terkirim, status, rating', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKurir' => array(self::BELONGS_TO, 'Kurir', 'id_kurir'),
			'nim0' => array(self::BELONGS_TO, 'Customer', 'nim'),
			'transaksiDetails' => array(self::HAS_MANY, 'TransaksiDetail', 'id_transaksi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_transaksi' => 'Id Transaksi',
			'id_kurir' => 'Id Kurir',
			'nim' => 'Nim',
			'tanggal' => 'Tanggal',
			'total' => 'Total',
			'nominal_pembayaran' => 'Nominal Pembayaran',
			'jam_pesan' => 'Jam Pesan',
			'jam_terkirim' => 'Jam Terkirim',
			'status' => 'Status',
			'rating' => 'Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_transaksi',$this->id_transaksi);
		$criteria->compare('id_kurir',$this->id_kurir);
		$criteria->compare('nim',$this->nim,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('total',$this->total);
		$criteria->compare('nominal_pembayaran',$this->nominal_pembayaran);
		$criteria->compare('jam_pesan',$this->jam_pesan,true);
		$criteria->compare('jam_terkirim',$this->jam_terkirim,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('rating',$this->rating);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transaksi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
