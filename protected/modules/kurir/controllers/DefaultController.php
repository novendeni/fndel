<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->layout = '/layouts/column2';
		$this->render('index');
	}
}