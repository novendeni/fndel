<?php /* @var $this Controller */ ?>
<?php Yii::app()->bootstrap->register(); //load bootstrap?>

<!DOCTYPE html">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<!-- blueprint CSS framework -->
	<!--
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	-->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<!-- 
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	-->
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

	<body class='layout-main'>
		<!-- Navbar -->
		<?php 
			$this->widget('bootstrap.widgets.TbNavbar', array(
				//'brandLabel' => 'Title',
				'display' => null, // default is static to top
				'items' => array(
					/*array(
						'class' => 'bootstrap.widgets.TbNav',
						'items' => array(
							array('label'=>'Pendaftaran', 'url'=>array('/pengajuan/create')),
							array('label'=>'List Calon Bayi', 'url'=>array('/kandidatbayi/index')),
							array('label'=>'Rekapitulasi Calon Bayi', 'url'=>array('/kandidatbayi/rekap')),
						),
					),
					array(
						'class' => 'bootstrap.widgets.TbNav',
						'items' => array(
							TbHtml::navbarMenuDivider(),
							array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
							array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
						),
						'htmlOptions'=>array(
							'class'=>'pull-right',
						),
					),*/
				),
			)); 
		?>
		<!-- Content -->
		<?php echo $content; ?>

		<!-- Footer -->
		<footer class='footer' style='text-align: center;'>
			<div class='container'>
				Copyright &copy; <?php echo date('Y'); ?> by MyNet<br/>
				All Rights Reserved.<br/>
			</div>
		</footer>
	</body>
</html>
