<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
<div class="row">
    <div class="span3">
        <?php
			$this->widget('bootstrap.widgets.TbNav', array(
				'type' => TbHtml::NAV_TYPE_LIST,
				'items' => array(
					array('label'=>'Profil'),
					array('label'=>'Lihat Profil', 'url'=>array('/kurir/kurir/view')),
					array('label'=>'Edit Profil', 'url'=>array('/kurir/kurir/update')),
					array('label'=>'History Transaksi', 'url'=>array('/kurir/transaksi/admin')),
					TbHtml::menuDivider(),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
				)
			));
        ?>
    </div>
	
	<div class="span9">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
</div>
<?php $this->endContent(); ?>