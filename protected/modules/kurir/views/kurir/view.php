<?php
/* @var $this KurirController */
/* @var $model Kurir */
?>

<?php
$this->breadcrumbs=array(
	'Kurirs'=>array('index'),
	$model->id_kurir,
);

$this->menu=array(
	array('label'=>'List Kurir', 'url'=>array('index')),
	array('label'=>'Create Kurir', 'url'=>array('create')),
	array('label'=>'Update Kurir', 'url'=>array('update', 'id'=>$model->id_kurir)),
	array('label'=>'Delete Kurir', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_kurir),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Kurir', 'url'=>array('admin')),
);
?>

<h1>View Kurir #<?php echo $model->id_kurir; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_kurir',
		'nama',
		'password',
		'tgl_lahir',
		'no_hp',
		'bank',
		'no_rekening',
		'roles',
	),
)); ?>