<?php
/* @var $this KurirController */
/* @var $model Kurir */
?>

<?php
$this->breadcrumbs=array(
	'Kurirs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Kurir', 'url'=>array('index')),
	array('label'=>'Manage Kurir', 'url'=>array('admin')),
);
?>

<h1>Create Kurir</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>