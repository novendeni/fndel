<?php
/* @var $this TransaksiController */
/* @var $data Transaksi */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_transaksi')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_transaksi),array('view','id'=>$data->id_transaksi)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kurir')); ?>:</b>
	<?php echo CHtml::encode($data->id_kurir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nim')); ?>:</b>
	<?php echo CHtml::encode($data->nim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nominal_pembayaran')); ?>:</b>
	<?php echo CHtml::encode($data->nominal_pembayaran); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_pesan')); ?>:</b>
	<?php echo CHtml::encode($data->jam_pesan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_terkirim')); ?>:</b>
	<?php echo CHtml::encode($data->jam_terkirim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rating')); ?>:</b>
	<?php echo CHtml::encode($data->rating); ?>
	<br />

	*/ ?>

</div>