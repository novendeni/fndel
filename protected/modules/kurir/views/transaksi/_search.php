<?php
/* @var $this TransaksiController */
/* @var $model Transaksi */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_transaksi',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_kurir',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'nim',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'tanggal',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'total',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'nominal_pembayaran',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'jam_pesan',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'jam_terkirim',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'status',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'rating',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->