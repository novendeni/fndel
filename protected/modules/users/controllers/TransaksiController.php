<?php

class TransaksiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column-user';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'history','simpan','test'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Transaksi;
		$model->nim = Yii::app()->user->nim;
		$model->tanggal = date('Y-m-d', strtotime(date('Y-m-d H:i:s')) + (7 * 60 * 60));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if (isset($_POST['Transaksi'])) {
			$model->attributes=$_POST['Transaksi'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_transaksi));
			}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Transaksi']))
		{
			$model->attributes=$_POST['Transaksi'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_transaksi));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Transaksi');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Transaksi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Transaksi']))
			$model->attributes=$_GET['Transaksi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Transaksi the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Transaksi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Transaksi $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='transaksi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSimpan()
	{
		if(isset($_POST['hrg-total'])){
			//if($_POST['hrg-total'] <= Yii::app()->user->saldo){
				if($_POST['hrg-total'] >= 10000){
					$user = Customer::model()->findByPK(Yii::app()->user->nim);
					//$user->sisa_saldo = $user->sisa_saldo - $_POST['hrg-total'];
					//$user->save(false);
					//Yii::app()->user->saldo = $user->sisa_saldo;

					$modelTrans = new Transaksi;
					$modelTrans->id_kurir = 0;
					$modelTrans->nim = Yii::app()->user->nim;
					$modelTrans->tanggal = date('Y-m-d', strtotime(date('Y-m-d H:i:s')) + (7 * 60 * 60));
					$modelTrans->total = $_POST['hrg-total'];
					$modelTrans->nominal_pembayaran = $modelTrans->total;
					$modelTrans->jam_pesan = date('H:i:s',time() + (7 * 60 * 60));
					$modelTrans->status = 0;
					$modelTrans->save();
					
					$models = array();
					$pesanan = '<br/>';

					// Uncomment the following line if AJAX validation is needed
					// $this->performAjaxValidation($model);
					
					if(isset($_POST['TransaksiDetail'])){
						$models = $_POST['TransaksiDetail'];
						$date = date('Y-m-d');
						foreach($models as $item){
							if ($item['id_menu'] !== ''){
								$detail = new TransaksiDetail;
								$detail->id_transaksi = $modelTrans->id_transaksi;
								$detail->id_menu = $item['id_menu'];
								$detail->jumlah = $item['jumlah'];
								$detail->subtotal = $item['subtotal'];
								$detail->catatan = '';
								$detail->testimonial = '';
								$detail->rating = 0;
								$pesanan = $pesanan . $detail->idMenu->idTenant->nama_warung.' / '.$detail->idMenu->nama_menu .' Jumlah : '. $detail->jumlah .' sub total : '.$detail->subtotal.'<br/>';
								// save item
								$detail->save();
								
								if($detail->idMenu->id_tenant !== '30'){
									//update kapasitas harian
									$kapHar = KapasitasHarian::model()->findByAttributes(array('id_menu'=>$item['id_menu'],'tanggal'=>$date));
									$kapHar->jml_terbeli = $kapHar->jml_terbeli + $item['jumlah'];
									$kapHar->jml_stock = $kapHar->jml_stock - $item['jumlah'];
									$kapHar->save();
								}
							}
							
						}
						unset(Yii::app()->request->cookies['order']);
						
						// send email to tanyafndel@gmail.com
						$to = 'tanyafndel@gmail.com';
						$from = 'admin@fndel.com';
						$subject = 'Pesanan Baru No '.$modelTrans->id_transaksi;
						$message = 'Dear Admin,
									<br/> Telah terjadi pesanan baru dengan rincian : 
									<br/> No Transaksi : '.$modelTrans->id_transaksi.'
									<br/> Nim : '.$user->nim.'
									<br/> Nama : '.$user->nama.'
									<br/> No. Tlp : '.$user->no_hp.'
									<br/> Total : '.$modelTrans->total.'
									<br/> Pesanan : '.$pesanan.'
									<br/> Alamat : '.$user->nama_kos.' / '.$user->no_kamar.'
									<br/><br/>Segera kirim pesanannya.
									<br/>Terimakasih. :)';
						$this->mailsend($to, $from, $subject, $message);
						
						// send email to user
						$to = $user->email;
						$from = 'admin@fndel.com';
						$subject = 'Pesanan No '.$modelTrans->id_transaksi.' segera diproses';
						$message = 'Dear '.$user->nama.',
									<br/> Pesanan kakak dengan rincian : 
									<br/> No Transaksi : '.$modelTrans->id_transaksi.'
									<br/> Total : '.$modelTrans->total.'
									<br/> Pesanan : '.$pesanan.'
									<br/> Alamat : '.$user->nama_kos.' / '.$user->no_kamar.'
									<br/> Telah kami terima.
									<br/><br/>Kami akan segera kirim pesanan kakak selambat-lambatnya 1 jam setelah kakak terima email ini.
									<br/>Terimakasih. :)
									<br/><br>
									Salam Hangat,
									<br/><br/>CS fndel.com
									<br/>=============================================
									<br/>www.fndel.com
									<br/>Office : Gang Masjid Sukabirus(Samping Masjid Istiqomah)
									<br/>Phone : 0857-9413-6415
									<br/>Line : fndel
									<br/>Email : tanyafndel@gmail.com';
						$this->mailsend($to, $from, $subject, $message);
						unset(Yii::app()->request->cookies['order']);
						$this->render('success');
					} else {
						$pesan = 'Silahkan pilih menu yang akan dipesan terlebih dahulu!';
						$this->render('error',array('pesan'=>$pesan));
					}
				} else {
					$pesan = 'Minimum pemesanan adalah Rp. 10.000!';
					$this->render('error',array('pesan'=>$pesan));
				}
			//} else {
			//	unset(Yii::app()->request->cookies['order']);
			//	$pesan = 'Maaf sisa saldo anda tidak mencukupi untuk melakukan transaksi ini, silahkan hubungi call center kami untuk melakukan pengisian ulang!';
			//	$this->render('error',array('pesan'=>$pesan));
			//}
		} else {
			$pesan = 'Silahkan pilih menu yang akan dipesan terlebih dahulu!';
			$this->render('error',array('pesan'=>$pesan));
		}
	}
	
	/**
	*
	*
	*/
	public function actionHistory(){
		
		$id = Yii::app()->user->nim;
		
		$criteria = new CDbCriteria;
		$criteria->addSearchCondition('nim',$id);
		$criteria->order = 'tanggal ASC';
		
		$models = Transaksi::model()->findAll($criteria);
		
		$this->render('history',array('models'=>$models));
	}
}
