<?php

class MenuController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column-user';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','loadImage','list', 'thumbnail','detail','voucher'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','rekomendasi','cart','cancel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/**
	*
	*
	*/
	public function actionCart(){
		
		$TD = new TransaksiDetail;
		$models=array();
		
		if(isset($_POST['id-menu'])){
			$TD->id_menu = $_POST['id-menu'];
			$TD->jumlah = $_POST['jml-menu'];
			$hrg = $_POST['hrg-menu'];
			$TD->subtotal = $hrg * $TD->jumlah;
		}
		
		if(isset(Yii::app()->request->cookies['order'])){
			$cookies = Yii::app()->request->cookies['order']->value;
			$temp = CJSON::decode($cookies);
			foreach($temp as $i=>$model){
				$obj = new TransaksiDetail;
				$obj->attributes = $temp[$i];
				$models[] = $obj;
			}
			$models[] = $TD;
			
			$encode = CJSON::encode($models);
			Yii::app()->request->cookies['order'] = new CHttpCookie('order', $encode);
		} else {
			$models[] = $TD;
			$encode = CJSON::encode($models);
			Yii::app()->request->cookies['order'] = new CHttpCookie('order', $encode);
		}
		
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	
	/**
	*
	*
	*/
	public function actionCancel(){
		
		$models=array();
		
		if(isset($_GET['id'])){
			$remove_id = $_GET['id'];
		
			if(isset(Yii::app()->request->cookies['order'])){
				$cookies = Yii::app()->request->cookies['order']->value;
				$temp = CJSON::decode($cookies);
				foreach($temp as $i=>$model){
					$obj = new TransaksiDetail;
					$obj->attributes = $temp[$i];
					if($obj->id_menu !== $remove_id)
						$models[] = $obj;
				}
				
				$encode = CJSON::encode($models);
				Yii::app()->request->cookies['order'] = new CHttpCookie('order', $encode);
			}
		}
		
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	
	/**
	* display model base on popularity / rate
	*/
	public function actionRekomendasi()
	{
		$model = new Menu;
		$model->unsetAttributes();
		
		$criteria = new CDbCriteria;
                $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
		$criteria->order = 'star DESC';
		
		//pagination
		$count = Menu::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Menu::model()->findAll($criteria);
		
		$this->render('list',array('model'=>$model, 'models'=>$models, 'pages'=>$pages));
	}
	
	/**
	* Display images with caption
	* 
	*/
	public function actionList(){
		$model = new Menu;
		$model->unsetAttributes();
		$models = array();
		$criteria = new CDbCriteria;
                $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
		$criteria->with = array('idTenant');
		
		if(isset($_POST['Menu'])){
			$model->attributes = $_POST['Menu'];
			$model->namaTenan = $_POST['Menu']['namaTenan'];

			if($model->nama_menu !== null)
				$criteria->addSearchCondition('nama_menu',$model->nama_menu);
			
			if($model->namaTenan !== null){
				$criteria->addSearchCondition('idTenant.nama_warung',$model->namaTenan);
			}
		}

		if (isset($_GET['id_tenant'])) {
			$criteria->addSearchCondition('id_tenant',$_GET['id_tenant']);
		}
		
		// add for pagination
		$count=Menu::model()->count($criteria);
		$pages=new CPagination($count);

		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$return = Menu::model()->findAll($criteria);
		$models = $return;
		if(isset($_POST['sort'])){
			$sort = $_POST['sort'];
			if($sort === 'tersedia'){
				$models = array();
				foreach($return as $item){
					if($item->available())
						$models[] = $item;
				}
			} 
		}
		
		$this->render('list', array('model'=>$model, 'models'=>$models, 'pages'=>$pages));
	}
	
	/**
	* Display menu with list mode
	* 
	*/
	public function actionThumbnail(){
		$model = new Menu;
		$model->unsetAttributes();
		
		if (isset($_GET['Menu'])) {
			$model->attributes=$_GET['Menu'];
		}
		
		$criteria = new CDbCriteria;
                $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
		$criteria->addSearchCondition('nama_menu',$model->nama_menu);
		//$criteria->addSearchCondition();
		//$criteria->addSearchCondition();
		
		$models = Menu::model()->findAll($criteria);
		
		$this->render('thumbnail', array('model'=>$model, 'models'=>$models));
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	 public function actionLoadImage($id){
		$model = $this->loadModel($id);
		$this->renderPartial('image',array('model'=>$model));
	 }
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Menu;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Menu'])) {
			$model->attributes=$_POST['Menu'];
			
			if(!empty($_FILES['Menu']['tmp_name']['foto'])){
				$file = CUploadedFile::getInstance($model,'foto');
				$fp = fopen($file->tempName, 'r');
				$content = fread($fp, filesize($file->tempName));
				fclose($fp);
				$model->fotoType = $file->type;
				$model->foto = $content;
			}
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id_menu));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Menu'])) {
			$model->attributes=$_POST['Menu'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id_menu));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Menu');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Menu('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Menu'])) {
			$model->attributes=$_GET['Menu'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Menu the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Menu::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Menu $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='menu-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionDetail($id)
	{
		$model = $this->loadModel($id);
		
		$now = date('Y-m-d');
		$kap = KapasitasHarian::model()->findByAttributes(array('id_menu'=>$model->id_menu, 'tanggal'=>$now));
		if($kap !== null && (($kap->jml_stock - $kap->jml_terbeli) > 0))
			$available = true;
		else
			$available = false;
		
		$this->render('detail',array('model'=>$model, 'available'=>$available));
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionVoucher()
	{	
		$criteria = new CDbCriteria;
                $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
		$criteria->addSearchCondition('id_tenant','30');
		
		$models = Menu::model()->findAll($criteria);
		
		$this->render('voucher',array('models'=>$models));
	}
}