<?php

/**
 * This is the model class for table "tbl_menu".
 *
 * The followings are the available columns in table 'tbl_menu':
 * @property integer $id_menu
 * @property integer $id_tenant
 * @property string $nama_menu
 * @property string $bahan_dasar_utama
 * @property integer $hrg_jual
 * @property integer $hrg_partner
 * @property string $bumbu
 * @property string $foto
 * @property integer $star
 *
 * The followings are the available model relations:
 * @property KapasitasHarian[] $kapasitasHarians
 * @property Tenant $idTenant
 * @property Customer[] $tblCustomers
 * @property PaketMenu[] $tblPaketMenus
 * @property SpesialMenu[] $spesialMenus
 * @property TemaMenu[] $TemaMenus
 * @property TransaksiDetail[] $transaksiDetails
 */
class Menu extends CActiveRecord
{
	//name for related table search
	public $namaTenan;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tenant, nama_menu, bahan_dasar_utama, hrg_jual, hrg_partner, bumbu, foto, star', 'required'),
			array('id_tenant, hrg_jual, hrg_partner, star', 'numerical', 'integerOnly'=>true),
			array('nama_menu, bahan_dasar_utama, bumbu, foto', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_menu, id_tenant, nama_menu, bahan_dasar_utama, hrg_jual, hrg_partner, bumbu, foto, star, namaTenan, TemaMenus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kapasitasHarians' => array(self::HAS_MANY, 'KapasitasHarian', 'id_menu'),
			'idTenant' => array(self::BELONGS_TO, 'Tenant', 'id_tenant'),
			'tblCustomers' => array(self::MANY_MANY, 'Customer', 'tbl_menu_preference(id_menu, nim)'),
			'tblPaketMenus' => array(self::MANY_MANY, 'PaketMenu', 'tbl_paket_menu_detail(id_menu, id_paket_menu)'),
			'spesialMenus' => array(self::HAS_MANY, 'SpesialMenu', 'id_menu'),
			'TemaMenus' => array(self::HAS_MANY, 'TemaMenu', 'id_menu'),
			'transaksiDetails' => array(self::HAS_MANY, 'TransaksiDetail', 'id_menu'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_menu' => 'Id Menu',
			'id_tenant' => 'Id Tenant',
			'nama_menu' => 'Nama Menu',
			'bahan_dasar_utama' => 'Bahan Dasar Utama',
			'hrg_jual' => 'Harga',
			'hrg_partner' => 'Hrg Partner',
			'bumbu' => 'Bumbu',
			'foto' => 'Foto',
			'star' => 'Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('idTenant', 'TemaMenus');

		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('id_tenant',$this->id_tenant);
		$criteria->compare('nama_menu',$this->nama_menu,true);
		//$criteria->compare('bahan_dasar_utama',$this->bahan_dasar_utama,true);
		$criteria->compare('hrg_jual',$this->hrg_jual);
		$criteria->compare('hrg_partner',$this->hrg_partner);
		//$criteria->compare('bumbu',$this->bumbu,true);
		//$criteria->compare('foto',$this->foto,true);
		$criteria->compare('star',$this->star);
		$criteria->compare('idTenant.nama_warung',$this->namaTenan,true);
		$criteria->compare('TemaMenus.nama_tema',$this->TemaMenus);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function available(){
		$now = date('Y-m-d');
		$time = date('H:i:s',time() + (7 * 60 * 60));
		
		$tenant = Tenant::model()->findByPK($this->id_tenant);
		$kap = KapasitasHarian::model()->findByAttributes(array('id_menu'=>$this->id_menu, 'tanggal'=>$now));
		
		$buka = date('H:i:s', strtotime($tenant->jam_buka));
		$tutup = date('H:i:s', strtotime($tenant->jam_tutup));
		
		$weekend = false;
		
		if(date('l') === 'Saturday' || date('l') === 'Sunday')
			$weekend = true;
		
		if($weekend === false){
			if($time >= $buka && $time <= $tutup){
				if($tenant->id_tenant === '30'){
					return true;
				}else {
					if($kap !== null && (($kap->jml_stock - $kap->jml_terbeli) > 0))
						return true;
					else
						return false;
				}
			} else {
				return false;
			}
		}else {
			return false;
		}
	}
}