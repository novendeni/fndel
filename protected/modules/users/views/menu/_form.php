<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'menu-form',
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data'
	),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib di isi!!</p><br />

    <?php echo $form->errorSummary($model); ?>

            <?php 
				$tenant = CHtml::listData(Tenant::model()->findAll(array('order' => 'id_tenant')), 'id_tenant', 'nama_warung');
				echo $form->dropDownListControlGroup($model,'id_tenant',$tenant, array('span'=>3, 'label'=>'Nama Tenant')); 
			?>

            <?php echo $form->textFieldControlGroup($model,'nama_menu',array('span'=>3,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'bahan_dasar_utama',array('span'=>3,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'hrg_jual',array('span'=>3)); ?>

            <?php echo $form->textFieldControlGroup($model,'hrg_partner',array('span'=>3)); ?>

            <?php echo $form->textFieldControlGroup($model,'bumbu',array('span'=>3,'maxlength'=>40)); ?>

            <?php //echo $form->textFieldControlGroup($model,'foto',array('span'=>3,'maxlength'=>40)); ?>
			<?php echo $form->fileFieldControlGroup($model,'foto');?>

            <?php echo $form->textFieldControlGroup($model,'star',array('span'=>3)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->