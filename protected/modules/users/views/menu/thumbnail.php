<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>

<h1>List Menu</h1>

<div class="form">

    <?php 
		$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
		)); 
	?>

        <?php //echo $form->textFieldControlGroup($model,'id_menu',array('span'=>3)); ?>

		<?php echo $form->textFieldControlGroup($model,'idTenant',array('span'=>3, 'label'=>'Nama Warung')); ?>

		<?php echo $form->textFieldControlGroup($model,'nama_menu',array('span'=>3,'maxlength'=>40)); ?>
		
		<?php //echo $form->textFieldControlGroup($model,'TemaMenus',array('span'=>3));?>

		<?php //echo $form->textFieldControlGroup($model,'bahan_dasar_utama',array('span'=>3,'maxlength'=>40)); ?>

		<?php //echo $form->textFieldControlGroup($model,'hrg_jual',array('span'=>3)); ?>

		<?php //echo $form->textFieldControlGroup($model,'hrg_partner',array('span'=>3)); ?>

		<?php //echo $form->textFieldControlGroup($model,'bumbu',array('span'=>3,'maxlength'=>40)); ?>

		<?php //echo $form->textFieldControlGroup($model,'foto',array('span'=>3,'maxlength'=>40)); ?>

		<?php //echo $form->textFieldControlGroup($model,'star',array('span'=>3)); ?>

    <div class="form-actions">
        <?php echo TbHtml::submitButton('Filter',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->

<?php 
	echo TbHtml::thumbnails(array(
		array('image' => 'holder.js/300x200', 'label' => $models[0]->nama_menu, 'caption' => $models[0]->bahan_dasar_utama . '<br/>' .$models[0]->hrg_jual),
		array('image' => 'holder.js/300x200', 'label' => $models[1]->nama_menu, 'caption' => $models[1]->bahan_dasar_utama .$models[0]->hrg_jual),
		//array('image' => 'holder.js/300x200', 'label' => $models[2]->nama_menu, 'caption' => $models[2]->bahan_dasar_utama .$models[0]->hrg_jual),
		//array('image' => 'holder.js/300x200', 'label' => $models[3]->nama_menu, 'caption' => $models[3]->bahan_dasar_utama .$models[0]->hrg_jual),
	), array('span' => 3)); 
?>
