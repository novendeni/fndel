<?php
/* @var $this MenuController */
/* @var $model Menu */
?>

<h1><?php //echo $model->idTenant->nama_warung.' : '.$model->nama_menu; ?></h1>

<div class='row'>
	<div class='span3'>
	<?php
		echo CHtml::image(Yii::app()->controller->createUrl('menu/loadImage', array('id'=>$model->id_menu)),'',array('width'=>250, 'height'=>100));
	?>
	</div>
	
	<div class='span5'>
		<h3><?php echo $model->idTenant->nama_warung.' : '.$model->nama_menu; ?></h3>
		
		<p><b>Review</b></p>
		<p><?php echo $model->bahan_dasar_utama?></p>
		<br />
		<p><b>Harga</b></p>
		<p>Rp. <?php echo $model->hrg_jual?>,-</p>
		<br />
		
		<?php
			echo TbHtml::button('Pesan',array(
				'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
				'submit'=>array('transaksidetail/order/id/'.$model->id_menu))
			);
			echo ' ';	
			echo TbHtml::button('komentar');
		?>
	</div>
</div>