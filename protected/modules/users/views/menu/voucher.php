<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>
<?php $this->layout='//layouts/column-user'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - Daftar Menu'; ?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'Voucher',
	);
?>


<div class="page-title">
	<div class="title"><h3>List Menu</h3></div>
</div>

<div class="menu-grid list">
	<div class="view-option">
		<button id="btn-list" class="btn btn-default active"><i class="fa fa-list-ul"></i></button>
		<button id="btn-grid" class="btn btn-default"><i class="fa fa-th"></i></button>
	</div>
	
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<?php $orderPage = Yii::app()->createUrl("users/transaksiDetail/order/id/".$model->id_menu); ?>
			<div class="box-menu">
				<div class="image">
				<!--	<a href="<?php //echo Yii::app()->createUrl("users/menu/detail/id/".$model->id_menu); ?>">
						<?php //echo CHtml::image(Yii::app()->controller->createUrl('menu/loadImage', array('id'=>$model->id_menu))); ?>
					</a> -->
                                    <?php 
							                                                        
                                                        $thumbFile = $_SERVER['DOCUMENT_ROOT'].'/images/thumb/product_'.$model->id_menu . '_thumb.jpg';
                                                         $thumbURL =  Yii::app()->getBaseUrl(true).'/images/thumb/product_'.$model->id_menu . '_thumb.jpg';
                                                       
                                                        if(!file_exists($thumbFile)){
                                                              // link to default image 
                                                               $thumbURL =    Yii::app()->getBaseUrl(true).'/images/menu_default.jpg';
							}
                                                        
                                                        
						?>
						
                                            <img src="<?php echo $thumbURL; ?>" width="65px" height="65px" alt=" "/>
				</div>
				<div class="title">
					<a href="#<?php //echo Yii::app()->createUrl("users/menu/detail/id/".$model->id_menu); ?>"><?php echo $model->nama_menu; ?></a>
					<a href="#<?php //echo Yii::app()->createUrl("users/menu/list/id_tenant/".$model->id_tenant); ?>" class="tenant"><i class="fa fa-home"></i> <?php echo $model->idTenant->nama_warung; ?></a>
				</div>
				<div class="price currency"><?php echo $model->hrg_jual; ?></div>
				<div class="status">
					<span class="likes">
						<a href="#"><i class="fa fa-thumbs-up"></i> 300 Likes</a>
					</span>
					<span class="separator">|</span>
					<span class="rating">
						<a href="#"><span class="rating-avg" value="<?php echo $model->star;?>"></span></a>
					</span>
				</div>
				<div class="button">
					<?php if($model->available()): ?>
						<a href="#form-modal" class="btn-1 pesan" id="btn-order" data-toggle="modal" data-id-menu="<?php echo $model->id_menu;?>" data-hrg-menu="<?php echo $model->hrg_jual;?>" data-nama-menu="<?php echo $model->nama_menu;?>">Pesan</a>
					<?php else : ?>
						<p class="btn btn-1 pesan" style="font-size:12px">Out Of Stock</p>
					<?php endif;?>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>

<div id="form-modal" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4>Pilih Jumlah Pesanan</h4>
			</div>
			<div class="modal-body">
				<form id="form-pesan" name="form-pesan"  action="<?php echo Yii::app()->createUrl('users/menu/cart');?>" method="POST">
					<fieldset>
						<input type="hidden" name="id-menu" id="id-menu" value=""/>
						<input type="hidden" name="hrg-menu" id="hrg-menu" value=""/>
						<input type="text" name="nama-menu" id="nama-menu" readOnly="true" value=""/>
						&nbsp;&nbsp;Jumlah 
						<select id="jml-menu" name="jml-menu">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
				<input type="button" class="btn btn-primary modal-order" name="submit_id" id="btn_id" value="Pesan" data-dismiss="modal"/>
			</div>
		</div>
	</div>
</div>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
			'id'=>'order_msg',
			//'cssFile'=>'fndel.css',
			'options'=>array(
				'autoOpen'=>false,
				'width'=>500,
				'height'=>150
			)
		));
			echo 'Pesanan anda telah masuk keranjang belanja. Silahkan klik <b>order di sebelah kiri</b> untuk memproses transaksi';
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>