<?php $this->layout='//layouts/column-user'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - History Top Up'; ?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'History Top Up',
	);
?>


<div class="page-title">
	<div class="title"><h3>History Top Up</h3></div>
</div>

<div class=''>
	<table class="table">
		<tr>
			<th width="20px">No</th>
			<th>Tanggal</th>
			<th>Jumlah</th>
		</tr>
		<?php $total = 0;?>
		<?php foreach($models as $i=>$model):?>
		<tr>
			<td><?php echo $i+1; ?></td>
			<td><?php echo date('d-m-Y', strtotime($model->tanggal)); ?></td>
			<td><?php echo "$model->jumlah"; ?></td>
			<?php $total = $total + $model->jumlah;?>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colSpan="2">Total</td>
			<td><?php echo $total;?></td>
		</tr>
	</table>
</div>