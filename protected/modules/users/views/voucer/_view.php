<?php
/* @var $this VoucerController */
/* @var $data Voucer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_voucer')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_voucer), array('view', 'id'=>$data->id_voucer)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kuota')); ?>:</b>
	<?php echo CHtml::encode($data->kuota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />


</div>