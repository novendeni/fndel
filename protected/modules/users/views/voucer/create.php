<?php
/* @var $this VoucerController */
/* @var $model Voucer */

$this->breadcrumbs=array(
	'Voucers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Voucer', 'url'=>array('index')),
	array('label'=>'Manage Voucer', 'url'=>array('admin')),
);
?>

<h1>Create Voucer</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>