<?php
/* @var $this VoucerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Voucers',
);

$this->menu=array(
	array('label'=>'Create Voucer', 'url'=>array('create')),
	array('label'=>'Manage Voucer', 'url'=>array('admin')),
);
?>

<h1>Voucers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
