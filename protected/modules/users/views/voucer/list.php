<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>
<?php $this->layout='//layouts/column-user'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - Voucer'; ?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'Voucer',
	);
?>


<div class="page-title">
	<div class="title"><h3>Voucer</h3></div>
</div>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>

<div class="menu-grid list">
	<div class="view-option">
		<button id="btn-list" class="btn btn-default active"><i class="fa fa-list-ul"></i></button>
		<button id="btn-grid" class="btn btn-default"><i class="fa fa-th"></i></button>
	</div>
	
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<div class="box-menu">
				<div class="image">
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/Voucher.png"></img>
				</div>
				<div class="title">
					<a href="#"><?php echo $model->kuota; ?></a>
					<a href="#" class="tenant"><i class="fa fa-home"></i> MyNet</a>
				</div>
				<div class="price currency"><?php echo $model->harga; ?></div>
				<div class="status">
					<span class="likes">
						<a href="#"><i class="fa fa-thumbs-up"></i> 300 Likes</a>
					</span>
					<span class="separator">|</span>
					<span class="rating">
						<a href="#"><span class="rating-avg" value="5"></span></a>
					</span>
				</div>
				<div class="button">
					<a href="<?php echo Yii::app()->createUrl('/users/voucer/pesan',array('id'=>$model->id_voucer));?>" class="btn-1 pesan" id="btn-order" >Pesan</a>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>