<?php
/* @var $this VoucerController */
/* @var $model Voucer */

$this->breadcrumbs=array(
	'Voucers'=>array('index'),
	$model->id_voucer,
);

$this->menu=array(
	array('label'=>'List Voucer', 'url'=>array('index')),
	array('label'=>'Create Voucer', 'url'=>array('create')),
	array('label'=>'Update Voucer', 'url'=>array('update', 'id'=>$model->id_voucer)),
	array('label'=>'Delete Voucer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_voucer),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Voucer', 'url'=>array('admin')),
);
?>

<h1>View Voucer #<?php echo $model->id_voucer; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_voucer',
		'kuota',
		'harga',
	),
)); ?>
