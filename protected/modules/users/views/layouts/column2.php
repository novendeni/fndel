<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="row">
    <div class="span3">
		<div class='row' style='padding-top:10px' align='center'>
			<?php 
				echo '<strong>'.Yii::app()->user->nama.'</strong>';
				echo '<br/>';
				echo '<strong>'.Yii::app()->user->nim.'</strong>';
			?>
		</div>
        <?php
			$this->widget('bootstrap.widgets.TbNav', array(
				'type' => TbHtml::NAV_TYPE_LIST,
				'items' => array(
					TbHtml::menuDivider(),
					array('label'=>'Profil'),
					array('label'=>'Edit Profil', 'url'=>array('/users/customer/view')),
					//array('label'=>'Edit Profil', 'url'=>array('/users/customer/update')),
					array('label'=>'History Transaksi', 'url'=>array('/users/transaksi/history')),
					array('label'=>'Menu'),
					array('label'=>'Daftar Menu / Ordre', 'url'=>array('/users/menu/list')),
					array('label'=>'Menu Favorit', 'url'=>array('')),
					array('label'=>'Tambah Menu Favorit', 'url'=>array('')),
					TbHtml::menuDivider(),
					array('label'=>'Logout ('.Yii::app()->user->nama.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
				)
			));
        ?>
    </div>
	
	<div class="span9">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
</div>
<?php $this->endContent(); ?>