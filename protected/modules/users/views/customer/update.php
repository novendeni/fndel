<?php
/* @var $this CustomerController */
/* @var $model Customer */
?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'Edit Profil'=>Yii::app()->createUrl('users/customer/view'),
		'Update Profil',
	);
?>
<div class="page-title">
	<div class="title"><h3>Update Profil</h3></div>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>