<?php
/* @var $this CustomerController */

?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama),
	);
?>

<div class="alert alert-success" role="alert">
	<p align='center'>
		Sisa saldo anda saat ini adalah </br>
		<b>Rp. <?php echo $saldo?></b>
	</p>
</div>