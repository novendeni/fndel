<?php
/* @var $this CustomerController */
/* @var $model Customer */
?>
<?php $this->layout='//layouts/column-user'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - Edit Profil'; ?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'Edit Profil',
	);
?>

<div class="page-title">
	<div class="title"><h3>Profil</h3></div>
</div>

<table class="table">
	<tr>
		<td>NIM</td>
		<td><?php echo $model->nim;?></td>
	</tr>
	<tr>
		<td>Nama</td>
		<td><?php echo $model->nama;?></td>
	</tr>
	<tr>
		<td>Tempat / Tgl Lahir</td>
		<td><?php echo $model->tempat_lhr.'/'.$model->tgl_lhr;?></td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td>
			<?php 
				if($model->jns_kelamin === 'L')
					echo 'Laki - laki';
				else
					echo 'Perempuan';
			?>
		</td>
	</tr>
	<tr>
		<td>No Hp</td>
		<td><?php echo $model->no_hp;?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td><?php echo $model->email;?></td>
	</tr>
	<tr>
		<td>Kos</td>
		<td><?php echo $model->nama_kos . ' / ' . $model->no_kamar . ' / ' . $model->area;?></td>
	</tr>
</table>