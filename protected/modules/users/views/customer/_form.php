<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<!--<div class="form-group">							
		<?php //echo $form->labelEx($model,'nim', array('label'=>'NIM')); ?>
		<?php //echo $form->textField($model,'nim', array('class'=>'form-control','placeholder'=>'NIM')); ?>
		<?php //echo $form->error($model,'nim', array('class'=>'alert-text')); ?>
	</div> -->
	<div class="form-group">							
		<?php echo $form->labelEx($model,'nama', array('label'=>'Nama')); ?>
		<?php echo $form->textField($model,'nama', array('class'=>'form-control','placeholder'=>'Nama')); ?>
		<?php echo $form->error($model,'nama', array('class'=>'alert-text')); ?>
	</div>
	
	<div class="form-group">							
		<?php echo $form->labelEx($model,'email', array('label'=>'Email')); ?>
		<?php echo $form->emailField($model,'email', array('class'=>'form-control','placeholder'=>'your@mail.com')); ?>
		<?php echo $form->error($model,'email', array('class'=>'alert-text')); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'password', array('label'=>'Password')); ?>
		<?php echo $form->passwordField($model,'password', array('class'=>'form-control','placeholder'=>'Password')); ?>
		<?php echo $form->error($model,'password', array('class'=>'alert-text')); ?>
	</div>
	<div class="form-group">
		<?php //echo $form->labelEx($model,'confirm_pass', array('label'=>'Ulangi Password')); ?>
		<?php echo $form->passwordField($model, 'confirm_pass', array('class'=>'form-control','placeholder'=>'Ulangi Password')); ?>
		<?php echo $form->error($model,'password', array('class'=>'alert-text')); ?>
	</div>
	<div class="form-group form-inline" >
		<div class="form-group form-inline" >
			<?php echo $form->labelEx($model,'nama_kos', array('label'=>'Kost / Tempat Tinggal')); ?>
			<br/>	
			<?php $areas = array_map('ucfirst',CHtml::listData(Kost::model()->findAll(array('order'=>'id_kost', 'group'=>'area')),'area','area'));?>
			<?php echo $form->dropDownList($model,'area',$areas, array('prompt'=>'-- Pilih Area --', 'class'=>'form-control', 'style'=>'width: 200px;', 'ajax'=>array('type'=>'POST','url'=>Yii::app()->createUrl('/customer/loadKost'),'update'=>'#Customer_nama_kos','data'=>array('area'=>'js:this.value')))); ?>
			<?php echo $form->dropDownList($model,'nama_kos',array(),array('prompt'=>'-- Pilih Kost --', 'class'=>'form-control', 'style'=>'width: 200px;')); ?>
			<?php echo $form->textField($model,'no_kamar', array('class'=>'form-control','placeholder'=>"Nomor Kamar", 'style'=>'width: 100px;')); ?>
			<?php echo $form->error($model,'no_kamar', array('class'=>'alert-text')); ?>
			<span class="help-block visible-xs-block visible-sm-block"></span>
		</div>
	</div>
	<div class="form-group form-inline" >
		<?php echo $form->labelEx($model,'tgl_lhr', array('label'=>'Tempat / Tanggal lahir')); ?>
		<br/>
		<?php echo $form->textField($model,'tempat_lhr', array('class'=>'form-control','placeholder'=>'Tempat Lahir')); ?>
		<?php echo $form->error($model,'tempat_lhr', array('class'=>'alert-text')); ?>
		<span class="help-block visible-xs-block"></span>
		<div class="input-append date date-picker inline">
			<?php echo $form->textField($model,'tgl_lhr', array('class'=>'form-control','placeholder'=>'17-08-1999', 'data-date'=>'12-02-2012', 'data-date-format'=>'dd-mm-yyyy', 'data-date-viewmode'=>'years')); ?>
			<?php echo $form->error($model,'tgl_lhr', array('class'=>'alert-text')); ?>
		</div>
	</div>
	<div class="form-group">							
		<?php echo $form->labelEx($model,'jns_kelamin', array('label'=>'Jenis Kelamin')); ?>
		<?php echo $form->dropDownList($model, 'jns_kelamin', array(
			'L'=>'Laki-laki', 
			'P'=>'Perempuan',
		), array('empty'=>'--Pilih Jenis Kelamin--', 'class'=>'form-control')); ?>
		
	</div>
	<div class="form-group">							
		<?php echo $form->labelEx($model,'no_hp', array('label'=>'Nomor HP')); ?>
		<?php echo $form->textField($model,'no_hp', array('class'=>'form-control','placeholder'=>'Nomor HP')); ?>
		<?php echo $form->error($model,'no_hp', array('class'=>'alert-text')); ?>
	</div>
	<button type="submit" class="btn btn-primary">Save</button>
	
	<?php $this->endWidget(); ?>
	<br/><br/>
	
</div><!-- form -->