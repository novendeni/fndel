<?php
	/**
	*
	*
	**/
	
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'transaksi-detail-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<table border='1'>
		<tr>
			<th>Nama Menu</th>
			<th>Harga</th>
			<th>Jumlah</th>
			<th>Sub Total</th>
		</tr>
		<?php foreach($models as $i=>$model): ?>
		<tr>
			<td width='200'>
				<?php 
					$warung = Tenant::model()->findByAttributes(array('id_tenant'=>$model->idMenu->id_tenant));
					echo TbHtml::activeHiddenField($model,"[$i]id_menu",array('disabled'=>true, 'value'=>$model->idMenu->nama_menu)); 
					echo $model->idMenu->nama_menu;
					echo '<br/>('.$warung->nama_warung.')';
				?>
			</td>
			<td><?php echo TbHtml::activeTextField($model,'', array('class'=>'harga','disabled'=>true, 'value'=>$model->idMenu->hrg_jual,'span'=>2)); ?></td>
			<td><?php echo TbHtml::activeDropDownList($model,"[$i]jumlah", array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5'),array('class'=>'jumlah','span'=>1)); ?></td>
			<td><?php echo TbHtml::activeTextField($model,"[$i]subtotal", array('class'=>'subtotal','disabled'=>true, 'span'=>2)); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
    <div class='row'>
		<div class="form-actions">
			<?php echo TbHtml::submitButton('Tambah Pesanan',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY));?>
		</div>
		<?php
			echo '<br/>';
			echo TbHtml::button('Selesai',array(
				'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
				'submit'=>array('transaksi/simpan'))
			);
		?>
	</div>
    <?php $this->endWidget(); ?>
</div>

<script>
	$.each($(".jumlah"), function(index, obj){
		$(obj).change(function(){
			var jml = ($(this).val());
			var hrg = ($(".harga:eq("+index+")").val());
			var subtot = jml * hrg;
			
			$(".subtotal:eq("+index+")").val(subtot);
		});
	});
</script>