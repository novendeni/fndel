<?php
/* @var $this TransaksiDetailController */
/* @var $model TransaksiDetail */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'transaksi-detail-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php //echo $form->textFieldControlGroup($model,'id_transaksi',array('span'=>3)); ?>

			<?php
				$tenant = CHtml::listData(Tenant::model()->findAll(array('order' => 'id_tenant')), 'id_tenant', 'nama_warung');
				echo TbHtml::dropDownListControlGroup('id_tenant','',$tenant, array('label'=>'Tenant', 'empty'=>'-- Pilih Tenant --', 'ajax'=> array('type'=>'POST','url'=>CController::createUrl('transaksiDetail/updateMenu'),'update'=>'#id_menu'))); 
			?>
			
            <?php //echo $form->textFieldControlGroup($model,'id_menu',array('span'=>3)); ?>
			<?php 
				echo $form->dropdownListControlGroup($model, 'id_menu', array(), array('empty'=>'-- Pilih menu --', 'label'=>'Menu'));
			?>

            <?php echo $form->textFieldControlGroup($model,'jumlah',array('span'=>3, 'onchange'=>'')); ?>

            <?php echo $form->textFieldControlGroup($model,'subtotal',array('span'=>3)); ?>

            <?php echo $form->textFieldControlGroup($model,'catatan',array('span'=>3,'maxlength'=>128)); ?>

            <?php echo $form->textFieldControlGroup($model,'testimonial',array('span'=>3,'maxlength'=>128)); ?>

            <?php echo $form->textFieldControlGroup($model,'rating',array('span'=>3)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->