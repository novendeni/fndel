<?php
/* @var $this TransaksiDetailController */
/* @var $model TransaksiDetail */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'transaksi-detail-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<table>
		<tr>
			<th>Menu</th>
			<th>Jumlah</th>
			<th>Harga</th>
			<th>Total</th>
		</tr>
		<?php foreach($models as $i=>$model): ?>
		<tr>
			<td><?php echo TbHtml::activeTextField($model,"[$i]id_menu",array('disabled'=>true, 'value'=>$model->idMenu->nama_menu)); ?></td>
			<td><?php echo TbHtml::activeTextField($model,"[$i]jumlah", array('span'=>1)); ?></td>
			<td><?php echo TbHtml::activeTextField($model,"[$i]subtotal", array('value'=>$model->idMenu->hrg_jual,'span'=>2)); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>

    <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Pesan Lainnya' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->