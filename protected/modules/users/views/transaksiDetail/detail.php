<?php

?>

<?php $this->pageTitle=Yii::app()->name . ' - Detail Transaksi'; ?>

<div class="page-title">
	<div class="title"><h3>Detail Transaksi</h3></div>
</div>

<table class="table">
	<tr>
		<th style="width:30%">Warung</th>
		<th style="width:30%">Menu</th>
		<th>Jumlah</th>
		<th>Sub Total</th>
	</tr>
	<?php $total = 0;?>
	<?php foreach($models as $i=>$detail): ?>
	<tr>
		<td>
			<?php $warung = Tenant::model()->findByAttributes(array('id_tenant'=>$detail->idMenu->id_tenant));?>
			<?php echo $warung->nama_warung;?>
		</td>
		<td><?php echo $detail->idMenu->nama_menu; ?></td>
		<td><?php echo $detail->jumlah; ?></td>
		<td><?php echo $detail->subtotal; ?></td>
		<?php $total = $total + $detail->subtotal;?>
	</tr>
	<?php endforeach;?>
	<tr>
			<td colSpan="3">Total</td>
			<td><?php echo $total;?></td>
		</tr>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?> 
</div>

<a href="<?php echo Yii::app()->createUrl('/users/transaksi/history');?>" class="btn btn-1 pesan">Back</a>