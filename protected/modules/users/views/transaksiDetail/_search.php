<?php
/* @var $this TransaksiDetailController */
/* @var $model TransaksiDetail */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_transaksi_detail',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_transaksi',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_menu',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'jumlah',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'subtotal',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'catatan',array('span'=>5,'maxlength'=>128)); ?>

                    <?php echo $form->textFieldControlGroup($model,'testimonial',array('span'=>5,'maxlength'=>128)); ?>

                    <?php echo $form->textFieldControlGroup($model,'rating',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->