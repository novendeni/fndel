<?php
/* @var $this TransaksiDetailController */
/* @var $model TransaksiDetail */
?>

<?php
$this->breadcrumbs=array(
	'Transaksi Details'=>array('index'),
	$model->id_transaksi_detail,
);

$this->menu=array(
	array('label'=>'List TransaksiDetail', 'url'=>array('index')),
	array('label'=>'Create TransaksiDetail', 'url'=>array('create')),
	array('label'=>'Update TransaksiDetail', 'url'=>array('update', 'id'=>$model->id_transaksi_detail)),
	array('label'=>'Delete TransaksiDetail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_transaksi_detail),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TransaksiDetail', 'url'=>array('admin')),
);
?>

<h1>View TransaksiDetail #<?php echo $model->id_transaksi_detail; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_transaksi_detail',
		'id_transaksi',
		'id_menu',
		'jumlah',
		'subtotal',
		'catatan',
		'testimonial',
		'rating',
	),
)); ?>