<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'Detail Transaksi',
	);
?>
<div class="page-title">
	<div class="title"><h3>Detail Pemesanan</h3></div>
</div>

<div class=''>
	<div class="form">

    <?php 
		$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
			'action'=>Yii::app()->createUrl('users/transaksi/simpan'),
			'method'=>'POST',
		)); 
	?>
	<table class="table">
		<tr>
			<th width=60%>Nama Menu</th>
			<th>Harga</th> <!--
			<th>Jumlah</th>
			<th>Sub Total</th> -->
		</tr>
		<?php foreach($models as $i=>$model): ?>
		<tr>
			<td width='200'>
				<?php 
					$warung = Tenant::model()->findByAttributes(array('id_tenant'=>$model->idMenu->id_tenant));
					echo TbHtml::activeHiddenField($model,"[$i]id_menu",array('disabled'=>true, 'value'=>$model->idMenu->nama_menu)); 
					echo $model->idMenu->nama_menu;
					echo '<br/>('.$warung->nama_warung.')';
				?>
			</td>
			<td><?php echo TbHtml::activeTextField($model,"", array('disabled'=>true, 'value'=>$model->idMenu->hrg_jual,'span'=>2)); ?></td>
		<!--	<td><?php //echo TbHtml::activeDropDownList($model,"[$i]jumlah", array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5'),array('span'=>1)); ?></td>
			<td><?php //echo TbHtml::activeTextField($model,"[$i]subtotal", array('disabled'=>true, 'value'=>$model->idMenu->hrg_jual,'span'=>2)); ?></td>
	-->	</tr>
		<?php endforeach; ?>
	</table>
	<?php echo '<br/>'; ?>
    <div>
		<div class="form-actions">
			<?php echo TbHtml::submitButton('Selesai',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY));?>
			<a href="<?php echo Yii::app()->createUrl("users/menu/list");?>" class="btn btn-primary">Tambah Pesanan</a>
		</div>
	</div>
    <?php $this->endWidget(); ?>
	</div><!-- search-form -->
	<br/><br/>
</div>