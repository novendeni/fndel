<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle='Login';
?>

<!--
<h2 align='center'>Halama Login</h2>
-->

<div class="form">
	<?php 
		$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
			'id'=>'login-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
		)); 
	?>

	<p class="help-block">Kolom dengan tanda<span class="required">*</span> WAJIB diisi !</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'nim',array('span'=>3)); ?>

    <?php echo $form->passwordFieldControlGroup($model,'password',array('span'=>3,'maxlength'=>30)); ?>
	
	<?php echo $form->checkBoxControlGroup($model,'rememberMe',array()); ?>
	
	 <div class="form-actions">
        <?php echo TbHtml::submitButton('Login',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    //'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
