<?php
/* @var $this TransaksiController */
/* @var $model Transaksi */
/* @var $form TbActiveForm */

Yii::app()->clientScript->registerScript('calculate',
	"$.each($(\".jumlah\"), function(index, obj){
		$(obj).change(function(){
			var jml = ($(this).val());
			var hrg = $(\".harga:eq(\"+index+\")\").val();
			var subtot = jml * hrg;
			var total = parseInt($(\".total\").val()) + parseInt(subtot);
			
			$(\".subtotal:eq(\"+index+\")\").val(subtot);
			$(\".total\").val(total);
		});
	});",
CClientScript::POS_READY);
	
?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'Transaksi',
	);
?>

<div class="page-title">
	<div class="title"><h3>Transaksi</h3></div>
</div>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'transaksi-form',
	'layout'=> TbHtml::FORM_LAYOUT_HORIZONTAL,
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
		<div class="row">
			<div class="col-md-5 col-sm-8">
				<?php //echo $form->textFieldControlGroup($modelTrans,'id_kurir',array('span'=>3)); ?>
			
				<?php echo $form->textFieldControlGroup($modelTrans,'nim',array('class'=>'form-control','readOnly'=>true)); ?>
			
				<?php echo $form->textFieldControlGroup($modelTrans,'tanggal',array('class'=>'form-control','readOnly'=>true)); ?>

				<?php echo $form->textFieldControlGroup($modelTrans,'total',array('class'=>'total form-control','readOnly'=>true)); ?>
			</div>
		</div>
		<span class="help-block"></span>
		<table class="table" >
			<tr>
				<th width=30%>Menu</th>
				<th width=10%>Jumlah</th>
				<th width=20%>Harga</th>
				<th width=20%>Sub Total</th>
			</tr>
			<?php foreach($models as $i=>$model): ?>
				<tr>
					<td>
						<?php
							$warung = Tenant::model()->findByAttributes(array('id_tenant'=>$model->idMenu->id_tenant));
							echo TbHtml::activeHiddenField($model,"[$i]id_menu",array('span'=>2)); 
							echo $model->idMenu->nama_menu;
							echo '<br/>('.$warung->nama_warung.')';
						?>
					</td>
					<td>
						<?php echo TbHtml::activeDropDownList($model,"[$i]jumlah", array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5'),array('class'=>'jumlah','span'=>1,'empty'=>'Pilih','readOnly'=>true)); ?>
						<?php ?>
					</td>
					<td>
						<?php echo TbHtml::activeTextField($model,'', array('class'=>'harga','value'=>$model->idMenu->hrg_jual,'span'=>2, 'disabled'=>true)); ?>
					</td>
					<td>
						<?php echo TbHtml::activeTextField($model,"[$i]subtotal", array('class'=>'subtotal','span'=>2,'readOnly'=>true)); ?>
						<?php ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($modelTrans->isNewRecord ? 'Pesan' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>
	<br/><br/>
</div><!-- form -->