<?php
/* @var $this TransaksiController */
/* @var $model Transaksi */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'transaksi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib di isi!!!</p><br />

    <?php //echo $form->errorSummary($model); ?>

            <?php //echo $form->textFieldControlGroup($model,'id_kurir',array('span'=>3)); ?>

            <?php echo $form->textFieldControlGroup($model,'nim',array('span'=>3,'maxlength'=>40, 'disabled'=>true)); ?>

            <?php echo $form->textFieldControlGroup($model,'tanggal',array('span'=>3, 'disabled'=>true)); ?>

            <?php echo $form->textFieldControlGroup($model,'total',array('span'=>3, 'disabled'=>true)); ?>

            <?php //echo $form->textFieldControlGroup($model,'nominal_pembayaran',array('span'=>3)); ?>
			
			<table border='1'>
				<tr>
					<th>Menu</th>
					<th>Jumlah</th>
					<th>Harga</th>
					<th>Sub Total</th>
				</tr>
				<?php foreach($models as $i=>$model): ?>
				<tr>
					<td><?php echo TbHtml::activeTextField($model,"[$i]id_menu",array('disabled'=>true, 'value'=>$model->idMenu->nama_menu)); ?></td>
					<td><?php echo TbHtml::activeDropDownList($model,"[$i]jumlah", array(1=>'1',2=>'2',3=>'3',4=>'4',5=>'5'),array('span'=>1)); ?></td>
					<td><?php echo TbHtml::activeTextField($model,'', array('value'=>$model->idMenu->hrg_jual,'span'=>2, 'disabled'=>true)); ?></td>
					<td><?php echo TbHtml::activeTextField($model,"[$i]subtotal", array('disabled'=>true,'span'=>2)); ?></td>
				</tr>
				<?php endforeach; ?>
			</table>

            <?php //echo $form->textFieldControlGroup($model,'jam_pesan',array('span'=>3)); ?>

            <?php //echo $form->textFieldControlGroup($model,'jam_terkirim',array('span'=>3)); ?>

            <?php //echo $form->textFieldControlGroup($model,'rating',array('span'=>3)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Pesan' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->