<?php 
	$this->breadcrumbs=array(
		'Tenant Menu',
	);
?>
		
<?php
	if(!isset($user)){
		$user = new LoginForm;
	}
?>
		
<div class="page-title">
	<div class="title"><?php echo $tenant->nama_warung;?></div>
</div>
<div class="menu-grid list">
	<div class="view-option">
		<button id="btn-list" class="btn btn-default active"><i class="fa fa-list-ul"></i></button>
		<button id="btn-grid" class="btn btn-default"><i class="fa fa-th"></i></button>
	</div>
	
	<?php if($models != null): ?>
		
		
		<?php foreach($models as $model): ?>
			<?php $orderPage = Yii::app()->createUrl("site/order/id/".$model->id_menu); ?>
			
			<div class="box-menu">
				<div class="image">
					<a href="<?php echo $orderPage ?>">
						<!--<img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/menu2.jpg"/>-->
						<?php echo CHtml::image(Yii::app()->controller->createUrl('menu/loadImage', array('id'=>$model->id_menu))); ?>
					</a>
				</div>
				<div class="title">
					<!--<a href="<?php //echo $orderPage ?>"><?php echo $model->nama_menu; ?></a>-->
					<?php echo $model->nama_menu;?>
					<a href="<?php echo Yii::app()->createUrl('/users/tenant/menu',array('id'=>$model->id_tenant)); ?>" class="tenant"><i class="fa fa-home"></i> <?php echo $model->idTenant->nama_warung; ?></a>
				</div>
				<div class="price currency"><?php echo $model->hrg_jual; ?></div>
				<div class="status">
					<span class="likes">
						<a href="#"><i class="fa fa-thumbs-up"></i> 300 Likes</a>
					</span>
					<span class="separator">|</span>
					<span class="rating">
						<span class="rating-avg" value="<?php echo $model->star;?>"></span>
					</span>
				</div>
				<div class="button">
					<?php 
						if(Yii::app()->user->isGuest){
							if($model->available())
								echo '<a href="#login-modal" class="btn-1 pesan" id="btn-login" data-toggle="modal">Pesan</a>';
							else
								echo '<p class="btn btn-1 pesan" style="font-size:12px">Out Of Stock</p>';
						} else {
							if($model->available()): ?>
								<a href="#form-modal" class="btn-1 pesan" id="btn-order" data-toggle="modal" data-id-menu="<?php echo $model->id_menu;?>" data-hrg-menu="<?php echo $model->hrg_jual;?>" data-nama-menu="<?php echo $model->nama_menu;?>">Pesan</a>
							<?php else : ?>
								<p class="btn btn-1 pesan" style="font-size:12px">Out Of Stock</p>
							<?php endif;
						}
					?>
				</div>
			</div>
		<?php endforeach; ?>
	<?php else :?>
		Maaf data Tidak ditemukan
	<?php endif; ?>
</div>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'maxButtonCount'=>5,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>

<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog',array('id'=>'order_msg','options'=>array('autoOpen'=>false)));
		echo 'Pesanan anda telah masuk keranjang belanja. Silahkan klik order di sebelah kiri untuk memproses transaksi';
	$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<div id="login-modal" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4>Login</h4>
			</div>
			<div class="modal-body">
				<p class="text-warning">Untuk melakukan pemesanan anda harus login terlebih dahulu.</p>
				<div class="form">
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'login-form',
							'enableClientValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
							),
							'action'=> Yii::app()->createUrl('/site/login'),
						)); ?>
						
							<div class="form-group">							
								<?php echo $form->labelEx($user,'nim', array('label'=>'Username / NIM')); ?>
								<?php echo $form->textField($user,'nim', array('class'=>'form-control','placeholder'=>'Username / NIM')); ?>
								<?php echo $form->error($user,'nim', array('class'=>'alert-text')); ?>
							</div>
							<div class="form-group">
								<?php echo $form->labelEx($user,'password', array('label'=>'Password')); ?>
								<?php echo $form->passwordField($user,'password', array('class'=>'form-control','placeholder'=>'Password')); ?>
								<?php echo $form->error($user,'password', array('class'=>'alert-text')); ?>
							</div>
							<div class="checkbox">
								<label>
									<?php echo $form->checkBox($user,'rememberMe'); ?>
									<?php echo $form->label($user,'rememberMe'); ?>
									<?php echo $form->error($user,'rememberMe', array('class'=>'alert-text')); ?>
								</label>
							</div>
							<button type="submit" class="btn btn-primary" data-dismiss="modal">Submit</button>
							
							<?php $this->endWidget(); ?>
							<br/><br/> 
							<a href="<?php Yii::app()->createUrl('site/forgetPass');?>">Lupa Password?</a> | Belum Punya Akun? <a href="<?php echo Yii::app()->createUrl('/customer/register'); ?>">Register</a>
					</div>
			</div>
			<div class="modal-footer">
				<!--<input type="button" class="btn btn-primary modal-order" name="submit_id" id="btn_id" value="Pesan" data-dismiss="modal"/>
				<input type="submit" value="pesan" data-dismiss="modal">-->
			</div>
		</div>
	</div>
</div>

<div id="form-modal" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4>Pilih Jumlah Pesanan</h4>
			</div>
			<div class="modal-body">
				<form id="form-pesan" name="form-pesan"  action="<?php echo Yii::app()->createUrl('users/menu/cart');?>" method="POST">
					<fieldset>
						<input type="hidden" name="id-menu" id="id-menu" value=""/>
						<input type="hidden" name="hrg-menu" id="hrg-menu" value=""/>
						<input type="text" name="nama-menu" id="nama-menu" readOnly="true" value=""/>
						&nbsp;&nbsp;Jumlah 
						<select id="jml-menu" name="jml-menu">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
				<input type="button" class="btn btn-primary modal-order" name="submit_id" id="btn_id" value="Pesan" data-dismiss="modal"/>
				<!--<input type="submit" value="pesan" data-dismiss="modal">-->
			</div>
		</div>
	</div>
</div>