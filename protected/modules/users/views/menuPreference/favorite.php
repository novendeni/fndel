<?php
/* @var $this MenuPreferenceController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>
<?php $this->layout='//layouts/column-user'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - Menu Favorite'; ?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'Menu Favorite',
	);
?>


<div class="page-title">
	<div class="title"><h3>Menu Favorite</h3></div>
</div>
<?php
	//echo TbHtml::dropDownListControlGroup('view','',array('list'=>'List', 'grid'=>'Grid'), array('span'=>1, 'label'=>'Mode'));
?>

<div class="menu-grid list">
	<div class="view-option">
		<button id="btn-list" class="btn btn-default active"><i class="fa fa-list-ul"></i></button>
		<button id="btn-grid" class="btn btn-default"><i class="fa fa-th"></i></button>
	</div>
	
	<?php if($models != null): ?>
		
		
		<?php foreach($models as $model): ?>
			<?php $orderPage = Yii::app()->createUrl("users/transaksiDetail/order/id/".$model->id_menu); ?>
			<?php $menu = Menu::model()->findByPK($model->id_menu);?>
			<div class="box-menu">
				<div class="image">
					<?php 
							                                                        
                                                        $thumbFile = $_SERVER['DOCUMENT_ROOT'].'/images/thumb/product_'.$model->id_menu . '_thumb.jpg';
                                                         $thumbURL =  Yii::app()->getBaseUrl(true).'/images/thumb/product_'.$model->id_menu . '_thumb.jpg';
                                                       
                                                        if(!file_exists($thumbFile)){
                                                              // link to default image 
                                                               $thumbURL =    Yii::app()->getBaseUrl(true).'/images/menu_default.jpg';
							}
                                                        
                                                        
						?>
						
                                            <img src="<?php echo $thumbURL; ?>" width="65px" height="65px" alt=" "/>
				</div>
				<div class="title">
					<a href="#<?php //echo Yii::app()->createUrl("users/menu/detail/id/".$menu->id_menu); ?>"><?php echo $menu->nama_menu; ?></a>
					<a href="<?php echo Yii::app()->createUrl("users/menu/list/id_tenant/".$menu->id_tenant); ?>" class="tenant"><i class="fa fa-home"></i> <?php echo $menu->idTenant->nama_warung; ?></a>
				</div>
				<div class="price">Rp. <?php echo $menu->hrg_jual; ?></div>
				<div class="status">
					<span class="likes">
						<a href="#"><i class="fa fa-thumbs-up"></i> 300 Likes</a>
					</span>
					<span class="separator">|</span>
					<span class="rating">
						<a href="#"><span class="rating-avg" value="<?php echo $menu->star;?>"></span></a>
					</span>
				</div>
				<div class="button">
					<?php
						if($menu->available())
							echo '<a href="#form-modal" class="btn-1 pesan" id="btn-order" data-toggle="modal" data-id-menu='.$model->id_menu.'data-nama-menu='.$model->nama_menu.'data-hrg-menu='.$model->hrg_jual.'>Pesan</a>';
						else
							echo '<a href="#" class="btn-1 pesan" id="btn-order">Out Of Stock</a>';
					?>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>