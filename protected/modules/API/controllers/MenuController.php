<?php

class MenuController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column-user';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('getMenuByListOfId','getMenuById',
                                    'getMenuUpdated','getMenuByTenant', 'getMenuByName',
                                    'getMenuByTheme','getMenuByKeyword','getMenuByPopularity',
                                    'getMenuByReview','getMenuByLatest','getMenuOnPromotion','getMenuByAvailability'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
        //--------------------------- A P I ---------------------------------
        public function actionGetMenuByListOfId(array $ids){
            $models = array();
            
            //define criteria object
            $criteria = new CDbCriteria;
            $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
            $criteria->addInCondition('id_menu', $ids);
            
            $models = Menu::model()->findAll($criteria);

            echo CJSON::encode($models);
        }
        
        public function actionGetMenuById($id, $updateTime){
            $model = new Menu;
            
            $criteria = new CDbCriteria;
            $criteria->select = 'id_menu,nama_menu,id_tenant,hrg_jual,star';
            $criteria->addCondition('id_menu = ' .$id);
            $criteria->addCondition('update_dateTime > ' .$updateTime);
            
            $model = Menu::model()->find($criteria);
            
            echo CJSON::encode($model);
        }
        
        public function actionGetMenuUpdated($startTime, $endTime){
            $models = array();
            $criteria = new CDbCriteria;

            $criteria->select = 'id_menu';
            $criteria->addBetweenCondition('update_dateTime', $startTime, $endTime);
            
            $models = Menu::model()->findAll($criteria);
            $idMenu = array();
            foreach($models as $item){
                $idMenu[] = $item->id_menu;
            }
            
            echo CJSON::encode($idMenu);
        }
        
        public function actionGetMenuByTenant($keyword){
            $models = array();
            $criteria = new CDbCriteria;

            $criteria->select = 'id_tenant';
            $criteria->addSearchCondition('nama_warung', strtolower($keyword));
            $models = Tenant::model()->findAll($criteria);
            
            $idTenants = array();
            foreach($models as $item){
                $idTenants[] = $item->id_tenant;
            }
            
            $criteria1 = new CDbCriteria;
            $criteria1->select = 'id_menu';
            $criteria1->addInCondition('id_tenant', $idTenants);
            
            $models = Menu::model()->findAll($criteria1);
            $idMenu = array();
            foreach($models as $item){
                $idMenu[] = $item->id_menu;
            }
            
            return $idMenu;
        }
        
        public function actionGetMenuByName($keyword){
            $models = array();
            $criteria = new CDbCriteria;

            $criteria->select = 'id_menu';
            $criteria->addSearchCondition('nama_menu', strtolower($keyword));
            
            $models = Menu::model()->findAll($criteria);
            $idMenu = array();
            foreach($models as $item){
                $idMenu[] = $item->id_menu;
            }
            
            return $idMenu;
        }
        
        public function actionGetMenuByTheme($keyword){
            $models = array();
            $criteria = new CDbCriteria;

            $criteria->select = 'id_tema';
            $criteria->addSearchCondition('nama_tema', strtolower($keyword));
            $models = Tema::model()->findAll($criteria);
            
            $idTema = array();
            foreach($models as $item){
                $idTema[] = $item->id_tema;
            }
            
            $criteria1 = new CDbCriteria;
            $criteria1->select = 'id_menu';
            $criteria1->addInCondition('id_tema', $idTema);
            
            $models = TemaMenu::model()->findAll($criteria1);
            $idMenu = array();
            foreach($models as $item){
                $idMenu[] = $item->id_menu;
            }
            
            return $idMenu;
        }
        
        public function actionGetMenuByKeyword($keyword, $hashCode){
            $models = array();
            $key = strtolower($keyword) .'_' . date("W");
            if(Yii::app()->cache->get($key)){
                $cacheData = Yii::app()->cache->get($key);
                if($cacheData['hashCode'] === $hashCode)
                        echo CJSON::encode($this->addHashCode($hashCode, $models));
                else{
                        echo CJSON::encode($cacheData);
                }
            } else {
                $idName = $this->actionGetMenuByName(strtolower($keyword));
                $idTenant = $this->actionGetMenuByTenant(strtolower($keyword));
                //$idTema = $this->actionGetMenuByTheme(strtolower($keyword)); 
                
                //$temp = array_merge($idName, $idTenant, $idTema);
                $temp = array_merge($idName, $idTenant);
                $hashCode = uniqid('hash_');

                $result['hashCode'] = $hashCode;
                $result['idMenu'] = $temp;
                $keyword = $keyword .'_' . date("W");
                
                Yii::app()->cache->set(strtolower($keyword), $result);
                echo CJSON::encode($result);
                //echo CJSON::encode($this->addHashCode($keyword, $result));
            }
        }
        
        public function actionGetMenuByPopularity($hashCode=''){
            $hash = $hashCode . date("W");
            if(Yii::app()->cache->get($hash)){
                $models = Yii::app()->cache->get($hash);
                
                echo CJSON::encode($this->addHashCode($hashCode, $models));
            } else {
                $models = array();
                $criteria = new CDbCriteria;
             
                $criteria->select = 't.id_menu, sum(td.jumlah) jml';
                $criteria->join = 'LEFT JOIN tbl_transaksi_detail td ON t.id_menu = td.id_menu';
                $criteria->group = 'td.id_menu';
                $criteria->order = 'jml DESC';
                
                $models = Menu::model()->findAll($criteria);
                $hashCode = uniqid('hash_');
                $hash = $hashCode . date("W");
                
                Yii::app()->cache->set($hash, $models);
                echo CJSON::encode($this->addHashCode($hashCode, $models));
            }
        }
        
        public function actionGetMenuByReview($hashCode){
            
        }
        
        public function actionGetMenuByLatest($hashCode=''){
            $hash = $hashCode . date("W");
            if(Yii::app()->cache->get($hash)){
                $models = Yii::app()->cache->get($hash);
                
                echo CJSON::encode($this->addHashCode($hashCode, $models));
            } else {
                $models = array();
                $criteria = new CDbCriteria;
             
                $criteria->select = 'id_menu';
                $criteria->order = 'id_menu DESC';
                
                $models = Menu::model()->findAll($criteria);
                $hashCode = uniqid('hash_');
                $hash = $hashCode . date("W");
                
                Yii::app()->cache->set($hash, $models);
                echo CJSON::encode($this->addHashCode($hashCode, $models));
            }
        }
        
        public function actionGetMenuOnPromotion($hashCode){
            if(Yii::app()->cache->get($hashCode)){
                $models = Yii::app()->cache->get($hashCode);
                
                echo CJSON::encode($this->addHashCode($hashCode, $models));
            } else {
                $models = array();
                $criteria = new CDbCriteria;
             
                $criteria->select = 'id_menu';
                
                $models = SpesialMenu::model()->findAll($criteria);
                $hashCode = uniqid('hash_');
                
                Yii::app()->cache->set($hashCode, $models);
                echo CJSON::encode($this->addHashCode($hashCode, $models));
            }
        }
        
        public function actionGetMenuByAvailability(){
            $command = Yii::app()->db->createCommand("SELECT t.id_menu, (kh.jml_stock - kh.jml_terbeli) as jml FROM tbl_menu as t LEFT JOIN tbl_kapasitas_harian as kh ON t.id_menu = kh.id_menu AND kh.tanggal='".date('Y-m-d')."'");
            $q = $command->queryAll();
			
            $result = array();
            foreach ($q as $item) {
                if($item['jml'] > 0){
                    $menu['id_menu'] = $item['id_menu'];
                    $menu['stock'] = $item['jml'];
                    $result[]=$menu;
                }
            }
            
            echo CJSON::encode($result);
        }
        
        private function addHashCode($hash, $models){
            $ids = array();
            foreach($models as $item){
                    $temp = $item->id_menu;
                    $ids[] = $temp;
                }
            $result['hashCode'] = $hash;
            $result['idMenu'] = $ids;
            return $result;
        }
        
}