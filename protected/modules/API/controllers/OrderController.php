<?php

class OrderController extends CController{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('getCartByCustomer', 'addMenuToCart','checkOutOrder'),
                'users'=>array('*'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),
        );
    }
    
    public function actionGetCartByCustomer($nim){
        if(isset(Yii::app()->request->cookies[$nim])){
            $items = array();
            $cookies = Yii::app()->request->cookies[$nim]->value;
            $temp = CJSON::decode($cookies);
            foreach($temp as $i=>$model){
                $obj['id_menu'] = $temp[$i]['id_menu'];
                $obj['quantity'] = $temp[$i]['quantity'];
                $items[] = $obj;
            }
            echo CJSON::encode($items);
        } else {
            echo CJSON::encode('data not found');
        }
    }
    
    public function actionAddMenuToCart($nim, $id_menu, $quantity, $catatan){
        $items = array();
        $item['id_menu'] = $id_menu;
        $item['quantity'] = $quantity;
        $item['catatan'] = $catatan;
        
        if(isset(Yii::app()->request->cookies[$nim])){
            $cookies = Yii::app()->request->cookies[$nim]->value;
            $temp = CJSON::decode($cookies);
            foreach($temp as $i=>$model){
                $obj = $temp[$i];
                $items[] = $obj;
            }
            $items[] = $item;

            $encode = CJSON::encode($items);
            Yii::app()->request->cookies[$nim] = new CHttpCookie($nim, $encode);
        } else {
            $encode = CJSON::encode($item);
            Yii::app()->request->cookies[$nim] = new CHttpCookie($nim, $encode);
        }
    }
    
    public function actionCheckOutOrder($nim, $total, $nominal_pembayaran){
        
    }
}
?>


