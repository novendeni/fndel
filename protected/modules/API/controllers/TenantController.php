<?php

class TenantController extends CController{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('getTenantByTheme', 'getTenantByName','getTenatByKeyword','getTenatUpdated'),
                'users'=>array('*'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),
        );
    }
    
    public function actionGetTenantByTheme($keyword){
        $models = array();
        $criteria = new CDbCriteria;

        $criteria->select = 'id_tema';
        $criteria->addSearchCondition('nama_tema', strtolower($keyword));

        $models = Tema::model()->findAll($criteria);
        $idTema = array();
        foreach($models as $item){
            $idTema[] = $item->id_tema;
        }
        
        $criteria1 = new CDbCriteria;
        $criteria1->select = 'id_menu';
        $criteria1->addInCondition('id_tema', $idTema);

        $models = TemaMenu::model()->findAll($criteria1);
        $idMenu = array();
        foreach($models as $item){
            $idMenu[] = $item->id_menu;
        }
        
        $criteria2 = new CDbCriteria;
        $criteria2->select = 'id_tenant';
        $criteria2->addInCondition('id_tenant', $idMenu);

        $models = Tenant::model()->findAll($criteria2);
        $idTenant = array();
        foreach($models as $item){
            $idTenant[] = $item->id_tenant;
        }

        return $idTenat;
    }
    
    public function actionGetTenantByName($keyword){
        $models = array();
        $criteria = new CDbCriteria;

        $criteria->select = 'id_tenant';
        $criteria->addSearchCondition('nama_warung', strtolower($keyword));

        $models = Tenant::model()->findAll($criteria);
        $idTenant = array();
        foreach($models as $item){
            $idTenant[] = $item->id_tenant;
        }

        return $idTenat;
    }
    
    public function actionGetTenantByKeyword($keyword,$hashCode){
        $models = array();
        $key = strtolower($keyword) .'_' . date("W");
        if(Yii::app()->cache->get($key)){
            $cacheData = Yii::app()->cache->get($key);
            if($cacheData['hashCode'] === $hashCode)
                    echo CJSON::encode($this->addHashCode($hashCode, $models));
            else{
                    echo CJSON::encode($cacheData);
            }
        } else {
            $idName = $this->actionGetTenantByName($keyword);
            //$idTema = $this->actionGetTenantByTheme($keyword);

            //$temp = array_merge($idName, $idTema);
            $temp = $idName;
            $hashCode = uniqid('hash_');

            $result['hashCode'] = $hashCode;
            $result['idTenat'] = $temp;
            $keyword = strtolower($keyword) .'_' . date("W");

            Yii::app()->cache->set($keyword, $result);
            echo CJSON::encode($result);
            //echo CJSON::encode($this->addHashCode($keyword, $result));
        }
    }
    
    public function actionGetTenantUpdated($startTime, $endTime){
        $models = array();
        $criteria = new CDbCriteria;

        $criteria->select = 'id_tenant';
        $criteria->addBetweenCondition('update_dateTime', $startTime, $endTime);

        $models = Tenant::model()->findAll($criteria);
        $idTenant = array();
        foreach($models as $item){
            $idTenant[] = $item->id_tenant;
        }

        echo CJSON::encode($idTenant);
    }
}
?>

