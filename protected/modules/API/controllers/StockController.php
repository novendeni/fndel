<?php

class StockController extends CController{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('getStockByMenuId', 'getStockByMenuIdList'),
                'users'=>array('*'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),
        );
    }
    
    public function actionGetStockByMenuId($id){
        $command = Yii::app()->db->createCommand("SELECT (kh.jml_stock - kh.jml_terbeli) as jml FROM tbl_menu as t LEFT JOIN tbl_kapasitas_harian as kh ON t.id_menu = kh.id_menu AND kh.tanggal='".date('Y-m-d')."' WHERE t.id_menu = ".$id);
        $q = $command->query();
        
        echo CJSON::encode($stock);
    }
    
    public function actionGetStockByMenuIdList(array $ids){
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.id_menu',$ids);

        $command = Yii::app()->db->createCommand("SELECT (kh.jml_stock - kh.jml_terbeli) as jml FROM tbl_menu as t LEFT JOIN tbl_kapasitas_harian as kh ON t.id_menu = kh.id_menu AND kh.tanggal='".date('Y-m-d')."' WHERE ".$criteria->condition);
        $q = $command->queryAll(true, $criteria->params);
        
        $result = array();
        foreach($q as $item){
            $result[] = $item['jml'];
        }
        echo CJSON::encode($result);
    }
}
?>

