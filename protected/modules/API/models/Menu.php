<?php

/**
 * This is the model class for table "tbl_menu".
 *
 * The followings are the available columns in table 'tbl_menu':
 * @property integer $id_menu
 * @property integer $id_tenant
 * @property string $nama_menu
 * @property string $kategori
 * @property string $deskripsi
 * @property string $bahan_dasar_utama
 * @property integer $hrg_jual
 * @property integer $hrg_partner
 * @property string $bumbu
 * @property string $foto
 * @property string $fotoType
 * @property string $star
 * @property string $update_dateTime
 */
class Menu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tenant, nama_menu, kategori, bahan_dasar_utama, hrg_jual, hrg_partner, bumbu, foto, fotoType, update_dateTime', 'required'),
			array('id_tenant, hrg_jual, hrg_partner', 'numerical', 'integerOnly'=>true),
			array('nama_menu, kategori, bahan_dasar_utama, bumbu', 'length', 'max'=>40),
			array('fotoType', 'length', 'max'=>8),
			array('star', 'length', 'max'=>10),
			array('deskripsi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_menu, id_tenant, nama_menu, kategori, deskripsi, bahan_dasar_utama, hrg_jual, hrg_partner, bumbu, foto, fotoType, star, update_dateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_menu' => 'Id Menu',
			'id_tenant' => 'Id Tenant',
			'nama_menu' => 'Nama Menu',
			'kategori' => 'Kategori',
			'deskripsi' => 'Deskripsi',
			'bahan_dasar_utama' => 'Bahan Dasar Utama',
			'hrg_jual' => 'Hrg Jual',
			'hrg_partner' => 'Hrg Partner',
			'bumbu' => 'Bumbu',
			'foto' => 'Foto',
			'fotoType' => 'Foto Type',
			'star' => 'Star',
			'update_dateTime' => 'Update Date Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('id_tenant',$this->id_tenant);
		$criteria->compare('nama_menu',$this->nama_menu,true);
		$criteria->compare('kategori',$this->kategori,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('bahan_dasar_utama',$this->bahan_dasar_utama,true);
		$criteria->compare('hrg_jual',$this->hrg_jual);
		$criteria->compare('hrg_partner',$this->hrg_partner);
		$criteria->compare('bumbu',$this->bumbu,true);
		$criteria->compare('foto',$this->foto,true);
		$criteria->compare('fotoType',$this->fotoType,true);
		$criteria->compare('star',$this->star,true);
		$criteria->compare('update_dateTime',$this->update_dateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
}
