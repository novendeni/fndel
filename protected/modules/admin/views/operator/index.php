<?php
/* @var $this OperatorController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Operators',
);

$this->menu=array(
	array('label'=>'Create Operator','url'=>array('create')),
	array('label'=>'Manage Operator','url'=>array('admin')),
);
?>

<h1>Operators</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>