<?php
/* @var $this CustomerController */
/* @var $data Customer */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('nim')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nim),array('view','id'=>$data->nim)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kos')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->no_kamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area')); ?>:</b>
	<?php echo CHtml::encode($data->area); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lhr')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_lhr')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_lhr); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('no_hp')); ?>:</b>
	<?php echo CHtml::encode($data->no_hp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sisa_saldo')); ?>:</b>
	<?php echo CHtml::encode($data->sisa_saldo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jns_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->jns_kelamin); ?>
	<br />

	*/ ?>

</div>