<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
<div class="row">
    <div class="span3">
        <?php
			$this->widget('bootstrap.widgets.TbNav', array(
				'type' => TbHtml::NAV_TYPE_LIST,
				'items' => array(
					array('label'=>'Tenant'),
					array('label'=>'Tambah Tenant', 'url'=>array('/admin/tenant/create')),
					array('label'=>'Manage Tenant', 'url'=>array('/admin/tenant/admin')),
					array('label'=>'Menu'),
					array('label'=>'Tambah Menu', 'url'=>array('/admin/menu/create')),
					array('label'=>'Manage Menu', 'url'=>array('/admin/menu/admin')),
					array('label'=>'Customer'),
					array('label'=>'Daftar Customer', 'url'=>array('/admin/customer/create')),
					array('label'=>'Manage Customer', 'url'=>array('/admin/customer/admin')),
					array('label'=>'Operator'),
					array('label'=>'Daftar Operator', 'url'=>array('/admin/operator/create')),
					array('label'=>'Manage Operator', 'url'=>array('/admin/operator/admin')),
					array('label'=>'Kurir'),
					array('label'=>'Daftar Kurir', 'url'=>array('/admin/kurir/create')),
					array('label'=>'Manage Kurir', 'url'=>array('/admin/kurir/admin')),
					array('label'=>'Tema'),
					array('label'=>'Tambah Tema', 'url'=>array('/admin/temamenu/create')),
					array('label'=>'Manage Tema', 'url'=>array('/admin/temamenu/admin')),
					array('label'=>'Paket Menu'),
					array('label'=>'Tambah Paket', 'url'=>array('/admin/paketmenu/create')),
					array('label'=>'Paket Detail', 'url'=>array('/admin/paketmenudetail/create')),
					array('label'=>'Manage Paket', 'url'=>array('/admin/paketmenu/admin')),
					array('label'=>'Spesial Menu'),
					array('label'=>'Tambah Spesial Menu', 'url'=>array('/admin/spesialmenu/create')),
					array('label'=>'Manage Spesial Menu', 'url'=>array('/admin/spesialmenu/admin')),
					TbHtml::menuDivider(),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
				)
			));
        ?>
    </div>
	
	<div class="span9">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
</div>
<?php $this->endContent(); ?>