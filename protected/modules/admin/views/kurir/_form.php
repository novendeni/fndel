<?php
/* @var $this KurirController */
/* @var $model Kurir */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'kurir-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib di isi!!</p><br />

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'nama',array('span'=>3,'maxlength'=>40)); ?>
			
			<?php echo $form->passwordFieldControlGroup($model,'password',array('span'=>3,'maxlength'=>40)); ?>

            <?php echo $form->dateFieldControlGroup($model,'tgl_lahir',array('span'=>3)); ?>

            <?php echo $form->textFieldControlGroup($model,'no_hp',array('span'=>3,'maxlength'=>20)); ?>

            <?php echo $form->textFieldControlGroup($model,'bank',array('span'=>3,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'no_rekening',array('span'=>3,'maxlength'=>40)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Daftar' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->