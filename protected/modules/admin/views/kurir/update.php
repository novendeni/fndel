<?php
/* @var $this KurirController */
/* @var $model Kurir */
?>

<?php
$this->breadcrumbs=array(
	'Kurirs'=>array('index'),
	$model->id_kurir=>array('view','id'=>$model->id_kurir),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kurir', 'url'=>array('index')),
	array('label'=>'Create Kurir', 'url'=>array('create')),
	array('label'=>'View Kurir', 'url'=>array('view', 'id'=>$model->id_kurir)),
	array('label'=>'Manage Kurir', 'url'=>array('admin')),
);
?>

    <h1>Update Kurir <?php echo $model->id_kurir; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>