<?php
/* @var $this KurirController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Kurirs',
);

$this->menu=array(
	array('label'=>'Create Kurir','url'=>array('create')),
	array('label'=>'Manage Kurir','url'=>array('admin')),
);
?>

<h1>Kurirs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>