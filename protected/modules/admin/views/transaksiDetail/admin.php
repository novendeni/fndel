<?php
/* @var $this TransaksiDetailController */
/* @var $model TransaksiDetail */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#transaksi-detail-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Transaksi Detail</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'transaksi-detail-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_transaksi_detail',
		//'id_transaksi',
		array(
			'nama'=>'idTransaksi',
			'type'=>'raw',
			'header'=>'Nama Customer',
			'value'=>'$data->idTransaksi->nim',
		),
		//'id_menu',
		'jumlah',
		'subtotal',
		'catatan',
		/*
		'testimonial',
		'rating',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>