<?php
/* @var $this TransaksiDetailController */
/* @var $data TransaksiDetail */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_transaksi_detail')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_transaksi_detail),array('view','id'=>$data->id_transaksi_detail)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_transaksi')); ?>:</b>
	<?php echo CHtml::encode($data->id_transaksi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_menu')); ?>:</b>
	<?php echo CHtml::encode($data->id_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subtotal')); ?>:</b>
	<?php echo CHtml::encode($data->subtotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('catatan')); ?>:</b>
	<?php echo CHtml::encode($data->catatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('testimonial')); ?>:</b>
	<?php echo CHtml::encode($data->testimonial); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('rating')); ?>:</b>
	<?php echo CHtml::encode($data->rating); ?>
	<br />

	*/ ?>

</div>