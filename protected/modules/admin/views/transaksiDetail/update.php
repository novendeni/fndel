<?php
/* @var $this TransaksiDetailController */
/* @var $model TransaksiDetail */
?>

<?php
$this->breadcrumbs=array(
	'Transaksi Details'=>array('index'),
	$model->id_transaksi_detail=>array('view','id'=>$model->id_transaksi_detail),
	'Update',
);

$this->menu=array(
	array('label'=>'List TransaksiDetail', 'url'=>array('index')),
	array('label'=>'Create TransaksiDetail', 'url'=>array('create')),
	array('label'=>'View TransaksiDetail', 'url'=>array('view', 'id'=>$model->id_transaksi_detail)),
	array('label'=>'Manage TransaksiDetail', 'url'=>array('admin')),
);
?>

    <h1>Update TransaksiDetail <?php echo $model->id_transaksi_detail; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>