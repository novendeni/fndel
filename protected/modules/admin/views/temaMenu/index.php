<?php
/* @var $this TemaMenuController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Tema Menus',
);

$this->menu=array(
	array('label'=>'Create TemaMenu','url'=>array('create')),
	array('label'=>'Manage TemaMenu','url'=>array('admin')),
);
?>

<h1>Tema Menus</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>