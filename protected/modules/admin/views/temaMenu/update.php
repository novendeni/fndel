<?php
/* @var $this TemaMenuController */
/* @var $model TemaMenu */
?>

<?php
$this->breadcrumbs=array(
	'Tema Menus'=>array('index'),
	$model->id_tema=>array('view','id'=>$model->id_tema),
	'Update',
);

$this->menu=array(
	array('label'=>'List TemaMenu', 'url'=>array('index')),
	array('label'=>'Create TemaMenu', 'url'=>array('create')),
	array('label'=>'View TemaMenu', 'url'=>array('view', 'id'=>$model->id_tema)),
	array('label'=>'Manage TemaMenu', 'url'=>array('admin')),
);
?>

    <h1>Update TemaMenu <?php echo $model->id_tema; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>