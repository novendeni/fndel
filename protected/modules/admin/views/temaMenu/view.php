<?php
/* @var $this TemaMenuController */
/* @var $model TemaMenu */
?>

<?php
$this->breadcrumbs=array(
	'Tema Menus'=>array('index'),
	$model->id_tema,
);

$this->menu=array(
	array('label'=>'List TemaMenu', 'url'=>array('index')),
	array('label'=>'Create TemaMenu', 'url'=>array('create')),
	array('label'=>'Update TemaMenu', 'url'=>array('update', 'id'=>$model->id_tema)),
	array('label'=>'Delete TemaMenu', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tema),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TemaMenu', 'url'=>array('admin')),
);
?>

<h1>View TemaMenu #<?php echo $model->id_tema; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_tema',
		'id_menu',
		'nama_tema',
	),
)); ?>