<?php
/* @var $this TemaMenuController */
/* @var $model TemaMenu */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'tema-menu-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Kolom dengantanda <span class="required">*</span> wajib diisi.</p><br/>

        <?php echo $form->textFieldControlGroup($model,'nama_tema',array('span'=>3,'maxlength'=>40)); ?>
			
		<?php
			$tenant = CHtml::listData(Tenant::model()->findAll(array('order' => 'id_tenant')), 'id_tenant', 'nama_warung');
			echo TbHtml::dropDownListControlGroup('id_tenant','',$tenant, array('empty'=>'--Pilih Tenant--', 'span'=>3, 'label'=>'Nama Warung', 'ajax'=>array('type'=>'POST', 'url'=>Yii::app()->createUrl('admin/temamenu/loadmenu'),'update'=>'#idmenu', 'data'=>array('id_tenant'=>'js:this.value'))));
		?>
		
		<?php echo $form->dropDownListControlGroup($model,'id_menu',array(),array('empty'=>'--Pilih Menu--', 'span'=>3, 'id'=>'id_menu'));?>
		<?php //echo $form->textFieldControlGroup($model,'id_menu',array('span'=>3, 'class'=>'id_menu')); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Daftar' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->