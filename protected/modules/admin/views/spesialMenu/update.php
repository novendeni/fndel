<?php
/* @var $this SpesialMenuController */
/* @var $model SpesialMenu */
?>

<?php
$this->breadcrumbs=array(
	'Spesial Menus'=>array('index'),
	$model->id_spesial_menu=>array('view','id'=>$model->id_spesial_menu),
	'Update',
);

$this->menu=array(
	array('label'=>'List SpesialMenu', 'url'=>array('index')),
	array('label'=>'Create SpesialMenu', 'url'=>array('create')),
	array('label'=>'View SpesialMenu', 'url'=>array('view', 'id'=>$model->id_spesial_menu)),
	array('label'=>'Manage SpesialMenu', 'url'=>array('admin')),
);
?>

    <h1>Update SpesialMenu <?php echo $model->id_spesial_menu; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>