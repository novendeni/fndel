<?php
/* @var $this SpesialMenuController */
/* @var $model SpesialMenu */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'spesial-menu-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib di isi!!</p><br />

            <?php //echo $form->textFieldControlGroup($model,'id_menu',array('span'=>5)); ?>
			<?php 
				$menu = CHtml::listData(Menu::model()->findAll(array('order' => 'id_menu')), 'id_menu', 'nama_menu');
				echo $form->dropDownListControlGroup($model,'id_menu',$menu, array('span'=>3, 'label'=>'Menu')); 
			?>

            <?php echo $form->dateFieldControlGroup($model,'tanggal_awal',array('span'=>3)); ?>

            <?php //echo $form->textFieldControlGroup($model,'update_by',array('span'=>4,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'occurence',array('span'=>3,'maxlength'=>40)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Update',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->