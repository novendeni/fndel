<?php
/* @var $this SpesialMenuController */
/* @var $data SpesialMenu */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_spesial_menu')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_spesial_menu),array('view','id'=>$data->id_spesial_menu)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_menu')); ?>:</b>
	<?php echo CHtml::encode($data->id_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_awal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_awal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_by')); ?>:</b>
	<?php echo CHtml::encode($data->update_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('occurence')); ?>:</b>
	<?php echo CHtml::encode($data->occurence); ?>
	<br />


</div>