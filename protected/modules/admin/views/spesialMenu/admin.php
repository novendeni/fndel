<?php
/* @var $this SpesialMenuController */
/* @var $model SpesialMenu */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#spesial-menu-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Spesial Menu</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'spesial-menu-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_spesial_menu',
		'id_menu',
		array(
			'name'=>'idMenu',
			'type'=>'raw',
			'header'=>'Menu',
			'value'=>'$data->id_menu->nama_menu',
		),
		'tanggal_awal',
		'update_by',
		'occurence',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>