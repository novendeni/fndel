<?php
/* @var $this SpesialMenuController */
/* @var $model SpesialMenu */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_spesial_menu',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_menu',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'tanggal_awal',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'update_by',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'occurence',array('span'=>5,'maxlength'=>40)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->