<?php
/* @var $this TenantController */
/* @var $model Tenant */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'id'=>'tenant-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib diisi!</p><br />

		<?php echo $form->textFieldControlGroup($model,'nama_warung',array('span'=>3,'maxlength'=>40)); ?>

		<?php echo $form->textFieldControlGroup($model,'nama_pemilik',array('span'=>3,'maxlength'=>40)); ?>

		<?php echo $form->textFieldControlGroup($model,'no_hp',array('span'=>3,'maxlength'=>20)); ?>

		<?php echo $form->textFieldControlGroup($model,'alamat',array('span'=>3,'maxlength'=>40)); ?>

		<?php 
			echo $form->dropDownListControlGroup($model,'area',array(
				'belakang lapangan tenis'=>'Belakang Lapangan Tenis',
				'jalan sukabirus'=>'Jalan Sukabirus',
				'gang umayah'=>'Gang Umayah',
				'sukapura'=>'Sukapura',
				'perumahan permata buah batu'=>'Perumahan Permata Buah Batu',
				'perumahan pesona bali'=>'Perumahan Pesona Bali',
				'asrama putra'=>' Asrama Putra',
				'asrama putri'=>'Asrama Putri',
				'pga'=>'PGA',
			), array('empty'=>'-- Pilih Area --', 'span'=>3)); 
		?>

	<div class="form-actions">
		<?php echo TbHtml::submitButton($model->isNewRecord ? 'Daftar' : 'Save',array(
			'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->