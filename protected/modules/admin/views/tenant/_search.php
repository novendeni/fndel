<?php
/* @var $this TenantController */
/* @var $model Tenant */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id_tenant',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'nama_warung',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'nama_pemilik',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'no_hp',array('span'=>5,'maxlength'=>20)); ?>

                    <?php echo $form->textFieldControlGroup($model,'alamat',array('span'=>5,'maxlength'=>40)); ?>

                    <?php echo $form->textFieldControlGroup($model,'area',array('span'=>5,'maxlength'=>40)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->