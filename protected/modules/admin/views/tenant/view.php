<?php
/* @var $this TenantController */
/* @var $model Tenant */
?>

<?php
$this->breadcrumbs=array(
	'Tenants'=>array('index'),
	$model->id_tenant,
);

$this->menu=array(
	array('label'=>'List Tenant', 'url'=>array('index')),
	array('label'=>'Create Tenant', 'url'=>array('create')),
	array('label'=>'Update Tenant', 'url'=>array('update', 'id'=>$model->id_tenant)),
	array('label'=>'Delete Tenant', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tenant),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tenant', 'url'=>array('admin')),
);
?>

<h1>View Tenant #<?php echo $model->id_tenant; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_tenant',
		'nama_warung',
		'nama_pemilik',
		'no_hp',
		'alamat',
		'area',
	),
)); ?>