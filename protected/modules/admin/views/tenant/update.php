<?php
/* @var $this TenantController */
/* @var $model Tenant */
?>

<?php
$this->breadcrumbs=array(
	'Tenants'=>array('index'),
	$model->id_tenant=>array('view','id'=>$model->id_tenant),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tenant', 'url'=>array('index')),
	array('label'=>'Create Tenant', 'url'=>array('create')),
	array('label'=>'View Tenant', 'url'=>array('view', 'id'=>$model->id_tenant)),
	array('label'=>'Manage Tenant', 'url'=>array('admin')),
);
?>

    <h1>Update Tenant <?php echo $model->id_tenant; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>