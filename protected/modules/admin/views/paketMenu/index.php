<?php
/* @var $this PaketMenuController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Paket Menus',
);

$this->menu=array(
	array('label'=>'Create PaketMenu','url'=>array('create')),
	array('label'=>'Manage PaketMenu','url'=>array('admin')),
);
?>

<h1>Paket Menus</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>