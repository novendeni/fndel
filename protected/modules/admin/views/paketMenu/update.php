<?php
/* @var $this PaketMenuController */
/* @var $model PaketMenu */
?>

<?php
$this->breadcrumbs=array(
	'Paket Menus'=>array('index'),
	$model->id_paket_menu=>array('view','id'=>$model->id_paket_menu),
	'Update',
);

$this->menu=array(
	array('label'=>'List PaketMenu', 'url'=>array('index')),
	array('label'=>'Create PaketMenu', 'url'=>array('create')),
	array('label'=>'View PaketMenu', 'url'=>array('view', 'id'=>$model->id_paket_menu)),
	array('label'=>'Manage PaketMenu', 'url'=>array('admin')),
);
?>

    <h1>Update PaketMenu <?php echo $model->id_paket_menu; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>