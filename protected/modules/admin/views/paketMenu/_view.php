<?php
/* @var $this PaketMenuController */
/* @var $data PaketMenu */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_paket_menu')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_paket_menu),array('view','id'=>$data->id_paket_menu)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_paket')); ?>:</b>
	<?php echo CHtml::encode($data->nama_paket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_active')); ?>:</b>
	<?php echo CHtml::encode($data->is_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />


</div>