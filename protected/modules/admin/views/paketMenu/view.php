<?php
/* @var $this PaketMenuController */
/* @var $model PaketMenu */
?>

<?php
$this->breadcrumbs=array(
	'Paket Menus'=>array('index'),
	$model->id_paket_menu,
);

$this->menu=array(
	array('label'=>'List PaketMenu', 'url'=>array('index')),
	array('label'=>'Create PaketMenu', 'url'=>array('create')),
	array('label'=>'Update PaketMenu', 'url'=>array('update', 'id'=>$model->id_paket_menu)),
	array('label'=>'Delete PaketMenu', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_paket_menu),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PaketMenu', 'url'=>array('admin')),
);
?>

<h1>View PaketMenu #<?php echo $model->id_paket_menu; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_paket_menu',
		'nama_paket',
		'is_active',
		'harga',
	),
)); ?>