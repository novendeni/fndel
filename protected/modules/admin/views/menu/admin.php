<?php
/* @var $this MenuController */
/* @var $model Menu */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#menu-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Menu</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'menu-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_menu',
		//'id_tenant',
		array(
			'name'=>'idTenant',
			'type'=>'raw',
			'header'=>'Tenant',
			'value'=>'$data->idTenant->nama_warung',
		),
		'nama_menu',
		//'bahan_dasar_utama',
		'hrg_jual',
		'hrg_partner',
		/*
		'bumbu',
		'foto',
		'star',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>