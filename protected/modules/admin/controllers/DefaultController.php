<?php

class DefaultController extends CController
{
	public function actionIndex()
	{
		$this->layout = '/layouts/column2';
		$this->render('index');
	}
}