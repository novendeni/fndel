<?php

/**
 * This is the model class for table "tbl_operator".
 *
 * The followings are the available columns in table 'tbl_operator':
 * @property integer $id_operator
 * @property string $nama
 * @property string $password
 * @property string $no_hp
 * @property string $roles
 */
class Operator extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_operator';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, password, no_hp', 'required','message'=>'{attributes} tidak boleh kosong.'),
			array('nama', 'length', 'max'=>40),
			array('password, no_hp', 'length', 'max'=>128),
			array('no_hp', 'numerical', 'integerOnly'=>true,'message'=>'{attributes} harus berupa angka.'),
			array('roles', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_operator, nama, password, no_hp, roles', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_operator' => 'Id Operator',
			'nama' => 'Nama',
			'password' => 'Password',
			'no_hp' => 'No Hp',
			'roles' => 'Roles',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_operator',$this->id_operator);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('roles',$this->roles,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Operator the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
