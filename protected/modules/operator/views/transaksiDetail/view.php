<?php

?>

<?php $this->pageTitle=Yii::app()->name . ' - Detail Transaksi'; ?>

<div class="page-title">
	<div class="title"><h3>Detail Transaksi</h3></div>
</div>

<table class="table borderless">
	<tr>
		<td style="width:20%;">Nama</td>
		<td><?php echo $user->nama;?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td><?php echo $user->nama_kos .' / '. $user->no_kamar;?></td>
	</tr>
	<tr>
		<td>No HP</td>
		<td><?php echo $user->no_hp;?></td>
	</tr>
	<tr>
		<td>Total</td>
		<td><?php echo $trans->total;?></td>
	</tr>
</table>

<table class="table">
	<tr>
		<th style="width:30%">Warung</th>
		<th style="width:30%">Menu</th>
		<th>Jumlah</th>
		<th>Sub Total</th>
	</tr>
	<?php foreach($models as $i=>$detail): ?>
	<tr>
		<td>
			<?php $warung = Tenant::model()->findByAttributes(array('id_tenant'=>$detail->idMenu->id_tenant));?>
			<?php echo $warung->nama_warung;?>
		</td>
		<td><?php echo $detail->idMenu->nama_menu; ?></td>
		<td><?php echo $detail->jumlah; ?></td>
		<td><?php echo $detail->subtotal; ?></td>
	</tr>
	<?php endforeach;?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?> 
</div>

<a href="<?php echo Yii::app()->request->urlReferrer;?>" class="btn btn-1 pesan">Back</a>