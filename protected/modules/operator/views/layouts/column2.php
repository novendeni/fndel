<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
<div class="row">
    <div class="span3">
        <?php
			$this->widget('bootstrap.widgets.TbNav', array(
				'type' => TbHtml::NAV_TYPE_LIST,
				'items' => array(
					array('label'=>'Input Menu Harian', 'url'=>array('/operator/kapasitasHarian/create')),
					array('label'=>'Manage Menu Harian', 'url'=>array('/operator/kapasitasHarian/admin')),
					array('label'=>'Top Up Saldo', 'url'=>array('/operator/topUp/create')),
					array('label'=>'Manage Transaksi', 'url'=>array('/operator/transaksi/admin')),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
				)
			));
        ?>
    </div>
	
	<div class="span9">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
</div>
<?php $this->endContent(); ?>