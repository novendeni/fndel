<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/images/icon.png" rel="shortcut icon" type="image/png" />
		<link href="<?php echo Yii::app()->request->baseUrl;?>/css/font-awesome.min.css" rel="stylesheet" >
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fndel.css" rel="stylesheet">

	</head>
	
	<body>
	
		<div id="wrapper">
			<header>
				<div id="line-header"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							
						</div>
					</div>	
					<nav class="navbar navbar-default" role="navigation"> 
						<div class="navbar-header">
							<button type="button" id="menu-toggle" class="navbar-toggle btn-3" data-toggle="collapse" data-target="#navbar-collapse-1">
								<i class="fa fa-bars"></i> Menu
							</button>
							<button type="button" id="user-toggle" class="navbar-toggle btn-3" data-toggle="collapse" data-target="#navbar-collapse-2">
								<i class="fa fa-user"></i>
							</button>
							<a href="<?php echo Yii::app()->request->baseUrl; ?>/" id="main-logo"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a> 
						</div>
						<div class="collapse navbar-collapse" id="navbar-collapse-2"> 
							<ul id="user-box" class="nav navbar-nav">
								<li>
									<div class="btn-group">
										<?php 
										if(!Yii::app()->user->isGuest)
											echo '<a href="'.Yii::app()->createUrl('/site/logout').'"><i class="fa fa-sign-out"></i> Logout</a>';
										?>
									</div>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</header>
			
			<br/>
			
			<?php echo $content; ?>
			<!-- Content -->
			
			<footer>
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 no-padding">
							<center>&copy; Copyright 2014 - 2020 FnDel. All Rights Reserved</center>
						</div>
					</div>
				</div>
			</footer>
		</div>
		
		<div class="device-xs visible-xs"></div>
		<div class="device-sm visible-sm"></div>
		<div class="device-md visible-md"></div>
		
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.10.2.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fndel.js"></script>
	</body>
</html>