<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="container">
	<div class="span-5 last">
		<div id="sidebar">
			<?php
				$this->beginWidget('zii.widgets.CPortlet', array(
					'title'=>'Menu',
				));
				
				$this->widget('zii.widgets.CMenu', array(
				'items'=>array(
					array('label'=>'Pendaftaran', 'url'=>array('/pengajuan/create')),
					array('label'=>'List Calon Bayi', 'url'=>array('/kandidatbayi/index')),
					array('label'=>'Rekapitulasi Calon Bayi', 'url'=>array('/kandidatbayi/rekap')),
					array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
				),
				'htmlOptions'=>array('class'=>'Menu'),
				));
				
				$this->endWidget();
			?>
		</div>
	</div>
	
	<div class="span-19">
		<?php echo $content; ?>
	</div>
</div><!-- content -->

<?php $this->endContent(); ?>