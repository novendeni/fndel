<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
			<section id="content">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div id="side-menu" class="list-group">
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#menu-edit">
									&nbsp;Kapasitas Harian
								</a>
								<div id="menu-edit" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('/operator/kapasitasHarian/create'); ?>" class="list-group-item"><i class="fa fa-edit"></i> Input Kapasitas Harian</a>
									<a href="<?php echo Yii::app()->createUrl('/operator/kapasitasHarian/manage'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Manage Kapasitas Harian</a>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#transaksi">
									&nbsp;Transaksi
								</a>
								<div id="transaksi" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('/operator/transaksi/progress'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Transaksi </a>
									<a href="<?php echo Yii::app()->createUrl('/operator/transaksi/setKurir'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Set Kurir</a>
									<a href="<?php echo Yii::app()->createUrl('/operator/transaksi/manage'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Manage Transaksi</a>
									<a href="<?php echo Yii::app()->createUrl('/operator/transaksi/report'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Report Transaksi</a>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#topup">
									&nbsp;Top Up Saldo
								</a>
								<div id="topup" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('/operator/topUp/create'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Top Up</a>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#user">
									&nbsp;User
								</a>
								<div id="user" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('/operator/customer/approve'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Approve User</a>
									<a href="<?php echo Yii::app()->createUrl('/operator/customer/manage'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Manage User</a>
									<a href="<?php echo Yii::app()->createUrl('/operator/customer/history'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> History Transaksi User</a>
									<a href="<?php echo Yii::app()->createUrl('/operator/topUp/history'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> History Top Up User</a>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#tenant">
									&nbsp;Tenant
								</a>
								<div id="tenant" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('/operator/tenant/create'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Create Tenant</a>
									<a href="<?php echo Yii::app()->createUrl('/operator/tenant/manage'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Manage Tenant</a>
								</div>
								<a class="list-group-item header" data-toggle="collapse" data-parent="#accordion" href="#kost">
									&nbsp;Kost
								</a>
								<div id="kost" class="panel-collapse collapse in">
									<a href="<?php echo Yii::app()->createUrl('/operator/kost/create'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Create Kost</a>
									<a href="<?php echo Yii::app()->createUrl('/operator/kost/manage'); ?>" class="list-group-item"><i class="fa fa-check-square-o"></i> Manage Kost</a>
								</div>
							</div>
						</div>
						<div class="col-md-9 col-sm-9 left">
							<?php echo $content; ?>
						</div>
					</div>
				</div>
			</section>

<?php $this->endContent(); ?>