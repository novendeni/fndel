<?php
/* @var $this KapasitasHarianController */
/* @var $model KapasitasHarian */
/* @var $form TbActiveForm */
?>

<div class="form">
	<?php echo CHtml::beginForm(); ?>
		<div class="form-group">
			<?php echo CHtml::label('Menu',false);?>
			<?php echo CHtml::textField('Menu',$model->idMenu->nama_menu,array('class'=>'form-control','disabled'=>true));?>
			<?php echo CHtml::activeHiddenField($model,'id_menu');?>
		</div>
		<div class="form-group">
			<?php echo CHtml::label('Tanggal',false);?> 
			<?php echo CHtml::textField('Tanggal',$model->tanggal,array('class'=>'form-control','disabled'=>true));?>
		</div>
		<div class="form-group">
			<?php echo CHtml::label('Stock',false);?>
			<?php echo CHtml::activeNumberField($model,'jml_stock',array('class'=>'form-control'));?> 
		</div>
		<?php echo CHtml::submitButton('Update',array('class'=>'btn btn-primary')); ?>
	<?php echo CHtml::endForm(); ?>
</div>