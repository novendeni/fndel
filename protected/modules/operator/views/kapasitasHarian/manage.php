<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="col-md-7 col-sm-7">
	<div class="form">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'register-form',
		)); ?>
		<div class="form-group form-inline">
			<div class="input-append date date-picker inline">
				<?php echo $form->labelEx($model,'tanggal',array('label'=>'Tanggal'));?>
				<?php echo $form->textField($model,'tanggal', array('class'=>'form-control','placeholder'=>'dd-mm-yyyy', 'data-date'=>'12-02-2012', 'data-date-format'=>'dd-mm-yyyy', 'data-date-viewmode'=>'years')); ?>
				<?php echo $form->error($model,'tanggal', array('class'=>'alert-text')); ?>
				
				<button type="submit" class="btn btn-primary">Cari</button>
			</div>
		</div>
		<?php $this->endWidget();?>
	</div>
</div>

	<table class="table">
		<tr>
			<th>Tenant</th>
			<th>Menu</th>
			<th>Terjual</th>
			<th>Stock</th>
			<th style="width:10%">Action</th>
		</tr>
		<?php if($models != null): ?>
			<?php foreach($models as $item):?>
				<tr>
					<?php $tenant = Tenant::model()->findByAttributes(array('id_tenant'=>$item->idMenu->id_tenant));?>
					<td><?php echo $tenant->nama_warung;?></td>
					<td><?php echo $item->idMenu->nama_menu;?></td>
					<td><?php echo $item->jml_terbeli;?></td>
					<td><?php echo $item->jml_stock; ?></td>
					<td>
						<a href="<?php echo Yii::app()->createUrl("operator/kapasitasHarian/update1",array("id"=>$item->id_menu, "tanggal"=>$item->tanggal)); ?>"><i class="fa fa-pencil"></i></a>
					</td>
				</tr>
			<?php endforeach;?>
		<?php endif; ?>
	</table>	