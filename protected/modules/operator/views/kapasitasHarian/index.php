<?php
/* @var $this KapasitasHarianController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Kapasitas Harians',
);

$this->menu=array(
	array('label'=>'Create KapasitasHarian','url'=>array('create')),
	array('label'=>'Manage KapasitasHarian','url'=>array('admin')),
);
?>

<h1>Kapasitas Harians</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>