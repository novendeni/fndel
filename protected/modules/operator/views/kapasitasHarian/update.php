<?php
/* @var $this KapasitasHarianController */
/* @var $model KapasitasHarian */
?>

<div class="page-title">
	<div class="title"><h3>Update Kapasitas Harian</h3></div>
</div>

<?php $this->renderPartial('_formUpdate', array('model'=>$model)); ?>