<?php
/* @var $this KapasitasHarianController */
/* @var $model KapasitasHarian */
?>

<div class="page-title">
	<div class="title"><h3>Input Kapasitas Harian</h3>&nbsp;</div>
</div>

<?php $this->renderPartial('_form', array('model'=>$model,'models'=>$models)); ?>