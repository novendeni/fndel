<?php
/* @var $this KapasitasHarianController */
/* @var $model KapasitasHarian */
/* @var $form TbActiveForm */

?>
<div class="row">
	<div class="form">
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'menu-harian-form',
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'POST',
		)); ?>
		<div class="form-group form-inline">
			<div class="input-append date date-picker inline">
				<?php $data = CHtml::listData(Tenant::model()->findAll(array('order' => 'id_tenant')), 'id_tenant', 'nama_warung');?>
				<?php echo $form->labelEx($model,'id_tenant',array('label'=>'Nama Warung'));?>
				<?php echo '&nbsp;';?>
				<?php echo $form->dropDownList($model,'id_tenant', $data, array('class'=>'form-control','empty'=>'--Pilih--','selected'=>'selected')); ?>
				<?php echo $form->error($model,'id_tenant', array('class'=>'alert-text')); ?>
				<?php echo '&nbsp;';?>
				
				<button type="submit" class="btn btn-primary">Cari</button>
			</div>
		</div>
		<?php $this->endWidget();?>
		
		<div class="" align="right">
		<div class="form-group form-inline">
			<input type="number" name="quantity" min="1" max="5" id="jml">
			<input type="button" id="batch_btn" value="Submit" class="btn btn-primary" />
		</div>
	</div>
	</div>
</div>

<br />

<div class="form">
	<?php echo CHtml::beginForm(); ?>
	<table class="table">
		<tr>
			<th>Nama Menu</th>
			<th width="20%">Stock</th>
		</tr>
		<?php foreach($models as $i=>$item): ?>
		<tr>
			<td>
				<?php echo TbHtml::textField('nama_menu',$item->idMenu->nama_menu,array('disabled'=>true)); ?>
				<?php echo CHtml::activeHiddenField($item,"[$i]id_menu",array()); ?>
			</td>
			<td><?php echo TbHtml::activeTextField($item,"[$i]jml_stock", array('class'=>'jml_stock','span'=>2)); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
 
		<?php echo CHtml::submitButton('Save',array('class'=>'btn btn-primary')); ?>
	<?php echo CHtml::endForm(); ?>
</div><!-- form -->