<?php
/* @var $this KapasitasHarianController */
/* @var $model KapasitasHarian */
?>

<h1>View KapasitasHarian</h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id_menu',
		'tanggal',
		'jml_terbeli',
		'jml_stock',
	),
)); ?>