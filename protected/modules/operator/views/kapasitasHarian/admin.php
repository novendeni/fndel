<?php
/* @var $this KapasitasHarianController */
/* @var $model KapasitasHarian */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#kapasitas-harian-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Kapasitas Harian</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'kapasitas-harian-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_menu',
		array(
			'name'=>'idMenu',
			'type'=>'raw',
			'header'=>'Menu',
			'value'=>'$data->idMenu->nama_menu',
		),
		//'tanggal',
		//'jml_terbeli',
		//'jml_stock',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view'=>array(
					'url'=>'Yii::app()->createUrl("operator/kapasitasHarian/view/",array("id"=>$data->id_menu,"id1"=>$data->tanggal))',
				),
				'update'=>array(
					'url'=>'Yii::app()->createUrl("operator/kapasitasHarian/update/",array("id"=>$data->id_menu,"id1"=>$data->tanggal))',
				),
			),
		),
	),
)); ?>