<?php
/* @var $this KapasitasHarianController */
/* @var $data KapasitasHarian */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_menu')); ?>:</b>
	<?php echo CHtml::encode($data->id_menu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_terbeli')); ?>:</b>
	<?php echo CHtml::encode($data->jml_terbeli); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_stock')); ?>:</b>
	<?php echo CHtml::encode($data->jml_stock); ?>
	<br />


</div>