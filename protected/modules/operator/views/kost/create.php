<?php
/* @var $this KostController */
/* @var $model Kost */
/* @var $form TbActiveForm */
?>

<div class="page-title">
	<div class="title"><h3>Pendaftaran Kostan</h3></div>
</div>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'kost-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
        'class'=>'form-horizontal',
    ),
)); ?>
	
	<div class="form-group">
		<?php echo $form->label($model,'nama_kost',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php echo $form->textField($model,'nama_kost',array('class'=>'form-control','placeholder'=>'Nama Kost'));?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->label($model,'area',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php $areas = array_map('ucwords',CHtml::listData(Kost::model()->findAll(array('order'=>'area', 'group'=>'area')),'area','area'));?>
			<?php echo $form->dropDownList($model,'area',$areas, array('prompt'=>'-- Pilih Area --', 'class'=>'form-control')); ?>
			<?php //echo $form->textField($model,'area',array('class'=>'form-control','placeholder'=>'Area'));?>
		</div>
	</div>
	
	<button type="submit" class="btn btn-primary">Simpan</button>

    <?php $this->endWidget(); ?>

</div><!-- form -->