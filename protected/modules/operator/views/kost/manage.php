<?php
/* @var $this KostController */
/* @var $model Kost */
/* @var $form CActiveForm */
?>

<?php $this->pageTitle=Yii::app()->name . ' - Manage Kost'; ?>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="page-title">
	<div class="title"><h3>Manage Kost</h3></div>
</div>

<table class="table">
	<tr>
		<th style="width:40%">Nama Kost</th>
		<th style="width:40%">Area</th>
		<th style="width:20%">Action</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<tr>
				<td><?php echo $model->nama_kost;?></td>
				<td><?php echo $model->area; ?></td>
				<td>
					<span title="Edit"><a href="<?php echo Yii::app()->createUrl("operator/kost/update/id/".$model->id_kost); ?>"><i class="fa fa-edit"></i></a></span>
					<!--<span title="Delete"><a href="<?php //echo Yii::app()->createUrl("operator/kost/remove/id/".$model->id_kost); ?>"><i class="fa fa-times"></i></a></span>-->
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>