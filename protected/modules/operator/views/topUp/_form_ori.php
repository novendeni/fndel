<?php
/* @var $this TopUpController */
/* @var $model TopUp */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'top-up-form',
	'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib diisi!</p> <br/>

    <?php //echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'nim',array('span'=>3,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'jumlah',array('span'=>3)); ?>

            <?php //echo $form->textFieldControlGroup($model,'tanggal',array('span'=>3)); ?>

            <?php //echo $form->textFieldControlGroup($model,'update_by',array('span'=>3,'maxlength'=>40)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Top Up',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->