<?php
/* @var $this TopUpController */
/* @var $model TopUp */
?>

<?php
$this->breadcrumbs=array(
	'Top Ups'=>array('index'),
	$model->id_top_up=>array('view','id'=>$model->id_top_up),
	'Update',
);

$this->menu=array(
	array('label'=>'List TopUp', 'url'=>array('index')),
	array('label'=>'Create TopUp', 'url'=>array('create')),
	array('label'=>'View TopUp', 'url'=>array('view', 'id'=>$model->id_top_up)),
	array('label'=>'Manage TopUp', 'url'=>array('admin')),
);
?>

    <h1>Update TopUp <?php echo $model->id_top_up; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>