<?php
/* @var $this TopUpController */
/* @var $model TopUp */
/* @var $form TbActiveForm */
?>

<div class="row">
	<div class="col-md-6 col-sm-6">
		<div class="form">
			<?php 
				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'top-up-form',
					)); 
			?>

				<div class="form-group">							
					<?php echo $form->labelEx($model,'nim', array('label'=>'NIM')); ?>
					<?php echo $form->textField($model,'nim', array('class'=>'form-control','placeholder'=>'NIM')); ?>
					<?php echo $form->error($model,'nim', array('class'=>'alert-text')); ?>
				</div>
				<div class="form-group">
					<?php echo $form->labelEx($model,'jumlah', array('label'=>'Jumlah')); ?>
					<?php echo $form->textField($model,'jumlah', array('class'=>'form-control','placeholder'=>'Jumlah')); ?>
					<?php echo $form->error($model,'jumlah', array('class'=>'alert-text')); ?>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
				
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>