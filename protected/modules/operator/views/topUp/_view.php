<?php
/* @var $this TopUpController */
/* @var $data TopUp */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id_top_up')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_top_up),array('view','id'=>$data->id_top_up)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nim')); ?>:</b>
	<?php echo CHtml::encode($data->nim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_by')); ?>:</b>
	<?php echo CHtml::encode($data->update_by); ?>
	<br />


</div>