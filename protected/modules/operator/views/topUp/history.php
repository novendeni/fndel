<?php $this->layout='/layouts/column-sidebar'; ?>
<?php $this->pageTitle=Yii::app()->name . ' - History Top Up'; ?>
<?php 
	$this->breadcrumbs=array(
		ucfirst(Yii::app()->user->nama)=> Yii::app()->createUrl('users'),
		'History Top Up',
	);
?>


<div class="page-title">
	<div class="title"><h3>History Top Up</h3></div>
</div>

<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'search-form',
	)); ?>
	<div class="form-group form-inline">
		<?php echo $form->label($model,'nim',array('label'=>'NIM'));?>
		<?php echo '&nbsp;';?>
		<?php echo $form->textField($model,'nim', array('class'=>'form-control','placeholder'=>'NIM')); ?>
			
		<button type="submit" class="btn btn-primary">Cari</button>
	</div>
	<?php $this->endWidget();?>
</div>

<div class=''>
	<table class="table">
		<tr>
			<th width="20px">No</th>
			<th>Tanggal</th>
			<th>Jumlah</th>
		</tr>
		<?php $total = 0;?>
		<?php foreach($models as $i=>$item):?>
		<tr>
			<td><?php echo $i+1; ?></td>
			<td><?php echo date('d-m-Y', strtotime($item->tanggal)); ?></td>
			<td><?php echo "$item->jumlah"; ?></td>
			<?php $total = $total + $item->jumlah;?>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colSpan="2">Total</td>
			<td><?php echo $total;?></td>
		</tr>
	</table>
</div>