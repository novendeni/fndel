<div class="page-title">
	<div class="title"><h3>Transaksi</h3></div>
</div>

<div class=''>
	<table class="table">
		<tr>
			<th width="20px">No</th>
			<th style="width:10%">Trans Id</th>
			<th>NIM</th>
			<th>Jam Pesan</th>
			<th>Total</th>
			<th>Status</th>
		</tr>
		<?php foreach($models as $i=>$model):?>
		<tr>
			<td><?php echo $i+1; ?></td>
			<td><a href="<?php echo Yii::app()->createUrl('operator/transaksiDetail/view',array('id'=>$model->id_transaksi));?>"><?php echo $model->id_transaksi;?></a></td>
			<td><a href="<?php echo Yii::app()->createUrl('operator/customer/detail',array('id'=>$model->nim));?>"><?php echo $model->nim;?></a></td>
			<td><?php echo $model->jam_pesan; ?></td>
			<td><?php echo "$model->total"; ?></td>
			<td>
				<?php 
					if($model->status === '0')
						echo "In Progress"; 
					if($model->status === '1')
						echo "Close"; 
					if($model->status === '2')
						echo "Rejected"; 
				?>
			</td>
		</tr>
		<?php endforeach;?>
	</table>
</div>