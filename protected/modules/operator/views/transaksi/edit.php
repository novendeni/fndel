<?php
/* @var $this TransaksiController */
/* @var $model Transaksi */
/* @var $form CActiveForm */
?>

<?php $this->pageTitle=Yii::app()->name . ' - Edit Transaksi'; ?>

<div class="page-title">
	<div class="title"><h3>Edit Transaksi</h3></div>
</div>

<?php echo CHtml::beginForm(Yii::app()->createUrl('/operator/transaksi/edit'),'POST');?>
<table class="table">
	<tr>
		<td>NIM</td>
		<td><a href="<?php echo Yii::app()->createUrl('operator/transaksiDetail/view',array('id'=>$model->id_transaksi));?>"><?php echo $model->nim;?></a></td>
	</tr>
	<tr>
		<td>Nama</td>
		<td><?php echo $model->nim0->nama;?></td>
	</tr>
	<tr>
		<td>Tanggal</td>
		<td><?php echo $model->tanggal; ?></td>
	</tr>
	<tr>
		<td>Total</td>
		<td><?php echo $model->total; ?></td>
	</tr>
	<tr>
		<td>Nilai transaksi Baru</td>
		<td><?php echo CHtml::textField('total','',array('class'=>'form-control','placeholder'=>'Total'));?></td>
	</tr>
	<tr>
		<td colspan=2><?php echo CHtml::submitButton('Simpan',array('id'=>'btn-ok','class'=>'btn btn-primary')); ?></td>
	</tr>
</table>
<?php echo CHtml::endForm();?>