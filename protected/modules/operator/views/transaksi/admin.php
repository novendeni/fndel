<?php
/* @var $this TransaksiController */
/* @var $model Transaksi */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#transaksi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Transaksi</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'transaksi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id_transaksi',
		//'id_kurir',
		array(
			'name'=>'idKurir',
			'type'=>'raw',
			'header'=>'Kurir',
			'value'=>'$data->idKurir->nama_kurir',
		),
		'nim',
		'tanggal',
		'total',
		'nominal_pembayaran',
		'jam_pesan',
		'jam_terkirim',
		'status',
		//'rating',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}',
		),
	),
)); ?>