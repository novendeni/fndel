<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript('setLink',
	"$.each($(\".dropdown\"), function(index, obj){
		$(obj).change(function(){
			var kurir = ($(this).val());
			var link = $(\"a.link-id:eq(\"+index+\")\").attr('href');
			var newLink = link+'/'+kurir;
			
			$(\"a.link-id:eq(\"+index+\")\").attr('href',newLink);
		});
	});",
CClientScript::POS_READY);
?>

<?php $this->pageTitle=Yii::app()->name . ' - Set Kurir'; ?>

<div class="page-title">
	<div class="title"><h3>Set Kurir</h3></div>
</div>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<table class="table">
	<tr>
		<th style="width:10%">Nomor</th>
		<th>NIM</th>
		<th>Tanggal</th>
		<th>Total</th>
		<th style="width:25%">Action</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<tr>
				<td><a class="trans-id" href="<?php echo Yii::app()->createUrl('operator/transaksiDetail/view',array('id'=>$model->id_transaksi));?>"><?php echo $model->id_transaksi;?></a></td>
				<td><a href="<?php echo Yii::app()->createUrl('operator/customer/detail',array('id'=>$model->nim));?>"><?php echo $model->nim;?></a></td>
				<td><?php echo $model->tanggal; ?></td>
				<td><?php echo $model->total; ?></td>
				<td>
					<?php $kurir = CHtml::listData(Kurir::model()->findAll(),'id_kurir','nama'); ?>
					<?php echo TbHtml::dropDownList('kurir','id_kurir',$kurir,array('empty'=>'-Pilih Kurir-','class'=>'dropdown')); ?>
					<?php echo ' -- ';?>
					<a class="link-id" href="<?php echo Yii::app()->createUrl('/operator/transaksi/setKurir',array('trans'=>$model->id_transaksi,'kurir'=>''));?>"><i class="fa fa-check"></i></a>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>