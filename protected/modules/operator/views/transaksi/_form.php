<?php
/* @var $this TransaksiController */
/* @var $model Transaksi */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'transaksi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'id_kurir',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'nim',array('span'=>5,'maxlength'=>40)); ?>

            <?php echo $form->textFieldControlGroup($model,'tanggal',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'total',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'nominal_pembayaran',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'jam_pesan',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'jam_terkirim',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'rating',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->