<div class="page-title">
	<div class="title"><h3>Transaksi Report</h3></div>
</div>

<div class="form">
	<?php echo CHtml::beginForm(Yii::app()->createUrl('/operator/transaksi/report'),'POST'); ?>
	<div class="form-group form-inline">
		<div class="input-append date date-picker inline">
			<?php echo CHtml::label('Tanggal Awal','');?><?php echo '&nbsp;';?>
			<?php echo CHtml::textField('tgl_awal','', array('class'=>'form-control','placeholder'=>'dd-mm-yyyy', 'data-date'=>'12-02-2012', 'data-date-format'=>'dd-mm-yyyy', 'data-date-viewmode'=>'years')); ?>
		</div>
	</div>
	<div class="form-group form-inline">
		<div class="input-append date date-picker inline">
			<?php echo CHtml::label('Tanggal Ahir','');?><?php echo '&nbsp;&nbsp;';?>
			<?php echo CHtml::textField('tgl_akhir','', array('class'=>'form-control','placeholder'=>'dd-mm-yyyy', 'data-date'=>'12-02-2012', 'data-date-format'=>'dd-mm-yyyy', 'data-date-viewmode'=>'years')); ?>
		</div>
	</div>
	<?php echo CHtml::submitButton('Find',array('id'=>'btn-ok','class'=>'btn btn-primary')); ?>
	<?php echo CHtml::endForm(); ?>
</div>

<br/>

<table class="table">
	<tr>
		<th style="width:10%">Id</th>
		<th>Nama</th>
		<th>Tanggal</th>
		<th>Jam Pesan</th>
		<th>Jumlah</th>
		<th>Status</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $item):?>
			<?php 
				if($item->status === '0'){
					$stat = 'In Progress';
					$color = '';
				}
				if($item->status === '1'){
					$stat = 'Success';
					$color = 'success';
				}
				if($item->status === '2'){
					$stat = 'Rejected';
					$color = 'danger';
				}
			?>
			<tr class="<?php echo $color?>">
				<td><a href="<?php echo Yii::app()->createUrl('operator/transaksiDetail/view',array('id'=>$item->id_transaksi));?>"><?php echo $item->id_transaksi;?></a></td>
				<td><a href="<?php echo Yii::app()->createUrl('operator/customer/detail',array('id'=>$item->nim));?>"><?php echo $item->nim0->nama;?></td>
				<td><?php echo date('d-m-Y',strtotime($item->tanggal));?></td>
				<td><?php echo $item->jam_pesan; ?></td>
				<td class="currency"><?php echo $item->total; ?></td>
				<td><?php echo $stat; ?></td>
			</tr>
		<?php endforeach;?>
	<?php endif; ?>
</table>	