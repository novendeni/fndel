<?php
/* @var $this MenuController */
/* @var $model Menu */
/* @var $form CActiveForm */
?>

<?php $this->pageTitle=Yii::app()->name . ' - Manage Transaksi'; ?>

<div class="page-title">
	<div class="title"><h3>Manage Transaksi</h3></div>
</div>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<table class="table">
	<tr>
		<th style="width:10%">Nomor</th>
		<th style="width:20%">NIM</th>
		<th>Nama</th>
		<th style="width:20%">Tanggal</th>
		<th style="width:20%">Total</th>
		<th style="width:10%">Action</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<tr>
				<td><a href="<?php echo Yii::app()->createUrl('operator/transaksiDetail/view',array('id'=>$model->id_transaksi));?>"><?php echo $model->id_transaksi;?></a></td>
				<td><a href="<?php echo Yii::app()->createUrl('operator/customer/detail',array('id'=>$model->nim));?>"><?php echo $model->nim;?></a></td>
				<td><?php echo $model->nim0->nama;?></td>
				<td><?php echo $model->tanggal; ?></td>
				<td><?php echo $model->total; ?></td>
				<td>
					<span title="Close Transaksi"><a href="<?php echo Yii::app()->createUrl("operator/transaksi/close/id/".$model->id_transaksi); ?>"><i class="fa fa-check"></i></a></span>
					<span title="Reject Transaksi"><a href="<?php echo Yii::app()->createUrl("operator/transaksi/reject/id/".$model->id_transaksi); ?>"><i class="fa fa-times"></i></a></span>
					<span title="Edit Transaksi"><a href="<?php echo Yii::app()->createUrl("operator/transaksi/edit/id/".$model->id_transaksi); ?>"><i class="fa fa-pencil"></i></a></span>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>