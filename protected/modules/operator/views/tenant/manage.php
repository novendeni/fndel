<?php
/* @var $this TenantController */
/* @var $model Tenant */
/* @var $form CActiveForm */
?>

<?php $this->pageTitle=Yii::app()->name . ' - Manage Tenant'; ?>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="page-title">
	<div class="title"><h3>Manage Tenant</h3></div>
</div>

<table class="table">
	<tr>
		<th style="width:20%">Nama Warung</th>
		<th style="width:20%">Nama Pemilik</th>
		<th>No HP</th>
		<th>Alamat</th>
		<th>Jam Operasional</th>
		<th style="width:10%">Action</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<tr>
				<td><?php echo $model->nama_warung;?></td>
				<td><?php echo $model->nama_pemilik; ?></td>
				<td><?php echo $model->no_hp; ?></td>
				<td><?php echo $model->alamat;?></td>
				<td><?php echo date('H:i',strtotime($model->jam_buka)).' s/d '.date('H:i',strtotime($model->jam_tutup))?></td>
				<td>
					<span title="Edit"><a href="<?php echo Yii::app()->createUrl("operator/tenant/update/id/".$model->id_tenant); ?>"><i class="fa fa-edit"></i></a></span>
					<span title="Delete"><a href="<?php echo Yii::app()->createUrl("operator/tenant/remove/id/".$model->id_tenant); ?>"><i class="fa fa-times"></i></a></span>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>