<?php
/* @var $this TenantController */
/* @var $model Tenant */
?>

<div class="page-title">
	<div class="title"><h3>Pendaftaran Tenant</h3></div>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>