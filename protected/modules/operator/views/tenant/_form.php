<?php
/* @var $this TenantController */
/* @var $model Tenant */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'tenant-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
        'class'=>'form-horizontal',
    ),
)); ?>
	
	<div class="form-group">
		<?php echo $form->label($model,'nama_warung',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php echo $form->textField($model,'nama_warung',array('class'=>'form-control','placeholder'=>'Nama Warung'));?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->label($model,'nama_pemilik',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php echo $form->textField($model,'nama_pemilik',array('class'=>'form-control','placeholder'=>'Nama Pemilik'));?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->label($model,'no_hp',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php echo $form->textField($model,'no_hp',array('class'=>'form-control','placeholder'=>'No Hp'));?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->label($model,'alamat',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php echo $form->textField($model,'alamat',array('class'=>'form-control','placeholder'=>'Alamat'));?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'area',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php 
				echo $form->dropDownList($model,'area',array(
					'belakang lapangan tenis'=>'Belakang Lapangan Tenis',
					'jalan sukabirus'=>'Jalan Sukabirus',
					'gang umayah'=>'Gang Umayah',
					'sukapura'=>'Sukapura',
					'perumahan permata buah batu'=>'Perumahan Permata Buah Batu',
					'perumahan pesona bali'=>'Perumahan Pesona Bali',
					'asrama putra'=>' Asrama Putra',
					'asrama putri'=>'Asrama Putri',
					'pga'=>'PGA',
				), array('empty'=>'-- Pilih Area --', 'class'=>'form-control')); 
			?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->label($model,'jam_buka',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php echo $form->textField($model,'jam_buka',array('class'=>'form-control'));?>
		</div>
	</div>
	
	<div class="form-group">
		<?php echo $form->label($model,'jam_tutup',array('class'=>'col-sm-3 control-label','style'=>'text-align:left;'));?>
		<div class="col-sm-6">
			<?php echo $form->textField($model,'jam_tutup',array('class'=>'form-control'));?>
		</div>
	</div>
	
	<button type="submit" class="btn btn-primary">Simpan</button>

    <?php $this->endWidget(); ?>

</div><!-- form -->