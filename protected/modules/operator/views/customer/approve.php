<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form CActiveForm */
?>

<?php $this->pageTitle=Yii::app()->name . ' - Approve User'; ?>

<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span>
		</button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<div class="page-title">
	<div class="title"><h3>Approve User</h3></div>
</div>

<table class="table">
	<tr>
		<th style="width:15%">NIM</th>
		<th style="width:25%">Nama</th>
		<th>No HP</th>
		<th>Alamat</th>
		<th>Tgl Register</th>
		<th style="width:10%">Action</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<tr>
				<td><?php echo $model->nim;?></td>
				<td><?php echo $model->nama; ?></td>
				<td><?php echo $model->no_hp; ?></td>
				<td><?php echo $model->nama_kos.' / '.$model->no_kamar; ?></td>
				<td><?php echo date('d-m-Y',strtotime($model->tgl_register));?></td>
				<td>
					<span title="Approve"><a href="<?php echo Yii::app()->createUrl("operator/customer/approve/id/".$model->nim); ?>"><i class="fa fa-check"></i></a></span>
					<span title="Reject"><a href="<?php echo Yii::app()->createUrl("operator/customer/remove/id/".$model->nim); ?>"><i class="fa fa-times"></i></a></span>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>

<div id="approve-modal" class="modal fade in">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4>Pilih Jumlah Pesanan</h4>
			</div>
			<div class="modal-body">
				<?php echo $message;?>
			</div>
			<div class="modal-footer">
				<input type="button" class="btn btn-primary modal-order" name="submit_id" id="btn_id" value="Pesan" data-dismiss="modal"/>
				<!--<input type="submit" value="pesan" data-dismiss="modal">-->
			</div>
		</div>
	</div>
</div>