<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form CActiveForm */
?>

<?php $this->pageTitle=Yii::app()->name . ' - Manage User'; ?>

<div class="page-title">
	<div class="title"><h3>Manage User</h3></div>
</div>

<table class="table">
	<tr>
		<th style="width:20%">NIM</th>
		<th style="width:30%">Nama</th>
		<th>No HP</td>
		<th>Nama Kos</th>
		<th style="width:10%">Action</th>
	</tr>
	<?php if($models != null): ?>
		<?php foreach($models as $model): ?>
			<tr>
				<td><?php echo $model->nim;?></td>
				<td><?php echo $model->nama; ?></td>
				<td><?php echo $model->no_hp; ?></td>
				<td><?php echo $model->nama_kos.' / '.$model->no_kamar; ?></td>
				<td>
					<span title="Edit"><a href="<?php echo Yii::app()->createUrl("operator/customer/update/id/".$model->nim); ?>"><i class="fa fa-edit"></i></a></span>
					<span title="Block"><a href="<?php echo Yii::app()->createUrl("operator/customer/block/id/".$model->nim); ?>"><i class="fa fa-minus-circle"></i></a></span>
					<span title="Unblock"><a href="<?php echo Yii::app()->createUrl("operator/customer/unblock/id/".$model->nim); ?>"><i class="fa fa-check-circle"></i></a></span>
				</td>
			</tr>
		<?php endforeach; ?>
	<?php endif; ?>
</table>

<div class='row'>
<?php 
	$this->widget('CLinkPager', array(
		'pages' => $pages,
		'header'=>'',
		'nextPageLabel'=>'&raquo;',
		'prevPageLabel'=>'&laquo;',
		'selectedPageCssClass'=>'active',
		'hiddenPageCssClass'=>'disabled',
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		)
	) 
?>
</div>