<?php
	// $this Customer controller
?>

<div class="page-title">
	<div class="title"><h3>Detail Customer</h3></div>
</div>

<div class=''>
	<table class="table table-striped">
		<tr>
			<td>Nim</td>
			<td><?php echo $model->nim;?></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td><?php echo $model->nama;?></td>
		</tr>
		<tr>
			<td>Alamat</td>
			<td><?php echo ucwords($model->nama_kos).' / '.$model->no_kamar;?></td>
		</tr>
		<tr>
			<td>No HP</td>
			<td><?php echo $model->no_hp;?></td>
		</tr>
	</table>
	<br/>
	
	<a href="<?php echo Yii::app()->request->urlReferrer;?>" class="btn btn-1 pesan">Back</a>
</div>