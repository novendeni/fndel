<?php

/**
 * This is the model class for table "tbl_kapasitas_harian".
 *
 * The followings are the available columns in table 'tbl_kapasitas_harian':
 * @property integer $id_menu
 * @property string $tanggal
 * @property integer $jml_terbeli
 * @property integer $jml_stock
 *
 * The followings are the available model relations:
 * @property Menu $idMenu
 */
class KapasitasHarian extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_kapasitas_harian';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_menu, tanggal, jml_terbeli, jml_stock', 'required'),
			array('id_menu, jml_terbeli, jml_stock', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_menu, tanggal, jml_terbeli, jml_stock', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMenu' => array(self::BELONGS_TO, 'Menu', 'id_menu'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_menu' => 'Id Menu',
			'tanggal' => 'Tanggal',
			'jml_terbeli' => 'Jml Terbeli',
			'jml_stock' => 'Jml Stock',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('jml_terbeli',$this->jml_terbeli);
		$criteria->compare('jml_stock',$this->jml_stock);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array('defaultOrder'=>'tanggal DESC'),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KapasitasHarian the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* overwrite primary key method
	*
	*/
	public function primaryKey(){
		return array('id_menu', 'tanggal');
	}
}
