<?php

/**
 * This is the model class for table "tbl_transaksi_detail".
 *
 * The followings are the available columns in table 'tbl_transaksi_detail':
 * @property integer $id_transaksi_detail
 * @property integer $id_transaksi
 * @property integer $id_menu
 * @property integer $jumlah
 * @property integer $subtotal
 * @property string $catatan
 * @property string $testimonial
 * @property integer $rating
 *
 * The followings are the available model relations:
 * @property Transaksi $idTransaksi
 * @property Menu $idMenu
 */
class TransaksiDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_transaksi_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_transaksi, id_menu, jumlah, subtotal, rating', 'required'),
			array('id_transaksi, id_menu, jumlah, subtotal, rating', 'numerical', 'integerOnly'=>true),
			array('catatan, testimonial', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_transaksi_detail, id_transaksi, id_menu, jumlah, subtotal, catatan, testimonial, rating', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTransaksi' => array(self::BELONGS_TO, 'Transaksi', 'id_transaksi'),
			'idMenu' => array(self::BELONGS_TO, 'Menu', 'id_menu'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_transaksi_detail' => 'Id Transaksi Detail',
			'id_transaksi' => 'Id Transaksi',
			'id_menu' => 'Id Menu',
			'jumlah' => 'Jumlah',
			'subtotal' => 'Subtotal',
			'catatan' => 'Catatan',
			'testimonial' => 'Testimonial',
			'rating' => 'Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_transaksi_detail',$this->id_transaksi_detail);
		$criteria->compare('id_transaksi',$this->id_transaksi);
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('subtotal',$this->subtotal);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('testimonial',$this->testimonial,true);
		$criteria->compare('rating',$this->rating);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TransaksiDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
