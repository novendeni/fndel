<?php

/**
 * This is the model class for table "tbl_top_up".
 *
 * The followings are the available columns in table 'tbl_top_up':
 * @property integer $id_top_up
 * @property string $nim
 * @property integer $jumlah
 * @property string $tanggal
 * @property string $update_by
 *
 * The followings are the available model relations:
 * @property Customer $nim0
 */
class TopUp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_top_up';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nim, jumlah, tanggal, update_by', 'required'),
			array('jumlah', 'numerical', 'integerOnly'=>true),
			array('nim, update_by', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_top_up, nim, jumlah, tanggal, update_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'nim0' => array(self::BELONGS_TO, 'Customer', 'nim'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_top_up' => 'Id Top Up',
			'nim' => 'Nim',
			'jumlah' => 'Jumlah',
			'tanggal' => 'Tanggal',
			'update_by' => 'Update By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_top_up',$this->id_top_up);
		$criteria->compare('nim',$this->nim,true);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('update_by',$this->update_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TopUp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
