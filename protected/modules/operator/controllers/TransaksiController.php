<?php

class TransaksiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column-sidebar';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','close','setKurir','report','progress','reject','manage','edit'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/**
	* menampilkan transaksi pada hari ini
	*/
	public function actionProgress(){
		
		$models = Transaksi::model()->findAllByattributes(array('tanggal'=>date('Y-m-d', strtotime(date('Y-m-d H:i:s')) + (7 * 60 * 60))));
		
		$this->render('progress',array('models'=>$models));
	}
	
	/**
	* menampilkan transaksi sesuai dengan range waktu yg di inputkan
	*/
	public function actionReport(){
		$awal= date('Y-m-d', strtotime(date('Y-m-d H:i:s')) + (7 * 60 * 60));
		$akhir = date('Y-m-d', strtotime(date('Y-m-d H:i:s')) + (7 * 60 * 60));
		
		if(isset($_POST['tgl_awal'])){
			$awal = date('Y-m-d',strtotime($_POST['tgl_awal']));
			$akhir = date('Y-m-d',strtotime($_POST['tgl_akhir']));
		}
		
		$criteria = new CDbCriteria;
		$criteria->addBetweenCondition('tanggal',$awal,$akhir);
		
		$models = Transaksi::model()->findAll($criteria);
		
		$this->render('report',array('models'=>$models));
	}
	
	/**
	* mengatur kurir yang melakukan transaksi
	*/
	public function actionsetKurir(){
		if(isset($_GET['trans'])){
			$trans = Transaksi::model()->findByPK($_GET['trans']);
			
			$trans->id_kurir = $_GET['kurir'];
			$trans->save();
			Yii::app()->user->setFlash('success',"Set kurir berhasil.");
		}
		
		$criteria = new CDbCriteria;
		$criteria->addSearchCondition('id_kurir','0');
		$criteria->order = 'tanggal DESC';
		
		//pagination
		$count = Transaksi::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Transaksi::model()->findAll($criteria);
		
		$this->render('setKurir',array('models'=>$models, 'pages'=>$pages));
	
	}
	
	/**
	* menampilkan semua transaksi serta aksi2 yg mungkin dilakukan
	*/
	public function actionManage(){
		$criteria = new CDbCriteria;
		//$criteria->addSearchCondition('status','0');
		$criteria->order = 'tanggal DESC';
		
		//pagination
		$count = Transaksi::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Transaksi::model()->findAll($criteria);
		
		$this->render('manage',array('models'=>$models, 'pages'=>$pages));
	}
	
	/**
	 * Ubah status transaksi menjadi Close(telah selesai) dan mengirimkan email pnotif ke user dan operator
	 */
	public function actionClose()
	{
		if(isset($_GET['id'])){
			$close = $this->loadModel($_GET['id']);
			
			$close->status = 1;
			$close->jam_terkirim = date('H:i:s', time() + (7 * 60 * 60));
			$close->save();
			
			// send email to tanyafndel@gmail.com
			$to = 'tanyafndel@gmail.com';
			$from = 'admin@fndel.com';
			$subject = 'Pesanan No '.$close->id_transaksi.' selesai';
			$message = 'Dear Admin,
						<br/>Pesanan dengan no id '.$close->id_transaksi.' telah selesai.
						<br/><br/>Terimakasih. :)';
			$this->mailsend($to, $from, $subject, $message);
			
			// send email to user
			$to = $close->nim0->email;
			$from = 'admin@fndel.com';
			$subject = 'Pesanan No '.$close->id_transaksi.' selesai';
			$message = 'Dear '.$close->nim0->nama.',
						<br/> Pesanan kakak dengan rincian : 
						<br/> No Transaksi : '.$close->id_transaksi.'
						<br/> Total : '.$close->total.'
						<br/> Pesanan : '.$pesanan.'
						<br/> Alamat : '.$close->nim0->nama_kos.' / '.$close->nim0->no_kamar.'
						<br/> Telah kami selesaikan.
						<br/><br/>Terimakasih atas kepercayaan kakak kepada kami. :)
						<br/><br>
						Salam Hangat,
						<br/><br/>CS fndel.com
						<br/>=============================================
						<br/>www.fndel.com
						<br/>Office : Gang Masjid Sukabirus(Samping Masjid Istiqomah)
						<br/>Phone : 0857-9413-6415
						<br/>Line : fndel
						<br/>Email : tanyafndel@gmail.com';
			$this->mailsend($to, $from, $subject, $message);
					
			Yii::app()->user->setFlash('success', "Transaksi telah di Close.");
			$this->redirect(array('/operator/transaksi/manage'));
		}
	}
	
	/**
	* batalkan transaksi sesuai id kemudian saldo yg terpakai di kembalikan
	*/
	public function actionReject(){
		
		$model=$this->loadModel($_GET['id']);
		$user = Customer::model()->findByPK($model->nim);
		
		$model->status = 2;
		$model->update();
		
		$user->sisa_saldo = $user->sisa_saldo + $model->total;
		$user->update();
		
		Yii::app()->user->setFlash('success','Transaksi telah dibatalkan');
		$this->redirect(array('/operator/transaksi/manage'));
	}
	
	/**
	* edit transaksi
	*/
	public function actionEdit(){
		$model = $this->loadModel($_GET['id']);
		$total = $model->total;
		
		if(isset($_POST['total'])){
			$model->total = $_POST['total'];
			if($total < $model->total)
				$model->nim0->sisa_saldo = $model->nim0->sisa_saldo - ($model->total - $total);
			
			if ($total > $model->total)
				$model->nim0->sisa_saldo = $model->nim0->sisa_saldo + ($model->total - $total);
			
			Yii::app()->user->setFlash('success','Transaksi berhasil di edit');
			$this->redirect(array('/operator/transaksi/manage'));
		}
		
		$this->render('edit',array('model'=>$model));
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Transaksi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Transaksi'])) {
			$model->attributes=$_POST['Transaksi'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id_transaksi));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Transaksi'])) {
			$model->attributes=$_POST['Transaksi'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id_transaksi));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Transaksi');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Transaksi('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Transaksi'])) {
			$model->attributes=$_GET['Transaksi'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Transaksi the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Transaksi::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Transaksi $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='transaksi-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}