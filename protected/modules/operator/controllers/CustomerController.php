<?php

class CustomerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column-sidebar';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update','saldo','approve','remove','manage','history','detail'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/**
	*
	*/
	public function actionManage(){
		$models = array();
		
		$criteria = new CDbCriteria;
		$criteria->addSearchCondition('approve','1');
		
		//pagination
		$count = Customer::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Customer::model()->findAll($criteria);
		
		$this->render('manage',array('models'=>$models, 'pages'=>$pages));
	}
	
	/**
	*
	*/
	public function actionHistory(){
		$models = array();
		$model = new Customer;
		$criteria = new CDbCriteria;
		
		if($_POST['Customer']){
			$model->attributes = $_POST['Customer'];
		
			$criteria->addSearchCondition('nim',$model->nim);
			
			//pagination
			$count = Transaksi::model()->count($criteria);
			$pages = new CPagination($count);
			// results per page
			$pages->pageSize=25;
			$pages->applyLimit($criteria);
			
			$models = Transaksi::model()->findAll($criteria);
		}
		
		$this->render('history',array('model'=>$model, 'models'=>$models, 'pages'=>$pages));
	}
	
	/**
	*
	*/
	public function actionRemove(){
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			
			$this->loadModel($id)->delete();
		Yii::app()->user->setFlash('success',"User telah dihapus.");
		$this->redirect(array('approve'));
		}
	}
	
	/**
	*
	*/
	public function actionApprove(){
		$message = '';
		if(isset($_GET['id'])){
			$id = $_GET['id'];
			
			$model = Customer::model()->findByPK($id);
			$model->approve = 1;
			$model->save(false);
			
			// send email to tanyafndel@gmail.com
			$to = 'tanyafndel@gmail.com';
			$from = 'admin@fndel.com';
			$subject = 'Approval account '.$model->nim.' berhasil';
			$message = 'Dear Admin, 
						<br/> Approval nim '.$model->nim.' telah berhasil.
						<br/> Terimakasih. :)';
			$this->mailsend($to, $from, $subject, $message);
			
			// send email to user
			$to = $model->email;
			$from = 'admin@fndel.com';
			$subject = 'Approval account '.$model->nim.' berhasil';
			$message = 'Dear '.$model->nama.',
						<br/>Account kakak dengan nim '.$model->nim.' telah dapat dipergunakan.
						<br/>Selamat bertransaksi :)
						<br/><br>
						Salam Hangat,
						<br/><br/>CS fndel.com
						<br/>=============================================
						<br/>www.fndel.com
						<br/>Office : Gang Masjid Sukabirus(Samping Masjid Istiqomah)
						<br/>Phone : 0857-9413-6415
						<br/>Line : fndel
						<br/>Email : tanyafndel@gmail.com';
			$this->mailsend($to, $from, $subject, $message);
			
			Yii::app()->user->setFlash('success',"User berhasil disetuji.");
		}
		
		$criteria = new CDbCriteria;
		$criteria->addSearchCondition('approve','0');
		
		//pagination
		$count = Customer::model()->count($criteria);
		$pages = new CPagination($count);
		// results per page
		$pages->pageSize=25;
		$pages->applyLimit($criteria);
		
		$models = Customer::model()->findAll($criteria);
		
		$this->render('approve',array('message'=>$message,'models'=>$models, 'pages'=>$pages));
	}
	
	/**
	*
	*/
	public function actionBlock(){
		$id = Yii::app()->user->nim;
		
		$model=$this->loadModel($id);
		$model->block = '1';
		$model->save(false);
		
		Yii::app()->setFlash('success','User berhasil di Block');
		$this->redirect(array('manage'));
	}
	
	/**
	*
	*/
	public function actionUnblock(){
		$id = Yii::app()->user->nim;
		
		$model=$this->loadModel($id);
		$model->block = '0';
		$model->save(false);
		
		Yii::app()->setFlash('success','User berhasil di Unblock');
		$this->redirect(array('manage'));
	}
	
	/**
	*
	*/
	public function actionDetail(){
		$model = $this->loadModel($_GET['id']);
		
		$this->render('detail',array('model'=>$model));
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$id = Yii::app()->user->nim;
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Customer;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Customer'])) {
			$model->attributes=$_POST['Customer'];
			$model->sisa_saldo = 0;
			$model->password = $model->hashPassword($model->password);
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->nim));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		
		$model=$this->loadModel($_GET['id']);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Customer'])) {
			$model->attributes=$_POST['Customer'];
			$oke = $model->validate();
			if($oke){
				$model->password = $model->hashPassword($model->password);
				$model->save(false);
				
				$this->redirect(array('manage'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Customer');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Customer('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Customer'])) {
			$model->attributes=$_GET['Customer'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Customer the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Customer::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Customer $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='customer-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function validatePassword($password){
		return CPasswordHelper::verifyPassword($password,$this->password);
	}
	
	public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }
	
	/******/
	public function actionSaldo(){
		$id = Yii::app()->user->nim;
		$model=$this->loadModel($id);
		
		$this->render('saldo',array('saldo'=>$model->sisa_saldo));
	}
}