<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->layout ='/layouts/column-sidebar';
		$this->render('index');
	}
}