<?php

class KapasitasHarianController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '/layouts/column2', meaning
	 * using two-column layout. See 'modules/operator/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column-sidebar';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','update1','delete','manage','admin'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	*
	*
	*/
	public function actionManage(){
		$model = new KapasitasHarian;
		if(isset($_POST['KapasitasHarian'])){
			$model->attributes = $_POST['KapasitasHarian'];
		}
		if(isset($_GET['tgl'])){
			$model->tanggal = $_GET['tgl'];
		}
		$tanggal = date('Y-m-d',strtotime($model->tanggal));
		
		$models = KapasitasHarian::model()->findAllByAttributes(array('tanggal'=>$tanggal));
		
		$this->render('manage',array('model'=>$model, 'models'=>$models));
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id, $id1)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id, $id1),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Tenant;
		//$model->unsetAttributes();
		
		$models = array();
		if(isset($_POST['Tenant'])){ 
			$model->id_tenant=$_POST['Tenant']['id_tenant'];
			$menus = Menu::model()->findAllByAttributes(array('id_tenant'=>$_POST['Tenant']['id_tenant']), array('order'=>'id_menu')); //get the menu list
			
			foreach($menus as $i=>$menu){
				$exist = KapasitasHarian::model()->findByAttributes(array('id_menu'=>$menu->id_menu, 'tanggal'=>date('Y-m-d', strtotime(date('Y-m-d H:i:s')) + (7 * 60 * 60))));
				if ($exist === null){
					$kapHar = new KapasitasHarian;
					$kapHar->id_menu = $menu->id_menu;
					$models[] = $kapHar;
				}
			}
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if (isset($_POST['KapasitasHarian'])) {	
			$models = $_POST['KapasitasHarian'];
			foreach($models as $item){
				$temp = new KapasitasHarian;
				$temp->attributes = $item;
				$temp->tanggal = date('Y-m-d', strtotime(date('Y-m-d H:i:s')) + (7 * 60 * 60));
				$temp->jml_terbeli = 0;
				
				$temp->save();
			}
			Yii::app()->user->setFlash('success',"Data berhasil di inputkan.");
			$this->redirect(array('manage'));
		}

		$this->render('create',array(
			'model'=>$model, 'models'=>$models,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id, $id1)
	{
		$model=$this->loadModel($id, $id1);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['KapasitasHarian'])) {
			$model->attributes=$_POST['KapasitasHarian'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id_menu, 'id1'=>$model->tanggal));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate1()
	{
		if(isset($_GET['id']))
			$model = $this->loadModel($_GET['id'], $_GET['tanggal']);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['KapasitasHarian'])) {
			$model->attributes=$_POST['KapasitasHarian'];
			if ($model->save()) {
				//$this->redirect(array('view','id'=>$model->id_menu, 'id1'=>$model->tanggal));
				//$this->redirect();
				$this->redirect(array('manage','tgl'=>$model->tanggal));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id, $id1)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id, $id1)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KapasitasHarian');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KapasitasHarian('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['KapasitasHarian'])) {
			$model->attributes=$_GET['KapasitasHarian'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return KapasitasHarian the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id, $id1)
	{
		$model=KapasitasHarian::model()->findByAttributes(array('id_menu'=>$id, 'tanggal'=>$id1));
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param KapasitasHarian $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='kapasitas-harian-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}