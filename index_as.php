<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>FnDel</title>
		<link href="temp/images/icon.png" rel="shortcut icon" type="image/png" />
		<link href="temp/css/font-awesome.min.css" rel="stylesheet" >
		<link href="temp/css/bootstrap.min.css" rel="stylesheet">
		<link href="temp/css/datepicker.css" rel="stylesheet">
		<link href="temp/css/fndel.css" rel="stylesheet">

	</head>
	
	<body>
	
		<div id="wrapper">
			
			
			<img src="temp/images/softlaunching.jpg" style="width:100%;"></img>
			
			
		</div>
		
		<div class="device-xs visible-xs"></div>
		<div class="device-sm visible-sm"></div>
		<div class="device-md visible-md"></div>
		
		<script src="temp/js/jquery-1.10.2.min.js"></script>
		<script src="temp/js/bootstrap.min.js"></script>
		<script src="temp/js/bootstrap-datepicker.js"></script>
		<script src="temp/js/fndel.js"></script>
	</body>
</html>