
$(document).ready(function()
{

	$('.radio-select').prepend('<div class="select"><span></span>&nbsp;&nbsp;<i class="fa fa-caret-down"></i></div>');
	$('.radio-select > .select > span').text($('.radio-select > .options > div:first-child').text());
	$('.radio-select > .options > div:first-child > input[type=radio]').prop("checked", true); 
	
	$(document).click(function(e)
	{
		if($(e.target).attr('class') == 'select')
		{
			if(!$(e.target).siblings('.options').hasClass('show'))
			{
				$(e.target).siblings('.options').addClass('show'); 
			}
			else
			{
				$(e.target).siblings('.options').removeClass('show'); 
			}
		}
		else if($(e.target).parent().attr('class') == 'select')
		{
			if(!$(e.target).parent().siblings('.options').hasClass('show'))
			{
				$(e.target).parent().siblings('.options').addClass('show');
			}
			else
			{
				$(e.target).parent().siblings('.options').removeClass('show');
			}
		}
		else
		{
			if($('.radio-select .options').hasClass('show'))
			{
				$('.radio-select .options').removeClass('show');
			}
			if($(e.target).is('.options > div'))
			{
				$(e.target).parent().siblings('.select').children('span').html($(e.target).text());
				$(e.target).children('input[type=radio]').prop("checked", true); 
			}
		}
		
	});	
	
	//--------------------collapse combinations---------------------------------
	$('#dropdown-login *').click(function(e) 
	{
		e.stopPropagation();
	});
	
	$('#menu-toggle').click(function()
	{
		if($('#navbar-collapse-2').hasClass('collapse in'))
		{
			$('#navbar-collapse-2').collapse('hide');
		}
		if($('#search-box').hasClass('collapse in'))
		{
			$('#search-box').collapse('hide');
		}
	});
	$('#user-toggle').click(function()
	{
		if($('#navbar-collapse-1').hasClass('collapse in'))
		{
			$('#navbar-collapse-1').collapse('hide');
		}
		if($('#search-box').hasClass('collapse in'))
		{
			$('#search-box').collapse('hide');
		}
	});
	$('#search-toggle').click(function()
	{
		if($('#navbar-collapse-1').hasClass('collapse in'))
		{
			$('#navbar-collapse-1').collapse('hide');
		}
		if($('#navbar-collapse-2').hasClass('collapse in'))
		{
			$('#navbar-collapse-2').collapse('hide');
		}
	});
	$('.close-collapse').click(function()
	{
		$(this).parent().collapse('hide');
	});
	
	//--------------------------custom validation for homepage-----------------------------
	$('#hero-search-box form').submit(function(e)
	{
		if($('#hero-search-box form input[type="text"]').val().length < 1 )
		{
			e.preventDefault();
			$('#hero-search-box-alert').modal('show');
		}
	});
	$('#search-box form').submit(function(e)
	{
		if($('#search-box form input[type="text"]').val().length < 1 )
		{
			e.preventDefault();
			$('#hero-search-box-alert').modal('show');
		}
	});
	/*$('#user-box form#login-form').submit(function(e)
	{
		if($('#user-box form#login-form input[type="text"]').val().length < 1 ||
		   $('#user-box form#login-form input[type="password"]').val().length < 1)
		{
			e.preventDefault();
			$('#user-login-alert').modal('show');
		}
	});*/
	
	//-----------------------date picker-------------------------------------------------------------------
	
	$('.date-picker input').click(function()
	{
		$(this).datepicker('show');
	});
	
	$('.date-picker input').on('changeDate', function(ev){
		if (ev.viewMode === 'days') {
			$(this).datepicker('hide');
		}
	});


	//-----------------------rating/stars-------------------------------------------------------------------
	$('.rating > span > input[type=radio]').prop("checked", false); 
	$('.rating > span').click(function()
	{
		$('.rating > span').removeClass('active');
		$('.rating > span').removeClass('fill');
		$('input[type=radio]', this).prop("checked", true); 
		for(i=0; i< $(this).index()+2; i++)
		{
			$(this).parent().find('span:nth-child('+i+')').addClass('active');
		}
		
	});
	$('.rating > span').hover(function()
	{
		$('.rating > span').removeClass('fill');
		for(i=0; i< $(this).index()+2; i++)
		{
			$(this).parent().find('span:nth-child('+i+')').addClass('fill');
		}	
	});
	$('.rating').mouseleave(function()
	{
		$('.rating > span').removeClass('fill');
	});
	$('.rating-avg').each(function()
	{
		$(this).append('<div class="rating-range"></div>');
		$(this).append('<div class="rating-value"></div>');
		for(i=0; i<5; i++)
		{
			$('.rating-range', this).append('<i class="fa fa-star"></i>');
		}
		a = $(this).attr('value');
		b = Math.floor(a);
		if(a <= 5)
		{
			for(i=0; i<b; i++)
			{
				$('.rating-value', this).append('<i class="fa fa-star"></i>');
			}
			if(a-b > 0)
			{
				$('.rating-value', this).append('<i class="fa fa-star-half"></i>');
			}	
		}
	});
	
	$('.view-option #btn-list').click(function()
	{
		$('.view-option button').removeClass('active');
		$(this).parent().parent().addClass('list');
		$(this).addClass('active');
	});
	$('.view-option #btn-grid').click(function()
	{
		$('.view-option button').removeClass('active');
		$(this).parent().parent().removeClass('list');
		$(this).addClass('active');
	});
	
	
	
	
	//--------------------------------------------------------------------------------------------------------
	
	$('.currency').html(function()
	{
		var total = parseInt($(this).html());
		//var strtotal = total.toFixed(2).toString();
		var strtotal = total.toFixed(0).toString();
		var a = strtotal.split('');

		if (total >= 1000000000)
			a.splice(a.length - 9, 0, ',');

		if (total >= 1000000)
			a.splice(a.length - 6, 0, ',');

		if (total >= 1000)
			a.splice(a.length - 3, 0, ',');

		return 'Rp. ' + a.join("");
	});
	
	//--------------------------------------------------------------------------------------------------------
	
	if(isBreakpoint('xs')) 
	{
		$('#side-menu .panel-collapse').collapse('hide');
	}
	else if(isBreakpoint('sm') || isBreakpoint('md')) 
	{
		//$('#side-menu .panel-collapse').collapse('show');
	}
	$(window).resize(function()
	{
		if(isBreakpoint('xs')) 
		{
			$('#side-menu .panel-collapse').collapse('hide');
		}
		else if(isBreakpoint('sm') || isBreakpoint('md')) 
		{
			$('#side-menu .panel-collapse').collapse('show');
		}
	});
	
	$('#side-menu .list-group-item.header').append('<i class="fa fa-angle-up arrow"></i>');
	$('#side-menu .panel-collapse').on('hidden.bs.collapse', function () 
	{
		$('#side-menu .list-group-item.header[href="#'+$(this).attr('id')+'"] .arrow').attr('class','fa fa-angle-down arrow');
	});
	$('#side-menu .panel-collapse').on('show.bs.collapse', function () 
	{
		$('#side-menu .list-group-item.header[href="#'+$(this).attr('id')+'"] .arrow').attr('class','fa fa-angle-up arrow');
	});
	
	//------------------------ Modal input quantity item ---------------------
	$('#form-modal').on('show.bs.modal', function(e) {

		//get data-id attribute of the clicked element
		var menuId = $(e.relatedTarget).data('id-menu');
		var menuNama = $(e.relatedTarget).data('nama-menu');
		var hrgNama = $(e.relatedTarget).data('hrg-menu');

		//populate the textbox
		$(e.currentTarget).find('input[name="id-menu"]').val(menuId);
		$(e.currentTarget).find('input[name="nama-menu"]').val(menuNama);
		$(e.currentTarget).find('input[name="hrg-menu"]').val(hrgNama);
		
	});
	
	//----------------------- Form add row ---------------------------
	$('#form-pesan').submit(function(){
			$('#form-alert').modal('show');
	});
	
	$("#btn_id").click(function() {
		var id_menu = $("#id-menu").val();
		var nama_menu = $("#nama-menu").val();
		var harga = $("#hrg-menu").val();
		var jumlah = $("#jml-menu").val();
		var subTot = harga * jumlah;
		
		//alert(" id menu : " + id_menu + " \n nama menu : " + nama_menu + " \n jumlah : " + jumlah + " \n sub total : " + subTot +"\n\n Form Submitted Successfully......");
		//$('#form-table').append('<tr><td>' + nama_menu + '</td><td>' + jumlah + '</td><td>' + subTot + '</td></tr>');
		
		$('#form-pesan').submit();
	});
	
	// --------------------- submit daftar kosan -------------------
	$("input#btn_daftar").click(function(){
		
		//alert('data'+ $("#nama_kost").val());
		$('#form-daftar').submit();
	});
	
	// ----------------------- show pop up message ------------------
	$("input#btn_id").click(function(){
		$("#order_msg").dialog("open");
	});
	
	// ----------------------- batch input ---------------------
	$("input#batch_btn").click(function(){
		//alert('data send');
		var stock = $("#jml").val();
		$(".jml_stock").val(stock);
	});
        
        // ----------------------- ajax call -----------------------
        $("#menu-makanan a").click(function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            var request = $.ajax({
                url : url,
                cache : false,
                success : function(html){
                    $("#updated").html(html)
                }
            });
        });
});

function isBreakpoint(alias) 
{
    return $('.device-' + alias).is(':visible');
}