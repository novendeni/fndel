
$(document).ready(function()
{

	$('.radio-select').prepend('<div class="select"><span></span>&nbsp;&nbsp;<i class="fa fa-caret-down"></i></div>');
	$('.radio-select > .select > span').text($('.radio-select > .options > div:first-child').text());
	$('.radio-select > .options > div:first-child > input[type=radio]').prop("checked", true); 
	
	$(document).click(function(e)
	{
		if($(e.target).attr('class') == 'select')
		{
			if(!$(e.target).siblings('.options').hasClass('show'))
			{
				$(e.target).siblings('.options').addClass('show'); 
			}
			else
			{
				$(e.target).siblings('.options').removeClass('show'); 
			}
		}
		else if($(e.target).parent().attr('class') == 'select')
		{
			if(!$(e.target).parent().siblings('.options').hasClass('show'))
			{
				$(e.target).parent().siblings('.options').addClass('show');
			}
			else
			{
				$(e.target).parent().siblings('.options').removeClass('show');
			}
		}
		else
		{
			if($('.radio-select .options').hasClass('show'))
			{
				$('.radio-select .options').removeClass('show');
			}
			if($(e.target).is('.options > div'))
			{
				$(e.target).parent().siblings('.select').children('span').html($(e.target).text());
				$(e.target).children('input[type=radio]').prop("checked", true); 
			}
		}
		
	});	
	//$('.dropdown-toggle').dropdown();
	$('#dropdown-login *').click(function(e) 
	{
		e.stopPropagation();
	});
	
	$('#menu-toggle').click(function()
	{
		if($('#navbar-collapse-2').hasClass('collapse in'))
		{
			$('#navbar-collapse-2').collapse('hide');
		}
		if($('#search-box').hasClass('collapse in'))
		{
			$('#search-box').collapse('hide');
		}
	});
	$('#user-toggle').click(function()
	{
		if($('#navbar-collapse-1').hasClass('collapse in'))
		{
			$('#navbar-collapse-1').collapse('hide');
		}
		if($('#search-box').hasClass('collapse in'))
		{
			$('#search-box').collapse('hide');
		}
	});
	$('#search-toggle').click(function()
	{
		if($('#navbar-collapse-1').hasClass('collapse in'))
		{
			$('#navbar-collapse-1').collapse('hide');
		}
		if($('#navbar-collapse-2').hasClass('collapse in'))
		{
			$('#navbar-collapse-2').collapse('hide');
		}
	});
	$('.close-collapse').click(function()
	{
		$(this).parent().collapse('hide');
	});
	
	//------------------------------------------------------------------------------------------
	$('.rating > span > input[type=radio]').prop("checked", false); 
	$('.rating > span').click(function()
	{
		$('.rating > span').removeClass('active');
		$('.rating > span').removeClass('fill');
		$('input[type=radio]', this).prop("checked", true); 
		for(i=0; i< $(this).index()+2; i++)
		{
			$(this).parent().find('span:nth-child('+i+')').addClass('active');
		}
		
	});
	$('.rating > span').hover(function()
	{
		$('.rating > span').removeClass('fill');
		for(i=0; i< $(this).index()+2; i++)
		{
			$(this).parent().find('span:nth-child('+i+')').addClass('fill');
		}	
	});
	$('.rating').mouseleave(function()
	{
		$('.rating > span').removeClass('fill');
	});
	$('.rating-avg').each(function()
	{
		$(this).append('<div class="rating-range"></div>');
		$(this).append('<div class="rating-value"></div>');
		for(i=0; i<5; i++)
		{
			$('.rating-range', this).append('<i class="fa fa-star"></i>');
		}
		a = $(this).attr('value');
		b = Math.floor(a);
		if(a <= 5)
		{
			for(i=0; i<b; i++)
			{
				$('.rating-value', this).append('<i class="fa fa-star"></i>');
			}
			if(a-b > 0)
			{
				$('.rating-value', this).append('<i class="fa fa-star-half"></i>');
			}	
		}
	});
	
	$('.view-option #btn-list').click(function()
	{
		$('.view-option button').removeClass('active');
		$(this).parent().parent().addClass('list');
		$(this).addClass('active');
	});
	$('.view-option #btn-grid').click(function()
	{
		$('.view-option button').removeClass('active');
		$(this).parent().parent().removeClass('list');
		$(this).addClass('active');
	});
	
	if(isBreakpoint('xs')) 
	{
		$('#side-menu .panel-collapse').removeClass('in');
	}
	else if(isBreakpoint('sm') || isBreakpoint('md')) 
	{
		$('#side-menu .panel-collapse').addClass('in');
	}
	$(window).resize(function()
	{
		if(isBreakpoint('xs')) 
		{
			$('#side-menu .panel-collapse').removeClass('in');
		}
		else if(isBreakpoint('sm') || isBreakpoint('md')) 
		{
			$('#side-menu .panel-collapse').addClass('in');
		}
	});
});


function isBreakpoint(alias) 
{
    return $('.device-' + alias).is(':visible');
}
